RestaurantWaiter::Application.routes.draw do
get "/websocket", :to => WebsocketRails::ConnectionManager.new
resources :roles, path: '6S2C2585/superadmin/roles'
devise_for :super_admins, :controllers => {:sessions=>"super_admin/sessions",:passwords=>"super_admin/passwords", :registrations=>'super_admin/registrations'}, path: '6S2C2585/superadmin'
  devise_scope :super_admin  do
    scope "6S2C2585/superadmin", :module => 'super_admin' do
      resources :ads_subscriptions
      get '/' => 'sessions#new'
      get '/paid_admins' => 'admin#index', :as => :all_admin_index
      put '/update' => 'admin#update', :as => :update_restaurant_status
      get '/ad_control' => 'admin#ad_controller', :as => :ad_control
      get '/get_city' => 'admin#get_city', :as => :get_city
      get '/get_restaurantdetails' => 'admin#get_restaurantdetails', :as => :get_restaurantdetails
      get '/get_adsdetails' => 'admin#get_adsdetails', :as => :get_adsdetails
      post '/ad_settings' => 'admin#ad_settings', :as => :ad_settings
      delete '/ad_destroy/:id' => 'admin#ad_settings', :as => :ad_destroy
      get '/view_customers' => 'admin#view_customers', :as => :view_customers
      get '/new_customers' => 'admin#new_customers', :as => :new_customers
      put '/update_admin_status/:id' => 'admin#update_admin_status', :as => :update_admin
      get '/get_notification' => 'admin#get_notification', :as => :get_notifications

      get '/report' => 'report#report', :as => :report
      get '/report/restaurant_report' => 'report#restaurant_report', :as => :restaurant_report
      get '/subregion_options' => 'report#subregion_options', :as => :country_subregion_for_report
      get '/report/subregion_options' => 'report#subregion_options', :as => :report_get_subregion_options
      get '/report/get_city' => 'report#get_city', :as => :report_get_city
      get '/report/get_restaurant' => 'report#get_restaurant', :as => :report_get_restaurant

      get '/report/get_graphs' => 'report#get_graphs', :as => :report_get_graphs
      get '/report/get_restaurants_graphs' => 'report#get_restaurants_graphs', :as => :report_get_restaurants_graphs
    end
  end

=begin
routes for admin starts here
=end
#routes for login in the admin panel
  devise_for :admins, :controllers => {:registrations=>"admin/registrations",:sessions=>"admin/sessions",:passwords=>"admin/passwords"}, path: '6S1D0518/admins'
  devise_scope :admin do
    get '/admins' => 'admin/registrations#new', as: 'new_admin'
    get '/admins/subregion_options' => 'admin/registrations#subregion_options'
  end
#routes for login in the admin panel
   #devise_for :admins, :controllers => {:sessions=>"admin/sessions",:passwords=>"admin/passwords"}
   get "/6S1D0518/admins/", :to=> "admin/sessions#new"
   get "/admins/sign_in", :to=> "admin/sessions#new"
#routes for manage options
   get "admins/manageoptions",:to =>"admin/templates#manageoptions"
   post "admins/manageoptions",:to =>"admin/templates#manageoptions"
   post "admins/updateoptions",:to =>"admin/templates#updateoptions" ,:as => :create

#routes for the template page
   #get template page
   get "/admins/template",:to =>"admin/templates#uploadtemplate"
   get "/admins/download_template",:to =>"admin/templates#downloadtemplate"
   post "admins/upload",:to =>"admin/templates#upload" #upload excel file
   post "admins/uploadallimages",:to =>"admin/templates#uploadallimages" 
   post "/admins/getcategorycount", :to => "admin/templates#getcategorycount"

#routes for the allcategories page
   #get page to display all categories of a restaurant
   get "/admins/allcategories",:to =>"admin/categories#allcategories"
   post "/admins/allcategories",:to =>"admin/categories#allcategories"
   post "/admins/addcategory",:to =>"admin/categories#addcategory" #add new category
   post "/admins/deletecategory",:to =>"admin/categories#deletecategory" #delete a category
   post "admins/getcategoryinfo", :to => "admin/categories#getcategoryinfo" #get category info
   post "/admins/updatecategory", :to => "admin/categories#updatecategory" #update a category
   
#routes for the drinks page

   #for controller categories. It will deal with all the methods related to drinks
   get "/admins/drinks",:to =>"admin/categories#drinks"
   post "/admins/adddrinkcategory",:to =>"admin/categories#adddrinkcategory"
   post "/admins/deletedrinkcategory",:to =>"admin/categories#deletedrinkcategory"
   get "/admins/drinkdetail", :to => "admin/categories#drinkdetail"
   post "/admins/deletedrink", :to => "admin/categories#deletedrink"
   post "/admins/adddrink", :to => "admin/categories#adddrink"
    post "/admins/updatedrink", :to => "admin/categories#updatedrink"
   post "/admins/getdrinkcategoryinfo", :to => "admin/categories#getdrinkcategoryinfo"
    post "/admins/updatedrinkcategory", :to => "admin/categories#updatedrinkcategory"
   

#routes for the all the food items in a category
   #for controller categories. It will deal with all the methods related to details of drinks or food items
   get "admins/categorydetail",:to =>"admin/categories#categorydetail"
   post "/admins/addfooditem",:to =>"admin/categories#addfooditem"
   post "/admins/deletefooditem",:to =>"admin/categories#deletefooditem"
    post "/admins/updatefooditem",:to =>"admin/categories#updatefooditem"

#routes for the all the extras in a food item
   get "/admins/extrasdetail",:to =>"admin/categories#extrasdetail"
  post "/admins/addextras",:to =>"admin/categories#addextras"
  post "/admins/deleteextras",:to =>"admin/categories#deleteextras"
   post "/admins/updateextras",:to =>"admin/categories#updateextras"

#routes for the all the extras in a food item
   get "/admins/extrasdetail",:to =>"admin/categories#extrasdetail"
  post "/admins/addextras",:to =>"admin/categories#addextras"
  post "/admins/deleteextras",:to =>"admin/categories#deleteextras"
   
   
 #routes for the settings and subscriptions ads
  get "/admins/settings",:to =>"admin/settings#restaurantsettings"
  get "/admins/othersettings",:to =>"admin/settings#othersettings"
  get "admins/adsubscription",:to =>"admin/settings#adsubscription"
  get "/admins/alladvertisements",:to =>"admin/settings#alladvertisements"
  post "/admins/alladvertisements",:to =>"admin/settings#alladvertisements"
  post "/admins/updatekitchentime",:to =>"admin/settings#updatekitchentime"
  post "/admins/updatebartime",:to =>"admin/settings#updatebartime"
  post "/admins/updatehappyhourtime",:to =>"admin/settings#updatehappyhourtime"
  post "/admins/addadvertisement",:to =>"admin/settings#addadvertisement"
  post "/admins/deleteadvertisement",:to =>"admin/settings#deleteadvertisement"
  post "/admins/disableadvertisement",:to =>"admin/settings#disableadvertisement"
  post "/admins/enableadvertisement",:to =>"admin/settings#enableadvertisement"
  post "/admins/updatesettingoptions",:to =>"admin/settings#updatesettingoptions"
  post "/admins/updaterestaurantoptions",:to =>"admin/settings#updaterestaurantoptions"
  post "/admins/paybill",:to =>"admin/settings#paybill"
  post "/admins/updatesubscription",:to =>"admin/settings#updatesubscription"
  post "/admins/updatetransactionid",:to =>"admin/settings#updatetransactionid"
  get '/admins/state_options' => 'admin/settings#state_options'
  post '/admins/makepayment' => 'admin/settings#makepayment'
  get  '/admins/paymentdetails'=>'admin/settings#paymentdetails'
  
  #routes for password
  post "/admins/forgotpassword",:to =>"admin/settings#forgotpassword"
  get "admins/settings/resetpassword", :to => "admin/settings#resetpassword"
  post "/admins/makesubscription", :to => "admin/settings#makesubscription"
  
=begin
routes for app starts here
=end
   
   
   
   
   
   
  devise_for :users, :controllers => {:registrations => "registrations",:sessions=>"sessions",:passwords=>"passwords"}
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".


  # You can have the root of your site routed with "root"
   root 'admin/sessions#new'

  #for controller allcaregories
  get "/allcategories/getallcategories", :to => "allcategories#getallcategories"

  post "/allcategories/getallcategories", :to => "allcategories#getcategories"

  post "/allcategories/getfooditemsincategory", :to => "allcategories#getfooditemsincategory"

  post "/allcategories/getfooditemsdetails", :to => "allcategories#getfooditemsdetails"

  post "/allcategories/getextraslist", :to => "allcategories#getextraslist"

  post "/allcategories/getextrasdetails", :to => "allcategories#getextrasdetails"


  #for controller drinks
  get "/drinkdetails", :to => "drinks#drinkdetails"

  post "/drinkdetails/getalldrinkcategories", :to => "drinks#getalldrinkcategories"

  post "/drinkdetails/getalldrinks", :to => "drinks#getalldrinks"

  post "/drinkdetails/getdrinkdetail", :to => "drinks#getdrinkdetail"


   #for controller allrestaurants
  get "/restaurantdetails", :to => "allrestaurants#restaurantdetails"

  post "/restaurantdetails/getrestaurantlist", :to => "allrestaurants#getrestaurantlist"

  post "/restaurantdetails/getrestaurantdetail", :to => "allrestaurants#getrestaurantdetail"

  post "/restaurantdetails/getqrcode", :to => "allrestaurants#getqrcode"
  
  post "/restaurantdetails/getrestaurantdetailfromgimbal", :to => "allrestaurants#getrestaurantdetailfromgimbal"
  
  post "/restaurantdetails/sendnotifications", :to => "allrestaurants#sendnotifications"
  
  get "/restaurantdetails/getcurrenttime", :to => "allrestaurants#getcurrenttime"
  

  #for controller orders
  get "/orderdetails", :to => "orders#orderdetails"

  post "/orderdetails/addorder", :to => "orders#addorder"

  post "/orderdetails/revieworder", :to => "orders#revieworder"

  post "/orderdetails/confirmorder", :to => "orders#confirmorder"

  post "/orderdetails/history", :to => "orders#history"

  post "/orderdetails/checkrestauranttimings", :to => "orders#checkrestauranttimings"
  
  post "/orderdetails/paylaterhistory", :to => "orders#paylaterhistory"
  
  #for controller userinfo
  get "/userdetails", :to => "userinfo#getuserdetails"

  post "/userinfo/getuserinfo", :to => "userinfo#getuserinfo"

  post "/userinfo/uploadimage", :to => "userinfo#uploadimage"

  post "/userinfo/accesstokenlogin", :to => "userinfo#accesstokenlogin"

  post "/userinfo/forgotpassword", :to => "userinfo#forgotpassword"

  get "/userinfo/resetpassword", :to => "userinfo#resetpassword"

  post "/userinfo/updatepassword", :to => "userinfo#updatepassword"

  #for controller payment
  get "/paymentdetails", :to => "payment#paymentdetails"

  post "/payment/paymenttransactions", :to => "payment#paymenttransactions"

  post "/payment/getcardlist", :to => "payment#getcardlist"

  post "/payment/addcardnumber", :to => "payment#addcardnumber"

  post "/payment/setdefaultcard", :to => "payment#setdefaultcard"

  post "/payment/deletecard", :to => "payment#deletecard"
  
  post "/payment/updatetipinfo", :to => "payment#updatetipinfo"

  get  "/payment/paylater", :to => "payment#paylater"
  
   #for controller advertisements
  get "/ad_details", :to => "advertisements#ad_details"
  
  post "/advertisements/get_ad_details", :to => "advertisements#get_ad_details"

#Last route in routes.rb
 match "*path", :to => 'admin/errors#routing' ,:via=> :all
end
