# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += [
    'jquery-ui-1.9.2.custom.min.js',
    'bootstrap.min.js',
    'bootbox.min.js',
    'bootstrap.min.css',
    'bootstrap-reset.css',
    'style.css',
    'style-responsive.css',
    'font-awesome.css',
    'jquery.nicescroll.js',
    'common-scripts.js',
    'jquery.scrollTo.min.js',
    'respond.min.js',
    'jquery.customSelect.min.js',
    'jquery.sparkline-11.js',
    'super_admin.js',
    'jquery.dataTables.min.js',
    'jquery.dataTables.css',
    'dataTables.bootstrap.css',
    'dataTables.bootstrap.js',
    'editable-table',
    'DT_bootstrap.js',
    'DT_bootstrap.css',
    'jquery.validate.min.js',
    'additional-methods.min.js'
]
