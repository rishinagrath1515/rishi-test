# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RestaurantWaiter::Application.config.secret_key_base = 'f322735731e9c4c88c9f0934f1c2efa1b22f292d4918b173d11446a219570a58d05db9439910caddf0c51901ee90b25f9ad4a9cf5e52774fd9e5b0163a886201'
