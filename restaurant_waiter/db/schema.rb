# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141204090632) do

  create_table "admins", force: true do |t|
    t.string   "email",                            default: "",   null: false
    t.string   "encrypted_password",               default: "",   null: false
    t.integer  "restaurant_id"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                    default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "authenticity_token"
    t.integer  "confirm_status",         limit: 1
    t.integer  "is_paid",                limit: 1
    t.string   "transaction_id"
    t.boolean  "active",                           default: true
    t.float    "amount_paid"
    t.datetime "amount_paid_date"
    t.string   "name"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "ads_subscriptions", force: true do |t|
    t.string   "name"
    t.string   "ad_type"
    t.float    "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "no_of_days"
    t.string   "short_name"
  end

  create_table "advertisements", force: true do |t|
    t.string   "name"
    t.integer  "ad_type",             limit: 1
    t.text     "image"
    t.text     "video_name"
    t.string   "video_thumb"
    t.integer  "restaurant_id"
    t.integer  "ad_settings",         limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "ad_subscription_end"
    t.integer  "time_period"
    t.integer  "subscription_type",   limit: 1
  end

  create_table "alldrinks", force: true do |t|
    t.integer  "drink_category_id"
    t.string   "drink_name"
    t.text     "image"
    t.string   "description"
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "max_quantity",        limit: 1
    t.integer  "include_in_bar",      limit: 1
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "menu_item_id"
  end

  create_table "appverions", force: true do |t|
    t.integer  "ios_app_version"
    t.integer  "ios_force_update",     limit: 1
    t.integer  "android_app_version"
    t.integer  "android_force_update", limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "category_name",       default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "restaurant_id"
    t.text     "image"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "drinkcategories", force: true do |t|
    t.string   "drinkcategoryname"
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "image"
    t.integer  "include_in_bar",      limit: 1
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "drinkpricesandsizes", force: true do |t|
    t.integer  "drink_id"
    t.integer  "size",             limit: 1
    t.float    "price"
    t.float    "happy_hour_price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "size_name"
  end

  create_table "extras", force: true do |t|
    t.string   "extra_name",                    default: "", null: false
    t.text     "image"
    t.integer  "size",                limit: 1, default: 1
    t.float    "price"
    t.string   "description"
    t.float    "happy_hour_price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "restaurant_id"
    t.integer  "food_id"
    t.integer  "max_quantity",        limit: 1
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "modifier_id"
  end

  create_table "foodandextras", force: true do |t|
    t.integer  "food_id"
    t.integer  "extras_id"
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fooditemandcatgories", force: true do |t|
    t.integer  "category_id"
    t.integer  "food_id"
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fooditems", force: true do |t|
    t.string   "food_name",                     default: "", null: false
    t.text     "image"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "restaurant_id"
    t.integer  "category_id"
    t.integer  "max_quantity",        limit: 1
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "menu_item_id"
  end

  create_table "fooditemspricesandsizes", force: true do |t|
    t.integer  "food_id"
    t.integer  "size",             limit: 1
    t.float    "price"
    t.float    "happy_hour_price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "size_name"
  end

  create_table "gimbaldetails", force: true do |t|
    t.integer  "gimbal_id"
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orderdetails", force: true do |t|
    t.integer  "order_id"
    t.integer  "food_id"
    t.integer  "quantity"
    t.float    "price"
    t.integer  "size",       limit: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "submitted",            default: false
  end

  create_table "orderdetailswithdrinks", force: true do |t|
    t.integer  "order_id"
    t.integer  "food_id"
    t.integer  "drink_id"
    t.integer  "drink_size", limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quantity"
    t.float    "price"
    t.boolean  "submitted",            default: false
  end

  create_table "orderdetailswithextras", force: true do |t|
    t.integer  "order_id"
    t.integer  "food_id"
    t.integer  "extras_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quantity"
    t.float    "price"
    t.boolean  "submitted",  default: false
  end

  create_table "orders", force: true do |t|
    t.integer  "user_id"
    t.integer  "gratuity"
    t.integer  "tax"
    t.float    "tip"
    t.float    "total_bill"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "restaurant_id"
    t.integer  "confirm_status", limit: 1
    t.integer  "pay_status",     limit: 1
    t.integer  "table_id"
    t.float    "pending_bill"
  end

  create_table "restaurantdetails", force: true do |t|
    t.string   "restaurant_name",                              default: "", null: false
    t.string   "address1",                         limit: 150
    t.string   "address2",                         limit: 150
    t.string   "address3",                         limit: 150
    t.string   "phone1",                           limit: 25
    t.string   "phone2",                           limit: 25
    t.float    "longitude"
    t.float    "latitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "restaurant_email"
    t.integer  "display_price_outside_restaurant", limit: 1
    t.integer  "gratuity_status",                  limit: 1
    t.float    "gratuity"
    t.text     "image"
    t.integer  "subscription_type",                limit: 1
    t.string   "excel_file_name"
    t.string   "excel_content_type"
    t.integer  "excel_file_size"
    t.datetime "excel_updated_at"
    t.integer  "is_happy_hour_available",          limit: 1
    t.integer  "zip"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "transaction_id"
    t.integer  "is_paid",                          limit: 1
    t.string   "rid"
    t.string   "chain_id"
  end

  create_table "restauranthappyhours", force: true do |t|
    t.string   "session_name"
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "restauranttables", force: true do |t|
    t.string   "qrcode",        null: false
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "table_id"
  end

  create_table "restauranttimings", force: true do |t|
    t.integer  "day",                      limit: 1
    t.time     "opening_time"
    t.time     "closing_time"
    t.integer  "is_happy_hours_available"
    t.time     "happy_hour_start_time"
    t.time     "happy_hour_end_time"
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "day_name"
    t.time     "bar_opening_time"
    t.time     "bar_closing_time"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sizes", force: true do |t|
    t.string   "size_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "variant_id"
  end

  create_table "super_admins", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role_id"
  end

  add_index "super_admins", ["email"], name: "index_super_admins_on_email", unique: true, using: :btree
  add_index "super_admins", ["reset_password_token"], name: "index_super_admins_on_reset_password_token", unique: true, using: :btree
  add_index "super_admins", ["role_id"], name: "index_super_admins_on_role_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                              default: "", null: false
    t.string   "username",               limit: 100, default: ""
    t.integer  "fb_id",                  limit: 8,   default: 0
    t.string   "fb_access_token",        limit: 300, default: ""
    t.string   "user_access_token",      limit: 300,              null: false
    t.string   "encrypted_password",                 default: "", null: false
    t.integer  "is_admin",               limit: 1,   default: 0
    t.integer  "device_type",            limit: 1,   default: 0
    t.string   "device_token",           limit: 300
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "image"
    t.integer  "cc_is_registered",       limit: 1,   default: 0
    t.integer  "customer_id_default",    limit: 1
    t.string   "customer_id1"
    t.string   "customer_id2"
    t.integer  "card1"
    t.integer  "card2"
    t.integer  "app_version"
    t.string   "avtar_file_name"
    t.string   "avtar_content_type"
    t.integer  "avtar_file_size"
    t.datetime "avtar_updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
