class CreateFooditems < ActiveRecord::Migration
  def change
    create_table :fooditems do |t|
      t.string :food_name, null: false, default: "",unique: true
      t.text  :image
      t.integer :size , :limit=>1, :default=>1
      t.float :price
      t.string :description
      t.float :happy_hour_price
      t.timestamps
    end
  end
end
