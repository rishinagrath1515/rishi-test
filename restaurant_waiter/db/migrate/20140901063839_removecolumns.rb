class Removecolumns < ActiveRecord::Migration
  def change
    remove_column :orderdetails, :extras_id
    remove_column :orderdetailswithdrinks, :size_name
    remove_column :fooditems, :size 
    remove_column :fooditems, :price
    remove_column :fooditems, :happy_hour_price
    remove_column :fooditems, :size_full
    remove_column :fooditems, :price_full
    remove_column :extras, :size_full
    remove_column :extras, :price_full
    remove_column :alldrinks, :size
    remove_column :alldrinks, :price
    remove_column :alldrinks, :happy_hour_price
  end
end
