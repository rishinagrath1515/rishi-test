class CreateDrinkcategories < ActiveRecord::Migration
  def change
    create_table :drinkcategories do |t|
      t.string :drinkcategoryname
      t.integer :restaurant_id
      t.timestamps
    end
  end
end
