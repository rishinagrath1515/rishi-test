class Addcolumnsubscriptionandbar < ActiveRecord::Migration
  def change
     add_column :restaurantdetails, :subscription_type,    :integer ,:limit=>1
     add_column :drinkcategories, :include_in_bar,  :integer ,:limit=>1
     add_column :alldrinks, :include_in_bar,  :integer ,:limit=>1
  end
end
