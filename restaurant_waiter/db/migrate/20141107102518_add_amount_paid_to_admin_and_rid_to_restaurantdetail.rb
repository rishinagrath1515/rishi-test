class AddAmountPaidToAdminAndRidToRestaurantdetail < ActiveRecord::Migration
  def change
    add_column :admins, :amount_paid, :float
    add_column :restaurantdetails, :rid, :string
  end
end

