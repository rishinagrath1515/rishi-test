class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
    t.integer :user_id
      t.integer :gratuity
      t.integer :tax
      t.float :tip
      t.float :total_bill
      t.timestamps
    end
  end
end
