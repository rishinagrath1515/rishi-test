class Addcolumnziptorestaurantdetails < ActiveRecord::Migration
  def change
  add_column :restaurantdetails, :zip,    :integer 
  add_column :restaurantdetails, :city,    :string
  add_column :restaurantdetails, :state,    :string
  add_column :restaurantdetails, :country,    :string
  end
end
