class CreateOrderdetailswithextras < ActiveRecord::Migration
  def change
    create_table :orderdetailswithextras do |t|
      t.integer :order_id
      t.integer :food_id
      t.integer :extras_id
      t.timestamps
    end
  end
end
