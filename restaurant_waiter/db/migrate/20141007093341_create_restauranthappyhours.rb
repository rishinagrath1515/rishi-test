class CreateRestauranthappyhours < ActiveRecord::Migration
  def change
    create_table :restauranthappyhours do |t|
      t.string :session_name
      t.time  :start_time
      t.time  :end_time
      t.integer :restaurant_id
      t.timestamps
    end
    
     add_column :restaurantdetails, :is_happy_hour_available,    :integer ,:limit=>1
  end
end
