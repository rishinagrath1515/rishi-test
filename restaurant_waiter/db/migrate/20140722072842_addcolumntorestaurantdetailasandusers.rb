class Addcolumntorestaurantdetailasandusers < ActiveRecord::Migration
  def change
   add_column :restaurantdetails, :gratuity_status,    :integer ,:limit=>1
    add_column :restaurantdetails, :gratuity,    :float ,:limit=>1


    add_column :users, :cc_is_registered,    :integer ,:limit=>1,:default=>0
    add_column :users, :cc_token,    :string
  end
end
