class Addcolumnimageanddaynametorestaurantdetails < ActiveRecord::Migration
  def change
    add_column :restaurantdetails, :image,    :text
    add_column :restaurantdetails, :day_name,    :string
  end
end
