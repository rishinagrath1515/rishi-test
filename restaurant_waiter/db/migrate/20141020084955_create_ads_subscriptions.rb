class CreateAdsSubscriptions < ActiveRecord::Migration
  def change
    create_table :ads_subscriptions do |t|
      t.string :name
      t.string :ad_type
      t.float :price
      t.timestamps
    end
  end
end
