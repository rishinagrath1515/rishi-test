class CreateAdvertisements < ActiveRecord::Migration
  def change
    create_table :advertisements do |t|
      t.string :name
      t.integer :ad_type, :limit=>1
      t.text :image
      t.text :video_name
      t.string :video_thumb
      t.integer :restaurant_id
      t.integer :ad_settings, :limit=>1
      t.timestamps
    end
  end
end
