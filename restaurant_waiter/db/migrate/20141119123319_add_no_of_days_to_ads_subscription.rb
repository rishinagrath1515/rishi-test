class AddNoOfDaysToAdsSubscription < ActiveRecord::Migration
  def change
    add_column :ads_subscriptions, :no_of_days, :integer
    add_column :ads_subscriptions, :short_name, :string
  end
end
