class CreateOrderdetails < ActiveRecord::Migration
  def change
    create_table :orderdetails do |t|
       t.integer :order_id
      t.integer :food_id
      t.integer :extras_id
      t.integer :quantity
      t.float :price
      t.integer :size,:limit=>2
      t.timestamps
      t.timestamps
    end
    
    add_column :orders, :restaurant_id,    :integer
  end
end
