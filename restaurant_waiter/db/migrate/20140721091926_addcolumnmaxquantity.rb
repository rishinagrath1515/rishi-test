class Addcolumnmaxquantity < ActiveRecord::Migration
  def change
   add_column :fooditems, :max_quantity,    :integer ,:limit=>1
    add_column :extras, :max_quantity,    :integer ,:limit=>1
    add_column :alldrinks, :max_quantity,    :integer ,:limit=>1
  end
end
