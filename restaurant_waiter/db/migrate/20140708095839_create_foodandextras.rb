class CreateFoodandextras < ActiveRecord::Migration
  def change
    create_table :foodandextras do |t|
      t.integer  :food_id
      t.integer  :extras_id
      t.integer :restaurant_id
      t.timestamps
    end
  end
end
