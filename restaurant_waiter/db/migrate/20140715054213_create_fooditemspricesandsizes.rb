class CreateFooditemspricesandsizes < ActiveRecord::Migration
  def change
    create_table :fooditemspricesandsizes do |t|
      t.integer :food_id
      t.integer :size ,:limit=>1
      t.float :price
      t.float :happy_hour_price
      t.timestamps
    end
  end
end
 