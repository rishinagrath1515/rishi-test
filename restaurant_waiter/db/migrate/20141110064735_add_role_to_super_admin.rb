class AddRoleToSuperAdmin < ActiveRecord::Migration
  def change
    add_reference :super_admins, :role, index: true
  end
end
