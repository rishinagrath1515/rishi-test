class CreateRestauranttimings < ActiveRecord::Migration
  def change
    create_table :restauranttimings do |t|
       t.integer :day, :limit=>1
      t.time :opening_time
      t.time :closing_time
      t.integer :is_happy_hours_available
      t.time :happy_hour_start_time
      t.time :happy_hour_end_time
      t.integer :restaurant_id
      t.timestamps
    end
    
    add_column :restaurantdetails, :restaurant_email,    :string
  end
end
