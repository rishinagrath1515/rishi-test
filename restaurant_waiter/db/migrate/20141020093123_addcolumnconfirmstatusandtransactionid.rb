class Addcolumnconfirmstatusandtransactionid < ActiveRecord::Migration
  def change
     add_column :admins, :confirm_status,    :integer ,:limit=>1
     add_column :restaurantdetails, :transaction_id,    :string
  end
end
