class CreateExtras < ActiveRecord::Migration
  def change
    create_table :extras do |t|
      t.string :extra_name, null: false, default: "",unique: true
      t.text  :image
      t.integer :size , :limit=>1, :default=>1
      t.float :price
      t.string :description
      t.float :happy_hour_price
      t.timestamps
    end
  end
end
