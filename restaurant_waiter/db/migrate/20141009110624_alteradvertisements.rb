class Alteradvertisements < ActiveRecord::Migration
  def change
    add_attachment :advertisements, :avatar
    add_attachment :advertisements, :videos
  end
end
