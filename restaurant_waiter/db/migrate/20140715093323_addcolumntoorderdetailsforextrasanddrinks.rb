class Addcolumntoorderdetailsforextrasanddrinks < ActiveRecord::Migration
  def change
    add_column :orderdetailswithextras, :quantity,    :integer
    add_column :orderdetailswithextras, :price,    :float
    add_column :orderdetailswithdrinks, :quantity,    :integer
    add_column :orderdetailswithdrinks, :price,    :float
  end
end
