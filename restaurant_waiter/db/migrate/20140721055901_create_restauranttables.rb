class CreateRestauranttables < ActiveRecord::Migration
  def change
   create_table :restauranttables do |t|
      t.string :qrcode,              null: false
      t.integer :restaurant_id
      t.timestamps
    end

    add_column :orders, :table_id,    :integer
  end
end
