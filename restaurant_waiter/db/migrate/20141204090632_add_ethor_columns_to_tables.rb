class AddEthorColumnsToTables < ActiveRecord::Migration
  def change
    add_column :restaurantdetails, :chain_id, :string
    add_column :fooditems, :menu_item_id, :string
    add_column :extras, :modifier_id, :string
    add_column :alldrinks, :menu_item_id, :string
    add_column :sizes, :variant_id, :string
    add_column :restauranttables, :table_id, :string
    add_column :orderdetails, :submitted, :boolean, :default => false
    add_column :orderdetailswithextras, :submitted, :boolean, :default => false
    add_column :orderdetailswithdrinks, :submitted, :boolean, :default => false
  end
end
