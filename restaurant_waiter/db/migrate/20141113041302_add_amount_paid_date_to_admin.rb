class AddAmountPaidDateToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :amount_paid_date, :datetime
  end
end
