class CreateDrinkpricesandsizes < ActiveRecord::Migration
  def change
    create_table :drinkpricesandsizes do |t|
         t.integer :drink_id
      t.integer :size ,:limit=>1
      t.float :price
      t.float :happy_hour_price
      t.timestamps
    end
  end
end
