class CreateAlldrinks < ActiveRecord::Migration
  def change
    create_table :alldrinks do |t|
      t.integer :drink_category_id
      t.string :drink_name
      t.integer  :size, :limit=>1
      t.text :image
      t.float :price
      t.string :description
      t.float :happy_hour_price
      t.integer :restaurant_id
      t.timestamps
    end
  end
end
