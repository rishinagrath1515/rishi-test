class CreateRestaurantdetails < ActiveRecord::Migration
  def change
    create_table :restaurantdetails do |t|
      t.string :restaurant_name,              null: false, default: ""
      t.string :address1 ,:limit=>150
      t.string :address2 ,:limit=>150
      t.string :address3 ,:limit=>150
      t.string :phone1 ,:limit=>25
      t.string :phone2 ,:limit=>25
      t.float :longitude
      t.float :latitude
      t.timestamps
    end
  end
end
