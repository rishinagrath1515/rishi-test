# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

super_admin_role = Role.create!(:name => "Super Admin")
manager_role = Role.create!(:name => "Manager")
SuperAdmin.create!(:email => 'superadmin@obronwaiter.com', :password => 'obron#waiter#superadmin', :role  => super_admin_role)
SuperAdmin.create!(:email => 'manager@obronwaiter.com', :password => 'obron#waiter#manager', :role => manager_role)
