$(document).ready(function () {
    var d = new Date();
    d.setDate(1);
    $('.reservation').daterangepicker({startDate:d,format: 'YYYY-MM-DD'});
});


$(document).ready(function () {
    $("#date_dropdown option[value='7']").attr("selected", "selected");
    $("#date_dropdown").change(function () {
        var dateFilterValue = $("#date_dropdown").val();
        if (dateFilterValue == 6) {
            $("#report_date_range").css("display", "block");
        }
        else {
            $("#report_date_range").css("display", "none");
        }
    });
});

$(document).on('change', '#report_country', function(e){
    $('#report_city_select_id').html('');
    $('#report_restaurant_select_id').html('');
    country_code = $(this).val();
    url = "/6S2C2585/superadmin/report/subregion_options?parent_region="+country_code;
    $('#report_state_select_id').load(url);

});

$(document).on('change', '#report_state', function(e){
    $('#report_restaurant_select_id').html('');
    getCityForReport();
});


function getCityForReport(){
    country = $("#report_country").val();
    state = $("#report_state").val();
    url = "/6S2C2585/superadmin/report/get_city?country="+country+"&state="+state
    $('#report_city_select_id').load(url);
}

$(document).on('click', "#report_filters", function(event){
    country = $("#report_country").val();
    state = $("#report_state").val();
    city = $("#report_city").val();
    date = $("#date_dropdown").val();
    time = date;
    if (date == 6){time = $("#report_date_range").val();}
    if(typeof state === "undefined"){state="";}
    if(typeof city === "undefined"){city="";}
    getGraph(country,state,city,time);
});

function getGraph(country,state,city,time){
    $.ajax({
        url: "/6S2C2585/superadmin/report/get_graphs",
        type: 'GET',
        data: {'country': country, 'state': state, 'city': city, 'time': time},
        success: function(response){
            $("#home").html(response);
        },
        error: function(response){

        }
    })
}