$('a.poplight1[href^=#]').click(function(event)
    {
        $('.close1').remove();
        var popID = $(this).attr('rel');
        //Get Popup Name
        var popURL = $(this).attr('href');
        //Get Popup href to define size
        //Pull Query & Variables from href URL
        var query = popURL.split('?');
        var dim = query[1].split('&');
        var popWidth = dim[0].split('=')[1];
        //Gets the first query string value
        //Fade in the Popup and add close button
        $('#' + popID).fadeIn().css({
                'width': Number(popWidth)}
        ).prepend('<a href="#" class="close1"><img src="../../assets/adminimg/close_icon.png" class="btn_close" title="Close Window" alt="Close" /></a>');
        //Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
        var popMargTop = ($('#' + popID).height() + 80) / 2;
        var popMargLeft = ($('#' + popID).width() + 80) / 2;
        //Apply Margin to Popup
        $('#' + popID).css({
                'margin-top': -popMargTop,
                'margin-left': -popMargLeft
            }
        );
        //Fade in Background
        $('body').append('<div id="fade"></div>');
        //Add the fade layer to bottom of the body tag.
        $('#fade').css({'filter': 'alpha(opacity=80)'}
        ).fadeIn();
        //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies
        return false;

    }
);
//Close Popups and Fade Layer
$('body').on('click','a.close1, #fade', function() {
        console.log('in');
        //When clicking on the close or fade layer...
        $('#fade , .popup_block').fadeOut(function() {
                //$('#fade, a.close').remove();  //fade them both out
                 $('.input-block-level').val("");
                        $('#addfooditemdesc').val("");
            }
        );
        return false;

    }
);