var EditableRoleTable = function () {
    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function createRow(oTable, nRow){
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = aData[3];
                jqTds[2].innerHTML = aData[4];
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[2].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                var jqTds = $('>td', nRow);
                role_id = $(jqTds[0]).attr('role_id');
                name = jqInputs[0].value;
                if(name == ""){
                    bootbox.alert({title: "Alert!", message: "Please enter name"});
                    oTable.fnDraw();
                    return false
                }
                if (typeof role_id == "undefined"){
                    $.ajax({
                        url: "/roles",
                        type: 'POST',
                        dataType: 'JSON',
                        data: {role: {'name': name }},
                        success: function(data){
                            if(data.success=='1'){
                                bootbox.alert({title: "Alert!", message: "Data saved successfully."});
                                $(jqTds[0]).attr('role_id', data.role_id);
                                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
                                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 2, false);
                                oTable.fnDraw();
                                nEditing = null;
                            }else{
                                bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                            }
                        },
                        error: function(data){
                            bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                        }
                    });
                }else{
                    $.ajax({
                        url: "/roles/"+role_id,
                        type: 'PUT',
                        dataType: 'JSON',
                        data: {role: {'name': name,' id': role_id}},
                        success: function(data){
                            if(data.success=='1'){
                                bootbox.alert({title: "Alert!", message: "Data saved successfully."});
                                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
                                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 2, false);
                                oTable.fnDraw();
                                nEditing = null;
                            }else{
                                bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                            }
                        },
                        error: function(data){
                            bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                        }
                    });
                }
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 3, false);
                oTable.fnDraw();
            }

            var oTable = $('#editable-roles').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }
            });

            jQuery('#editable-roles_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#editable-roles_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

            var nEditing = null;

            $(document).on('click', '#editable-roles_new', function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '',
                    '<a class="edit" href="">Save</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                if (nEditing !== null){
                    var jqTds = $('>td>a', nEditing);
                    if ($(jqTds[1]).attr("data-mode") == "new") {
                        oTable.fnDeleteRow(nEditing);
                    } else {
                        restoreRow(oTable, nEditing);
                    }
                }
                createRow(oTable, nRow);
                nEditing = nRow;
            });

            $(document).on('click', '#editable-roles a.delete', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var jqTds = $('>td', nRow);
                role_id = $(jqTds[0]).attr('role_id');
                bootbox.confirm({
                    message: "Are you sure you want to delete this row",
                    title: "Please Confirm",
                    callback:  function(doIt){
                        if(doIt){
                                $.ajax({
                                    url: "/roles/"+role_id,
                                    type: 'DELETE',
                                    dataType: 'JSON',
                                    data: {role: {'id': role_id}},
                                    success: function(data){
                                        if(data.success=='1'){

                                            oTable.fnDeleteRow(nRow);
                                            bootbox.alert({title: "Alert!", message: "Data deleted successfully."});
                                        }else{
                                            bootbox.alert({title: "Alert!", message: data.message});
                                        }
                                    },
                                    error: function(data){
                                        alert('error')
                                    }
                                });
                        }else{
                        return false;
                        }
                    }
                })

            });

            $(document).on('click', '#editable-roles a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    nEditing = null;
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $(document).on('click', '#editable-roles a.edit', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    var jqTds = $('>td>a', nEditing);
                    if ($(jqTds[1]).attr("data-mode") == "new") {
                        oTable.fnDeleteRow(nEditing);
                        editRow(oTable, nRow);
                        nEditing = nRow;
                    } else {
                        restoreRow(oTable, nEditing);
                        editRow(oTable, nRow);
                        nEditing = nRow;
                    }
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };

}();



//=========================================================================Assigning Roles to Super Admin jQuery ====================================================================================//





var AssignRoleTable = function () {
    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }


            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                var jqSelect = $('select', nRow)
                $(jqSelect).removeAttr('disabled')
                jqTds[2].innerHTML = '<a class="edit" href="">Save</a> &nbsp;<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                var jqSelect = $('select', nRow)
                var jqTds = $('>td', nRow);
                super_admin_id = $(jqTds[0]).attr('super_admin_id');
                role = jqSelect[0].value;
                bootbox.confirm({
                    message: "Are you sure you want to update super admin role",
                    title: "Please Confirm",
                    callback:  function(doIt){
                        if(doIt){
                            $.ajax({
                                url: "/super_admin/assign_roles/"+super_admin_id,
                                type: 'PUT',
                                dataType: 'JSON',
                                data: {'id': super_admin_id,'role_id': role},
                                success: function(data){
                                    if(data.success=='1'){
                                        bootbox.alert({title: "Alert!", message: "Data saved successfully."});
                                        $(jqSelect[0]).attr('disabled', 'disabled');
                                        oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 2, false);
                                        oTable.fnDraw();
                                        nEditing = null;
                                    }else{
                                        bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                                    }
                                },
                                error: function(data){
                                    bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                                }
                            });
                        }
                    }
                });
            }

            var oTable = $('#assign-roles').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }
            });

            jQuery('#assign-roles_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#assign-roles_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

            var nEditing = null;


            $(document).on('click', '#assign-roles a.cancel', function (e) {
                e.preventDefault();
                restoreRow(oTable, nEditing);
                nEditing = null;
            });

            $(document).on('click', '#assign-roles a.edit', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    var jqTds = $('>td>a', nEditing);
                    if ($(jqTds[1]).attr("data-mode") == "new") {
                        oTable.fnDeleteRow(nEditing);
                        editRow(oTable, nRow);
                        nEditing = nRow;
                    } else {
                        restoreRow(oTable, nEditing);
                        editRow(oTable, nRow);
                        nEditing = nRow;
                    }
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };
}();


$(document).on('change', '#assign_role', function(event){
    id = $(this).val();
    url = '/super_admin//roles/get_permissions/'+id
    if(id==""){
        $("#roles_permissions").html("");
        return false
    }
    $("#roles_permissions").load(url);
} );