$(document).ready(function(){
    $(document).ajaxStart(function(){
        $("#wait").css("display","block");
    });
    $(document).ajaxComplete(function(){
        $("#wait").css("display","none");
    });

    $(document).on("keypress keyup", "input.floatclass", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1 ) && ((event.which!=8 && event.which < 48) || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('.confirm_paid_user').attr('disabled',true)
});

function enableConfirmButton(textfield){
    id = $(textfield).attr('id');
    str = id.replace("amount_paid_", "confirm_paid_user_");
    if($(textfield).val().length !=0)
        $("#"+str).attr('disabled', false);
    else
        $("#"+str).attr('disabled', true);
};


function confirm_payment_status(admin_id, status){
    amount_paid = jQuery("#amount_paid_" + admin_id).val();
    if(amount_paid==""){
        bootbox.alert({title: "Alert", message: "Please fill the amount paid by admin."});
        return false;
    }
    bootbox.confirm({
        message: "Do you want to proceed?",
        title: "Please Confirm",
        callback:  function(doIt){
            if(doIt){
               $.ajax({
                   url: "/6S2C2585/superadmin/update",
                   type: 'PUT',
                   dataType: 'JSON',
                   data: {'id': admin_id, 'status': status, 'amount_paid': amount_paid},
                   success: function(data){
                       if(data.success=='1'){
                           $("#restaurant_status_"+admin_id).html('').append('<button type="button" class="btn btn-default" disabled>Confirmed</button>&nbsp;<button type="button" class="btn btn-danger" onclick="confirm_payment_status('+admin_id+', 0)">Reject</button>')
                           $("#amount_paid_by_admin_"+admin_id).html("$ "+ amount_paid);
                       }else{
                           $("#restaurant_status_"+admin_id).html('').append('<button type="button" class="btn btn-success" id="confirm_paid_user_'+ admin_id +'" onclick="confirm_payment_status('+admin_id+', 1)" disabled="disabled" >Confirm</button>')
                           $("#amount_paid_by_admin_"+admin_id).html('').append('<div  class="input-group m-bot15"><span class="input-group-addon">$</span><input class="form-control input-sm m-bot15 floatclass" id="amount_paid_'+admin_id+'" name="amount_paid_'+admin_id+'" type="text" onkeyup= "enableConfirmButton(this)">');
                       }
                   },
                   error: function(data){

                   }
               });
            }
        }
    });
}

$(document).on('change', '#restaurantdetail_country', function(e){
    select_wrapper = $('#country_state_code_wrapper')
    $('select', select_wrapper).attr('disabled', true);
    country_code = $(this).val();
    if(country_code!=""){$('label[for=restaurantdetail_country]').remove();}
    url = "/admins/subregion_options?parent_region="+country_code;
    $('#country_state_code_wrapper').load(url);
    $('#superadmin_getcities').html('');
    $('#superadmin_getrestaurantdetails').html('');
    get_ads_details(null);
});

$(document).on('change', '#restaurantdetail_state', function(e){
    $('#superadmin_getcities').html('');
    $('#superadmin_getrestaurantdetails').html('');
    if($(this).val()!=""){$('label[for=restaurantdetail_state]').remove();}
    get_ads_details(null);
});

$(document).on('change', '#restaurantdetail_city', function(e){
    select_wrapper = $('#superadmin_getrestaurantdetails')
    $('select', select_wrapper).attr('disabled', true);
    city = $(this).val();
    country = $("#restaurantdetail_country").val();
    state = $("#restaurantdetail_state").val();
    $.ajax({
        url: "/6S2C2585/superadmin/get_restaurantdetails",
        type: 'GET',
        data: {'city': city, 'country':country, 'state': state},
        success: function(data){
            $('#superadmin_getrestaurantdetails').html(data);
        }
    });
    get_ads_details(null);
});

$(document).on('change', '#restaurantdetail_restaurant', function(e){
    select_wrapper = $('#super_admin_ad_details')
    $('select', select_wrapper).attr('disabled', true);
    restaurant=$("#restaurantdetail_restaurant").val();
    get_ads_details(restaurant);

});


function getrestaurantcity(){
   $('#superadmin_getrestaurantdetails').html('');
   country = $("#restaurantdetail_country").val();
   state = $("#restaurantdetail_state").val();
   if(country == "" || typeof country === "undefined"){
       $('label[for=restaurantdetail_country]').remove();
       $("#restaurantdetail_country").removeClass('error');

       $("#restaurantdetail_country").addClass('error');
       $("<label for='restaurantdetail_country' class='error'>Please select a country</label>").insertAfter("#restaurantdetail_country");
       return false;
   }else{$('label[for=restaurantdetail_country]').remove();}
   if(state =="" || typeof state === "undefined"){
       $('label[for=restaurantdetail_state]').remove();
       $("#restaurantdetail_state").removeClass('error');

       $("#restaurantdetail_state").addClass('error');
       $("<label for='restaurantdetail_state' class='error'>Please enter or select a state</label>").insertAfter("#restaurantdetail_state");
       return false;
   }else{$('label[for=restaurantdetail_state]').remove();}
    $('label[for=restaurantdetail_country]').remove();
    $('label[for=restaurantdetail_state]').remove();
    $("#restaurantdetail_country").removeClass('error');
    $("#restaurantdetail_state").removeClass('error');
    url = "/6S2C2585/superadmin/get_city?country="+country+"&state="+state
    $('#superadmin_getcities').load(url);
    get_ads_details(null);
};

$(document).on('click', '#super_admin_ad_form', function(event){
   event.preventDefault();
    id = $(this).find("#ad_id").val();
    ad_setting = $(this).find("#ad_setting").val();
    $.ajax({
        url: "/6S2C2585/superadmin/ad_settings",
        type: 'POST',
        dataType: 'JSON',
        data: {'id': id, 'ad_setting':ad_setting},
        success: function(data){
            if(data.success==true){
                restaurant=$("#restaurantdetail_restaurant").val();
                get_ads_details(restaurant);
            }
            else{
                bootbox.alert({title: "Alert!", message: data.success});
            }
        },
        error: function(data){

        }
    });
});

$(document).on('click', '#super_admin_ad_destroy_form', function(event){
    event.preventDefault();
    id = $("#ad_id").val();
    $.ajax({
        url: "/6S2C2585/superadmin/ad_destroy/"+id,
        type: 'DELETE',
        dataType: 'JSON',
        data: {'id': id},
        success: function(data){
            if(data.success==true){
                restaurant=$("#restaurantdetail_restaurant").val();
                get_ads_details(restaurant);
            }
            else{
                bootbox.alert({title: "Alert!", message: "Error while destroying ad detail."});
            }
        },
        error: function(data){

        }
    });
});

$(document).on('click', '#update_admin_id', function(event){
    event.preventDefault();
    id = $(this).attr('admin_id');
    $.ajax({
        url: "/6S2C2585/superadmin/update_admin_status/"+id,
        type: 'PUT',
        dataType: 'JSON',
        data: {'id': id},
        success: function(data){
            if(data.confirmed){
                $(".update_admin_id_"+id).html("Disable")
            }
            else{
                $(".update_admin_id_"+id).html("Enable")
            }
        },
        error: function(data){

        }
    });
});

function get_ads_details(restaurant){
    $.ajax({
        url: "/6S2C2585/superadmin/get_adsdetails",
        type: 'GET',
        data: {'id': restaurant},
        success: function(data){
            $('#super_admin_ad_details').html(data)
            $('#superadmin_ads_table').dataTable({
                "iDisplayLength": 10,
                "sPaginationType": "bootstrap",
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }
            });
            jQuery('#superadmin_ads_table_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#superadmin_ads_table_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown
        },
        error: function(data){
            $('#super_admin_ad_details').html(data)
            $('#superadmin_ads_table').dataTable().fnDraw();
        }

    })
}