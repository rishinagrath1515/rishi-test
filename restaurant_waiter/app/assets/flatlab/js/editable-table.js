var EditableTable = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function createRow(oTable, nRow){
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = addAdTypeTag(aData[1]);
                jqTds[2].innerHTML = '<input type="text" class="form-control small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control small floatclass" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<div class="input-group m-bot15" style="width: 125px;"><span class="input-group-addon">$</span><input type="text" class="form-control floatclass" value="' + aData[4] +'"></div>';
                jqTds[5].innerHTML = aData[5];
                jqTds[6].innerHTML = aData[6];
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = addAdTypeTag(aData[1]);
                jqTds[2].innerHTML = '<input type="text" class="form-control small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control small floatclass" value="' + aData[3] + '">';
                $(jqTds[4]).find('input').removeAttr('readonly');
                jqTds[5].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[6].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {

                var jqInputs = $('input', nRow);
                var jqSelect = $('select', nRow)
                var jqTds = $('>td', nRow);
                ad_id = $(jqTds[0]).attr('ad_id');
                name = jqInputs[0].value;
                short_name = jqInputs[1].value;
                no_of_days = jqInputs[2].value;
                ad_type = jqSelect[0].value;
                price = jqInputs[3].value;
                if(name == "" && price == ""){
                    bootbox.alert({title: "Alert!", message: "Please enter name and price"});
                    oTable.fnDraw();
                    return false
                }
                if(name == ""){
                    bootbox.alert({title: "Alert!", message: "Please enter name."});
                    oTable.fnDraw();
                    return false
                }
                if(short_name == ""){
                    bootbox.alert({title: "Alert!", message: "Please enter Short Name."});
                    oTable.fnDraw();
                    return false
                }
                if(no_of_days == ""){
                    bootbox.alert({title: "Alert!", message: "Please enter no of days."});
                    oTable.fnDraw();
                    return false
                }
                if(price == ""){
                    bootbox.alert({title: "Alert!", message: "Please enter price"});
                    oTable.fnDraw();
                    return false
                }
                if (typeof ad_id == "undefined"){
                    $.ajax({
                        url: "/6S2C2585/superadmin/ads_subscriptions/",
                        type: 'POST',
                        dataType: 'JSON',
                        data: {super_admin_ads_subscription: {'name': name, 'ad_type': ad_type, 'price': price, 'no_of_days':no_of_days,'short_name':short_name}},
                        success: function(data){
                            if(data.success=='1'){
                                bootbox.alert({title: "Alert!", message: "Data saved successfully."});
                                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                                $(jqTds[0]).attr('ad_id', data.ad_id);
                                oTable.fnUpdate(jqSelect[0].value, nRow, 1, false);
                                oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
                                oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
                                oTable.fnUpdate('<div class="input-group m-bot15" style="width: 125px;"><span class="input-group-addon">$</span><input type="text" class="form-control floatclass" value="'+jqInputs[3].value +'" readonly></div>', nRow, 4, false);
                                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
                                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 6, false);
                                oTable.fnDraw();
                                nEditing = null;
                            }else{
                                bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                            }
                        },
                        error: function(data){
                            bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                        }
                    });
                }else{
                    $.ajax({
                        url: "/6S2C2585/superadmin/ads_subscriptions/"+ad_id,
                        type: 'PUT',
                        dataType: 'JSON',
                        data: {super_admin_ads_subscription: {'name': name, 'ad_type': ad_type, 'price': price, 'id': ad_id, 'no_of_days':no_of_days,'short_name':short_name}},
                        success: function(data){
                            if(data.success=='1'){
                                bootbox.alert({title: "Alert!", message: "Data saved successfully."});
                                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                                oTable.fnUpdate(jqSelect[0].value, nRow, 1, false);
                                oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
                                oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
                                oTable.fnUpdate('<div class="input-group m-bot15" style="width: 125px;"><span class="input-group-addon">$</span><input type="text" class="form-control floatclass" value="'+jqInputs[3].value +'" readonly></div>', nRow, 4, false);
                                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
                                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 6, false);
                                oTable.fnDraw();
                                nEditing = null;
                            }else{
                                bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                            }
                        },
                        error: function(data){
                            bootbox.alert({title: "Alert!", message: "Error while saving the data."});
                        }
                    });
                }
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 3, false);
                oTable.fnDraw();
            }

            var oTable = $('#editable-sample').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }
            });

            jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

            var nEditing = null;

            $(document).on('click', '#editable-sample_new', function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '','','',
                        '<a class="edit" href="">Save</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                if (nEditing !== null){
                    var jqTds = $('>td>a', nEditing);
                    if ($(jqTds[1]).attr("data-mode") == "new") {
                        oTable.fnDeleteRow(nEditing);
                    } else {
                        restoreRow(oTable, nEditing);
                    }
                }
                createRow(oTable, nRow);
                nEditing = nRow;
            });

            $(document).on('click', '#editable-sample a.delete', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var jqTds = $('>td', nRow);
                ad_id = $(jqTds[0]).attr('ad_id');
                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }
                $.ajax({
                    url: "/6S2C2585/superadmin/ads_subscriptions/"+ad_id,
                    type: 'DELETE',
                    dataType: 'JSON',
                    data: {super_admin_ads_subscription: {'id': ad_id}},
                    success: function(data){
                        if(data.success=='1'){

                            oTable.fnDeleteRow(nRow);
                            bootbox.alert({title: "Alert!", message: "Data deleted successfully."});
                        }else{
                            bootbox.alert({title: "Alert!", message: "Error while deleting the row."});
                        }
                    },
                    error: function(data){
                        alert('error')
                    }
                });

            });

            $(document).on('click', '#editable-sample a.cancel', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    nEditing = null;
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $(document).on('click', '#editable-sample a.edit', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    var jqTds = $('>td>a', nEditing);
                    if ($(jqTds[1]).attr("data-mode") == "new") {
                        oTable.fnDeleteRow(nEditing);
                        editRow(oTable, nRow);
                        nEditing = nRow;
                    } else {
                        restoreRow(oTable, nEditing);
                        editRow(oTable, nRow);
                        nEditing = nRow;
                    }
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });

            function addAdTypeTag(selected){
                if(selected=='Image'){
                    return '<select class="form-control small select_subscription_class">' +
                        '<option selected="selected" value="Image">Image</option>' +
                        '<option value="Video">Video</option>' +
                        '</select>'
                }else if(selected=='Video'){
                    return '<select class="form-control small select_subscription_class">' +
                        '<option value="Image">Image</option>' +
                        '<option selected="selected" value="Video">Video</option>' +
                        '</select>'
                }else{
                    return '<select class="form-control small select_subscription_class">' +
                                '<option value="Image">Image</option>' +
                                '<option value="Video">Video</option>' +
                        '</select>'
                }
            }
        }

    };

}();