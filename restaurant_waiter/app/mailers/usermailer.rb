class Usermailer < ActionMailer::Base
  default from: "obronregister@obronwaiter.com"

  def forgot_password(user)
    @user = user
    @url  = 'https://obronwaiter.com/userinfo/resetpassword?token='+@user.reset_password_token
    mail(to: @user.email, subject: 'Reset Password')
  end
  
  def forgot_admin_password(user)
    @admin = user
    @url  = 'https://obronwaiter.com/admins/settings/resetpassword?token='+@admin.reset_password_token
    mail(to: @admin.email, subject: 'Reset Password')
  end
end
