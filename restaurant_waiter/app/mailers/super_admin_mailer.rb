class SuperAdminMailer < ActionMailer::Base
  default from: "obronregister@obronwaiter.com"

  def admin_creation_mail(admin,request,admin_password)
    @admin = admin
    @request = request
    @admin_password = admin_password
    @name = admin.name
    mail(:from => "obronregister@obronwaiter.com", :to => "#{admin.email} <#{admin.email}>", :subject => "Registration waiting to be process")
  end

  def send_mail_to_sales(admin,request,admin_password)
    @admin = admin
    @request = request
    @admin_password = admin_password
    @name = admin.name
    mail(:from => "obronregister@obronwaiter.com", :to => "sales@obronwaiter.com", :subject => "New User Registered")
  end

  def send_confirmation_mail(admin)
    @admin = admin
    mail(:from => "obronregister@obronwaiter.com", :to => "#{admin.email} <#{admin.email}>", :subject => "Payment Confirmation")
  end
end
