class Orderdetail < ActiveRecord::Base
  has_many :fooditems, :class_name => 'Fooditem', :primary_key => 'food_id', :foreign_key => 'id'
  belongs_to :variant, :class_name => 'Size', :primary_key => 'id', :foreign_key => 'size'
end
