class Restaurantdetail < ActiveRecord::Base
 acts_as_mappable :default_units => :miles,
                   :default_formula => :sphere,
                   :distance_field_name => :distance,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude
                   
                   
 has_attached_file :excel,
     :s3_protocol => 'http',
     :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
     :s3_permissions => :public_read,
     :url            => "waiterimages.s3.amazonaws.com",
     :path => "excel_sheets/:id/:basename.:extension",
     :s3_host_name=>'s3-us-west-2.amazonaws.com',
     :default_url=>"/Restaurant_Waiter_Data.xls"
     
 validates_attachment :excel, content_type: { content_type: ".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" }

 validates :address1, :country, :state, :city, :presence => true
 validates :restaurant_email, :uniqueness =>  true

 after_validation :set_lat_lng, if: ->(obj){ (obj.address1_changed?  || obj.address2_changed? || obj.address3_changed? ) || obj.country_changed? || obj.state_changed? || obj.city_changed? || obj.zip_changed? }
 after_validation :set_restaurant_id, if: ->(obj){ (obj.restaurant_name_changed?  || obj.address1_changed?  ||  obj.zip_changed? )}

 def admin
   Admin.where(:restaurant_id=> self.id).first rescue nil
 end

 def set_lat_lng
   begin
      location = Restaurantdetail.geocode(self.full_street_address)
      self.latitude = location.lat
      self.longitude = location.lng
   rescue
   end
 end

 def set_restaurant_id
    self.rid = self.restaurant_name.gsub(/\s+/, "").first(3).upcase << self.address1.gsub(/\s+/, "").first(4).upcase << self.zip.to_s.gsub(/\s+/, "").last(3).upcase
 end

 def full_street_address
   "#{self.address1.to_s}, #{self.address2.to_s}, #{self.address3.to_s}, #{self.city.to_s}, #{self.statename}, #{self.countryname}, #{self.zip.to_s}"
 end

 def countryname
   country = Carmen::Country.coded(self.country)
   country.name.to_ascii rescue ''
 end

 def statename
   country = Carmen::Country.coded(self.country)
   country.name rescue ''
   country.subregions.coded(self.state).name.to_ascii rescue self.state.to_ascii
 end 
end
