class Extra < ActiveRecord::Base
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "142x142#" }, :default_url => "/images/:style/missing.png",
                    :storage        => :s3,
                    :s3_protocol => 'http',
                    :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
                    :s3_permissions => :public_read,
                    :url            => "waiterimages.s3.amazonaws.com",
                    :path => "extras_images/:id/:style/:basename.:extension",
                    :s3_host_name=>'s3-us-west-2.amazonaws.com'
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
end
