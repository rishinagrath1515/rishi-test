class Advertisement < ActiveRecord::Base
  has_attached_file :avatar, :styles =>  lambda { |a| a.instance.is_image? ? {:thumb => "243x243#", :medium => "243x243#", :large => "x400>"}  : {:thumb => { :geometry => "640x480", :format => 'jpg', :time => 10}, :medium => { :geometry => "640x480", :format => 'mp4', :time => 10}}},
    processors: lambda { |a| a.is_video? ? [ :ffmpeg ] : [ :thumbnail ] },
                    :storage        => :s3,
                    :s3_protocol => 'http',
                    :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
                    :s3_permissions => :public_read,
                    :url            => "waiterimages.s3.amazonaws.com",
                    :path => "advertisement/:id/:style/:basename.:extension",
                    :s3_host_name=>'s3-us-west-2.amazonaws.com'
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/,:if => :is_image?
  validates_attachment_content_type :avatar,  :content_type => /.*/,:if => :is_video?
   validates_attachment_size :avatar, :less_than => 500.kilobytes,:if => :is_image?,message: "Please upload image of size less or equal to 500 kb."
  belongs_to :ads_subscription

  def is_video?
     avatar.content_type =~ %r(video)
  end

  def is_image?
    avatar.content_type=~ %r(image)
  end

  def ads_subscription
    AdsSubscription.where(:id => self.subscription_type).first rescue nil
  end
end
