class Order < ActiveRecord::Base
  has_many :orderdetails, :class_name => 'Orderdetail', :primary_key => 'id', :foreign_key => 'order_id'
  has_many :fooditems, lambda { uniq }, :through => :orderdetails
  has_many :orderdetailswithextras, :class_name => 'Orderdetailswithextra', :primary_key => 'id', :foreign_key => 'order_id'
  has_many :orderdetailswithdrinkses, :class_name => 'Orderdetailswithdrinks', :primary_key => 'id', :foreign_key => 'order_id'
  has_many :drinks, lambda { uniq }, :through => :orderdetailswithdrinkses
  belongs_to :table, :class_name => "Restauranttable", :primary_key => "id", :foreign_key => 'table_id'
  before_save :todo

  def todo
    self.gratuity = 0
    self.tax=0
    self.gratuity = 0
    self.tip = 0
    #self.confirm_status=0
    #self.pay_status=0
  end

  def user
    User.find_by_id(self.user_id) rescue nil
  end

  def restaurant
    Restaurantdetail.find_by_id(self.restaurant_id) rescue nil
  end
end
