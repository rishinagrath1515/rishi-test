class SuperAdmin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  belongs_to :role

  def has_role?(role)
    self.role.name.parameterize.underscore.to_sym ==  role rescue false
  end
end
