class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  cattr_accessor :skip_callbacks
 # skip_callback  :before_save , if: :skip_some_callbacks
  
  validates_presence_of :email
  validates_uniqueness_of :email

  validates_presence_of :username
  #validates_uniqueness_of :username

  validates_presence_of :password, :if => :password_required?

  validates_presence_of :device_type
  validates_presence_of :device_token
  
 # has_attached_file :avtar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
 # validates_attachment_content_type :avtar, :content_type => /\Aimage\/.*\Z/
  has_attached_file :avtar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png",
                    :storage        => :s3,
                    :s3_protocol => 'http',
                    :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
                    :s3_permissions => :public_read,
                    :url            => "waiterimages.s3.amazonaws.com",
                    :path => "user_images/:id/:style/:basename.:extension",
                    :s3_host_name=>'s3-us-west-2.amazonaws.com'
  validates_attachment_content_type :avtar, :content_type => /\Aimage\/.*\Z/
  
  validates_presence_of :app_version

  before_save :welcome ,unless: :skip_callbacks

  def welcome
    self.user_access_token= Digest::MD5.hexdigest(email+Time.now.strftime("%d/%m/%Y %H:%M"))
    self.is_admin=0
  end
  
   def password_required?
    password.present?
  end
  
  def send_password_reset
  generate_token(:reset_password_token)
  self.reset_password_sent_at = Time.zone.now
  save!
  end

 def generate_token(column)
   begin
     self[column] = SecureRandom.urlsafe_base64
     end while User.exists?(column => self[column])
end
end
