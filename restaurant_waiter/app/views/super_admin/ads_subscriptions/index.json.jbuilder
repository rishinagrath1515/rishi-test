json.array!(@super_admin_ads_subscriptions) do |super_admin_ads_subscription|
  json.extract! super_admin_ads_subscription, :id
  json.url super_admin_ads_subscription_url(super_admin_ads_subscription, format: :json)
end
