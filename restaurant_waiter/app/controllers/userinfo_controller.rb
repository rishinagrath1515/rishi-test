class UserinfoController < ApplicationController
  def getuserdetails
  end

  def getuserinfo
    if (params[:useraccesstoken].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      if (@isUseAuthorized.fb_id).present? && (@isUseAuthorized.fb_access_token).present?
        @isUseAuthorized.image= @isUseAuthorized.image
        imageFrom=1
      else
        if  (@isUseAuthorized.avtar).present?
          @isUseAuthorized.image= @isUseAuthorized.avtar.url
        else
          @isUseAuthorized.image= root_url[0..-1]+"default.jpg"
        end
        imageFrom=0
      end
      render :json => JSON.pretty_generate({"data" => {'user_id' => @isUseAuthorized.id, "username" => @isUseAuthorized.username, "email" => @isUseAuthorized.email, "imageFrom" => imageFrom, "image" => @isUseAuthorized.image}})
    end
  end


  def uploadimage
    if (params[:useraccesstoken].blank?) || (params[:avtar].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      image=params[:avtar]
      #img_name = image['datafile'].original_filename
      #imageArray=img_name.split('.')
      #render json: {'here'=>imageArray} and return
      # extension=imageArray[1]
      # if (extension=="jpeg") ||(extension=="png") ||(extension=="gif") ||(extension=="jpg") then
      #  encrypted_image_name=save(image)
      User.skip_callbacks = true
      isUpdated= @isUseAuthorized.update_attribute(:avtar, image) #User.where(:id =>@isUseAuthorized.id).update_all(:image => encrypted_image_name)
      if isUpdated then
        render :json => JSON.pretty_generate({"log" => "Image has been uploaded successfully", "imagepath" => @isUseAuthorized.avtar.url})
      else
        render :json => JSON.pretty_generate({"error" => "Unable to upload image. Please try again later."})
      end

      #else
      #render :json => JSON.pretty_generate({"error" => "Invalid image."})
      #end
    end
  end


  def forgotpassword
    if (params[:email].blank?) then
      render :json => JSON.pretty_generate({"error" => 'some parameters missing'}) and return
    else
      @user = User.find_for_database_authentication email: params[:email]
      return unauthorized_user unless @user
      @user.send_password_reset
      if (Usermailer.forgot_password(@user).deliver) then
        render :json => JSON.pretty_generate({"log" => "Email has been sent successfully. Please check your mailbox to reset password."})
      else
        render :json => JSON.pretty_generate({"error" => "Unable to send mail. Please try again later."})
      end
    end
  end


  def resetpassword
  end


  def save(upload)
    img_name = upload['datafile'].original_filename
    directory = "public/userimages/"
    # create the file path
    @random=SecureRandom.urlsafe_base64
    encrypted_image_name=@random+"$101$"+img_name
    path = File.join(directory, encrypted_image_name)
    File.open(path, "wb") { |f| f.write(upload['datafile'].read) }
    return encrypted_image_name;
  end



 def accesstokenlogin
    if (params[:useraccesstoken].blank?) || (params[:app_version].blank?) || (params[:device_type].blank?) || (params[:device_token].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      User.where(:user_access_token => params[:useraccesstoken]).update_all(:device_type => params[:device_type], :device_token => params[:device_token],:last_sign_in_at => Time.now())
      appVersion=Appverion.last
      currentVersion= params[:device_type] == 1 ? appVersion['android_app_version'] : appVersion['ios_app_version']
      forceUpdate = params[:device_type] == 1 ? appVersion['android_force_update'] : appVersion['ios_force_update']
      if ((@isUseAuthorized.app_version).to_i) != (params[:app_version].to_i) then
        User.where('id=?',@isUseAuthorized.id).update_all(:app_version=>params[:app_version])
        if (params[:app_version].to_i) <= (currentVersion.to_i)
          showPopup=1
        else
          showPopup=0
        end
      else
        if ((@isUseAuthorized.app_version).to_i) < (currentVersion.to_i)then
          showPopup=1
        else
          showPopup=0
        end
      end
      render :json => JSON.pretty_generate({"log" => "1","useraccesstoken"=>@isUseAuthorized.user_access_token,'cc_is_registered'=>@isUseAuthorized.cc_is_registered,'username'=>@isUseAuthorized.username,'popup'=>showPopup,'forceupdate'=>forceUpdate})
    end
  end

=begin
  def accesstokenlogin
    if (params[:useraccesstoken].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      User.where(:user_access_token => params[:useraccesstoken]).update_all(:last_sign_in_at => Time.now())
      render :json => JSON.pretty_generate({"log" => "1", "useraccesstoken" => @isUseAuthorized.user_access_token, 'cc_is_registered' => @isUseAuthorized.cc_is_registered, 'username' => @isUseAuthorized.username})
    end
  end
=end


  private
  def invalid_params_msg
    render :json => JSON.pretty_generate({"error" => "some parameter missing"})
  end

  def unauthorized_user
    render :json => JSON.pretty_generate({"error" => "Unauthorized access."})
  end

  def person_params
    params.permit(:avtar)
  end
end
