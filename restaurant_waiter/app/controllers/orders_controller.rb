=begin
  @ Description :This Contains all the methods related to the orders placed by user.
  @ Copyright :
  @ Developed by : Click Labs Pvt. Ltd
=end

class OrdersController < ApplicationController
  include ActionView::Helpers::NumberHelper
#to test methods of restaurant class(shift control to test file)
  def orderdetails
  end

=begin
  @ Description :  Add order of the user which can be
           ->food items, extras and drinks
           ->or only drinks
=end
  def addorder
    if (params[:useraccesstoken].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      allFoddItems=params[:fooditemid]=="0" ? params[:fooditemid]: params[:fooditemid].split('###');

      allExtrasId=params[:extrasid]=="0" ? params[:extrasid]: params[:extrasid].split('$$$');

      allExtrasquantity=params[:extrasquantity].split('$$$');
      allExtrasprice=params[:extrasprice].split('$$$');
      price=params[:price].split('###');
      size=params[:size].split('###');
      quantity=params[:quantity].split('###');
      alldrinksId=params[:drinksid]=="0" ? params[:drinksid]: params[:drinksid].split('###');
      alldrinkSizes=params[:drinksizes].split('###');
      alldrinkquantity=params[:drinkquantity].split('###');
      alldrinkprice=params[:drinkprice].split('###');
      deletedItems=Array.new

      if allFoddItems != "0" && allFoddItems.length then
        allFoddItems.each do |val|
        if val != "0" then
          isExisting= Fooditem.where("id=?", val)
          deletedFoodItem=Hash.new
          if isExisting.blank? then
            deletedFoodItem[:id]=val
            deletedFoodItem[:type]=1
            deletedItems<<deletedFoodItem
          end
        end
        end
        #render :json => JSON.pretty_generate({"data" => deletedFoodItems}) and return
      end

      if allExtrasId != "0" && allExtrasId.length  then

        allExtrasId.each do |val|
          allExtrasIdInFoodItem=val.split('###');
          if allExtrasIdInFoodItem.length then
            allExtrasIdInFoodItem.each do |value|
             if value != "0" then
              isExisting= Extra.where("id=?", value)
              deletedExtraItem=Hash.new
              if isExisting.blank? then
                  deletedExtraItem[:id]=value
                  deletedExtraItem[:type]=2
                  deletedItems<<deletedExtraItem
              end
            end
            end
          end
        end
      end

      if alldrinksId != "0" && alldrinksId.length then
        alldrinksId.each do |value|
        if value != "0" then
          isExisting= Alldrinks.where("id=?", value)
          deletedDrinkItem=Hash.new
          if isExisting.blank? then
            deletedDrinkItem[:id]=value
            deletedDrinkItem[:type]=3
            deletedItems<<deletedDrinkItem
          end
        end
        end
      end

      if deletedItems.length>0 then
       render :json => JSON.pretty_generate({"data" => deletedItems}) and return
      end

      gratuityInfo=Restaurantdetail.where("id=?", params[:restaurant_id])
      if params[:orderid].present? then
        orderInfo=Order.find(params[:orderid])
        if orderInfo.pay_status== 0 then
          #lastgratuity=(gratuityInfo[0]['gratuity']/100)*(((orderInfo.total_bill)).to_f)
          totalBill=((orderInfo.total_bill).to_f)+ (params[:total_bill].to_f)
          Order.where('id=?', params[:orderid]).update_all(:total_bill => totalBill, :pending_bill => totalBill)
          orderId=params[:orderid]
        else
          newOrder= Order.new(:user_id => @isUseAuthorized.id, :total_bill => params[:total_bill], :pending_bill => params[:total_bill], :restaurant_id => params[:restaurant_id])
          newOrder.save
          orderId=newOrder.id
          # lastgratuity=0
        end
      else
        newOrder= Order.new(:user_id => @isUseAuthorized.id, :total_bill => params[:total_bill], :pending_bill => params[:total_bill], :restaurant_id => params[:restaurant_id])
        newOrder.save
        orderId=newOrder.id
        # lastgratuity=0
      end

      if orderId then
        if gratuityInfo.present? then
          if (allFoddItems.length) == (allExtrasId.length) then
            i=0
            while i < (allFoddItems.length) do
              if allFoddItems[i]!="0" then
                newOrderDetail= Orderdetail.new(:order_id => orderId, :food_id => allFoddItems[i], :quantity => quantity[i], :price => price[i], :size => size[i])
                newOrderDetail.save

                allExtrasIdInFoodItem=allExtrasId[i].split('###');
                allExtrasQuantityInFoodItem=allExtrasquantity[i].split('###');
                allExtrasPriceInFoodItem=allExtrasprice[i].split('###');
                j=0
                while j < (allExtrasIdInFoodItem.length) do
                  if allExtrasIdInFoodItem[j]!= "0" then
                    (Orderdetailswithextra.new(:order_id => orderId, :food_id => allFoddItems[i], :extras_id => allExtrasIdInFoodItem[j], :quantity => allExtrasQuantityInFoodItem[j], :price => allExtrasPriceInFoodItem[j])).save
                  end
                  j+=1
                end
              end
              i +=1
            end
=begin
          allDrinksIdInFoodItem=alldrinksId[i].split('###');
          alldrinkSizesInFoodItem=alldrinkSizes[i].split('###');
          alldrinkQuantityInFoodItem=alldrinkquantity[i].split('###');
          alldrinkPriceInFoodItem=alldrinkprice[i].split('###');
=end
            if alldrinksId.length then
              k=0
              while k < (alldrinksId.length) do
                if alldrinksId[k]!= "0" then
                  (Orderdetailswithdrinks.new(:order_id => orderId, :food_id => 0, :drink_id => alldrinksId[k], :drink_size => alldrinkSizes[k], :quantity => alldrinkquantity[k], :price => alldrinkprice[k])).save
                end
                k+=1
              end
            end
            orderInfowithupdatedbill=Order.find(orderId)

            #if gratuityInfo[0]['gratuity_status'] then
             # totalgratuity=(gratuityInfo[0]['gratuity']/100)*((orderInfowithupdatedbill.total_bill).to_f)
             # render :json => JSON.pretty_generate({"log" => "1", "order_id" => orderId, "gratuity" => totalgratuity, "total_bill" => orderInfowithupdatedbill.total_bill})
            #else
             # render :json => JSON.pretty_generate({"log" => "1", "order_id" => orderId, "gratuity" => 0, "total_bill" => orderInfowithupdatedbill.total_bill})
           # end
            if gratuityInfo[0]['gratuity_status']==1 then
              totalgratuity=(gratuityInfo[0]['gratuity']/100)*((orderInfowithupdatedbill.total_bill).to_f)
              #totalgratuity=temptotalgratuity+lastgratuity
              render :json => JSON.pretty_generate({"log" => "1", "order_id" => orderId, "gratuity" => totalgratuity, "total_bill" => orderInfowithupdatedbill.total_bill})
            elsif gratuityInfo[0]['gratuity_status']==2 then
              ordersCount=Order.where("user_id=? && restaurant_id=?",@isUseAuthorized.id,params[:restaurant_id])
              if ordersCount.size >= 8 then
                totalgratuity=(gratuityInfo[0]['gratuity']/100)*((orderInfowithupdatedbill.total_bill).to_f)
                render :json => JSON.pretty_generate({"log" => "1", "order_id" => orderId, "gratuity" => totalgratuity, "total_bill" => orderInfowithupdatedbill.total_bill})
              else
                render :json => JSON.pretty_generate({"log" => "1", "order_id" => orderId, "gratuity" => 0, "total_bill" => orderInfowithupdatedbill.total_bill})
              end
            else
              render :json => JSON.pretty_generate({"log" => "1", "order_id" => orderId, "gratuity" => 0, "total_bill" => orderInfowithupdatedbill.total_bill})
            end
          else
            render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
          end
        else
          render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
        end
      end
    end
  end

#end  addorder


#API NOT IN USE
  def revieworder
    if (params[:useraccesstoken].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      orderId=Order.select("id,total_bill").where("user_id=? && confirm_status=?", @isUseAuthorized.id, 0)
      allId=Array.new;
      orderId.each do |val|
        allId<<val['id']
      end
      allOrderDetails=Orderdetail.find(:all, :conditions => ["order_id IN (?)", allId])
      if allOrderDetails.present? then
        allOrderDetailsArray=allOrderDetails.to_a.map(&:serializable_hash)
        allOrderDetailsArray.each do |val|
          allFoodDetails=Fooditem.select(" id,food_name,image").where("id=?", val['food_id']).limit(1)
          val[:food_name]=allFoodDetails.first['food_name']
          val[:food_image]=root_url[0..-1]+allFoodDetails.first['image']
        end
        render :json => JSON.pretty_generate({"data" => {'allorderdetails' => allOrderDetailsArray, 'total_bill' => orderId[0]['total_bill']}})
      else
        render :json => JSON.pretty_generate({"error" => "No order details available."})
      end
    end
  end

#end  revieworder


#API NOT IN USE
  def confirmorder
    if (params[:useraccesstoken].blank?) || (params[:orderid].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      isUpdated=Order.where(:id => params[:orderid]).update_all(:confirm_status => 1);
      if isUpdated then
        render :json => JSON.pretty_generate({"log" => "your order has been confirmed successfully"})
      else
        render :json => JSON.pretty_generate({"error" => "unable to update your order"})
      end
    end
  end

#end  confirmorder


=begin
  @ Description : Get history of orders of a user
  @input : useraccesstoken,
  @output : get list of all orders placed by user
=end
  def history
    if (params[:useraccesstoken].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      allOrders=Order.select("id,gratuity,tip,total_bill,restaurant_id,DATE_FORMAT(created_at,'%d %M, %Y') as created_at").where("user_id=? && table_id IS NOT NULL", @isUseAuthorized.id);

      if allOrders.present? then
        allOrders=allOrders.to_a.map(&:serializable_hash)
        allOrders.each do |val|
          restaurantName=Restaurantdetail.find(val['restaurant_id']);
          val[:restaurant_name]=restaurantName.restaurant_name
          totalBill=val['total_bill']
          val.delete('total_bill');
          #totalgratuity= ((restaurantName.gratuity)/100) * (totalBill.to_f)
          #val[:total_bill]=(totalBill.to_f)+(totalgratuity)+(val['tip'].to_f)
          val[:total_bill]=(totalBill.to_f)
          val[:total_bill] = number_with_precision(val[:total_bill], precision: 2)
        end
        render :json => JSON.pretty_generate({"data" => allOrders})
      else
        render :json => JSON.pretty_generate({"error" => "No order history is available."})
      end
    end
  end

#end  history

=begin
    @ Description : Get history of orders for which user will pay later
    @input : useraccesstoken
=end
  def paylaterhistory
    if (params[:useraccesstoken].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      allOrders=Order.select("id,gratuity,tip,total_bill,restaurant_id,DATE_FORMAT(created_at,'%d %M, %Y') as created_at").where("user_id=? && table_id IS NOT NULL && pay_status=0", @isUseAuthorized.id);

      if allOrders.present? then
        allOrders=allOrders.to_a.map(&:serializable_hash)
        allOrders.each do |val|
          restaurantName=Restaurantdetail.find(val['restaurant_id']);
          val[:restaurant_name]=restaurantName.restaurant_name
          totalBill=val['total_bill']
          val.delete('total_bill');
          totalgratuity= ((restaurantName.gratuity)/100) * (totalBill.to_f)
          val[:total_bill]=(totalBill.to_f)+(totalgratuity)+(val['tip'].to_f)
          val[:total_bill] = number_with_precision(val[:total_bill], precision: 2)
        end
        render :json => JSON.pretty_generate({"data" => allOrders})
      else
        render :json => JSON.pretty_generate({"error" => "No order history is available."})
      end
    end
  end

#end  paylaterhistory

=begin
  @ Description : Check if restaurant kitchen and bar are open or not
  @input : useraccesstoken,restaurant_id
  @output : send flags for each kitchen and bar
=end
  def checkrestauranttimings
    if (params[:useraccesstoken].blank?) || (params[:restaurant_id].blank?) || (params[:timezone].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      today=((Time.now.in_time_zone(params[:timezone])).wday)
      nextday = today+1
      nextDayCheck = nextday==7 ? 0 : nextday
      restaurantDetails=Restauranttiming.select('id,day,opening_time,closing_time,bar_opening_time,bar_closing_time,is_happy_hours_available,happy_hour_start_time,happy_hour_end_time').where("(restaurant_id=? && day=?) || (restaurant_id=? && day=?) ", params[:restaurant_id], today, params[:restaurant_id], nextDayCheck) #((Time.new).wday)
      #render json: {'here'=>restaurantDetails} and return
      if restaurantDetails.present?
        if restaurantDetails[0]['day']== today then
          restaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
          restaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
          barOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
          barClosingTime=convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])
          next_day_opening_time = convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time'])
          next_day_closing_time = convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time'])
        else
          restaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time'])
          restaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time'])
          barOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[1]['bar_opening_time'])
          barClosingTime=convertutctolocal(params[:timezone],restaurantDetails[1]['bar_closing_time'])
          next_day_opening_time = convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
          next_day_closing_time = convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
        end
        currentTime= (Time.now.in_time_zone(params[:timezone]))
        ishappyhouravailable=Restaurantdetail.where('id=?', params[:restaurant_id]).limit(1)
        if ishappyhouravailable[0]['is_happy_hour_available']== 1 then
          happyhourstimings=Restauranthappyhour.where('restaurant_id=?', params[:restaurant_id])
          isHappyHoursStarted=0;
          happyhourstimings.each do |val|
            localStartTime=convertutctolocal(params[:timezone],val['start_time'])
            localEndTime=convertutctolocal(params[:timezone],val['end_time'])
            if (localEndTime.strftime("%Y-%m-%d")) == (localStartTime.strftime("%Y-%m-%d")) then
              finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
              finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
            elsif (localEndTime.strftime("%Y-%m-%d")) > (localStartTime.strftime("%Y-%m-%d")) then
              nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
              finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
              finalEndTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
            else
              finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
              finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
            end
            if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalStartTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalEndTime) then
              isHappyHoursStarted=1
              break
            else
              isHappyHoursStarted=0
            end
          end
        else
          isHappyHoursStarted=0
        end


        if (restaurantClosingTime.strftime("%Y-%m-%d")) == (restaurantOpeningTime.strftime("%Y-%m-%d")) then
          finalRestaurantClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
          finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
          time_diff_components_for_restaurant = Time.diff(finalRestaurantClosingTime, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))

        elsif (restaurantClosingTime.strftime("%Y-%m-%d")) > (restaurantOpeningTime.strftime("%Y-%m-%d")) then
          nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
          finalRestaurantClosingTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
          finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
          time_diff_components_for_restaurant = Time.diff(finalRestaurantClosingTime, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))
        else
          finalRestaurantClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
          finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
          time_diff_components_for_restaurant = Time.diff(finalRestaurantClosingTime, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))
        end

        if ((currentTime.strftime("%Y-%m-%d %H:%M:%S"))  >= finalRestaurantOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S"))  <= (finalRestaurantClosingTime)) then
          if time_diff_components_for_restaurant[:hour]== 0 && time_diff_components_for_restaurant[:minute]<=15 then
            canOrderFood=0;
          else
            canOrderFood=1;
          end
        else
          canOrderFood=0;
        end

        if (barClosingTime.strftime("%Y-%m-%d")) == (barOpeningTime.strftime("%Y-%m-%d")) then
          finalBarClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (barClosingTime.strftime("%H:%M:%S"))
          finalBarOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (barOpeningTime.strftime("%H:%M:%S"))
        elsif (barClosingTime.strftime("%Y-%m-%d")) > (barOpeningTime.strftime("%Y-%m-%d")) then
          nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
          finalBarClosingTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (barClosingTime.strftime("%H:%M:%S"))
          finalBarOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (barOpeningTime.strftime("%H:%M:%S"))
        else
          finalBarClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (barClosingTime.strftime("%H:%M:%S"))
          finalBarOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (barOpeningTime.strftime("%H:%M:%S"))
        end

        time_diff_components_for_bar = Time.diff(finalBarClosingTime, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))
        time_diff_components_for_restaurant1 = Time.diff(finalRestaurantClosingTime, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))
        if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalBarOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalBarClosingTime)) && canOrderFood == 1 then #bar open and kitchen open
          if time_diff_components_for_bar[:day]== 0 && time_diff_components_for_bar[:hour]== 0 && time_diff_components_for_bar[:minute]<=15 then
            canOrderDrink=0;
          else
            canOrderDrink=1;
          end
        elsif ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalBarOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalBarClosingTime)) && canOrderFood == 0 then #bar open and kitchen close

          if time_diff_components_for_bar[:day]== 0 && time_diff_components_for_bar[:hour]== 0 && time_diff_components_for_bar[:minute]<=15 then
            canOrderDrink=0;
          else
            canOrderDrink=1;
          end
        elsif !(((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalBarOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalBarClosingTime))) && canOrderFood == 1 then #bar close and kitchen open
          if time_diff_components_for_restaurant1[:day]== 0 && time_diff_components_for_restaurant1[:hour]== 0 && time_diff_components_for_restaurant1[:minute]<=15 then
            canOrderDrink=0;
          else
            canOrderDrink=1;
          end

        elsif !(((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalBarOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalBarClosingTime))) && canOrderFood == 0 then #bar close and kitchen close
          canOrderDrink=0;
        end

        render :json => JSON.pretty_generate({"data" => {'canOrderFood' => canOrderFood, 'canOrderDrink' => canOrderDrink, 'isHappyHoursStarted' => isHappyHoursStarted, 'next_day_opening_time' => (next_day_opening_time.strftime("%H:%M:%S")), 'next_day_closing_time' => (next_day_closing_time.strftime("%H:%M:%S"))}})
      else
        render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
      end
    end
  end

#end  checkrestauranttimings


  def convertutctolocal(timezone, date)
    offset= Time.now.in_time_zone(timezone).utc_offset
    totaltime = Time.now.in_time_zone(timezone)
    utctime=''
    if (totaltime.to_s).include? "+"
      if (offset.to_s).include? "+" then
        finaloffset=(offset.to_s).split("+")
        utctime= date + (finaloffset[1].to_i).to_i.seconds
      else
        utctime= date + (offset.to_i).to_i.seconds
      end
    else
      if (offset.to_s).include? "-" then
        finaloffset=(offset.to_s).split("-")
        utctime= date - (finaloffset[1].to_i).to_i.seconds
      else
        utctime= date - (offset.to_i).to_i.seconds
      end
    end
    return utctime
  end

  private
#error in case invalid parameters passed.
  def invalid_params_msg
    render :json => JSON.pretty_generate({"error" => "some parameter missing"})
  end

#error in case invalid access token(unauthorized access).
  def unauthorized_user
    render :json => JSON.pretty_generate({"error" => "Unauthorized access."})
  end


end
