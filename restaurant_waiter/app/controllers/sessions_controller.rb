=begin
  @ Description :This Contains all the methods related to the User login.
   This is to override the Devise Session Controller.
  @ Copyright :
  @ Developed by : Click Labs Pvt. Ltd
=end


class SessionsController < ApplicationController

#to test methods of Session controller(shift control to test file)
  def new
  end


=begin
  @ Description :  Here Login is of two types(using email +facebook login)
  @input : email,password,device_type and device_token(in case of email login)
           fb_id, fb_access_token,device_type and device_token(in case of facebook login)
  @output : send usesaccesstoken in case of successful login else error
=end
  def create
    if params[:logintype]=="0"
      if (params[:email].blank?) || (params[:password].blank?) || (params[:device_type].blank?) || (params[:device_token].blank?) || (params[:app_version].blank?) then
        render :json => JSON.pretty_generate({"error" => 'some parameters missing'}) and return
      else
        @user = User.find_for_database_authentication email: params[:email]
        return invalid_login_attempt unless @user
        if @user.valid_password? params[:password]
          User.where(:email => params[:email]).update_all(:device_type => params[:device_type], :device_token => params[:device_token], :last_sign_in_at => Time.now())
          @version = Appverion.last
          currentversion = params[:device_type]==1 ? @version.android_app_version : @version.ios_app_version
          forceupdate = params[:device_type]==1 ? @version.android_force_update : @version.ios_force_update
          if @user.app_version != params[:app_version] then
            #@user.update_attribute(:app_version, params[:app_version])
            User.where('id=?',@user.id).update_all(:app_version=>params[:app_version])
            if (params[:app_version].to_i) < (currentversion.to_i) then
              popup="1"
            else
              popup="0"
            end
          else
            if @user.app_version < (currentversion.to_i) then
              popup="1"
            else
              popup="0"
            end
          end
          render :json => JSON.pretty_generate({"log" => '1', 'useraccesstoken' => @user.user_access_token, 'cc_is_registered' => @user.cc_is_registered, 'username' => @user.username, 'popup' => popup, 'forceupdate' => forceupdate})
        else
          render :json => JSON.pretty_generate({"error" => "Incorrect password."})
        end
      end
    else
      User.skip_callbacks = false
      if (params[:email].blank?)||(params[:username].blank?)||(params[:fb_id].blank?) || (params[:fb_access_token].blank?) || (params[:device_type].blank?) || (params[:device_token].blank?) then
        render :json => JSON.pretty_generate({"error" => 'some parameters missing'}) and return
      else
        @user = User.find_for_database_authentication fb_id: params[:fb_id]
        if @user.present? then
          User.where(:email => params[:email]).update_all(:device_type => params[:device_type], :device_token => params[:device_token], :last_sign_in_at => Time.now())
          @version = Appverion.last
          currentversion = params[:device_type]==1 ? @version.android_app_version : @version.ios_app_version
          forceupdate = params[:device_type]==1 ? @version.android_force_update : @version.ios_force_update
          if @user.app_version != params[:app_version] then
            User.where('id=?',@user.id).update_all(:app_version=>params[:app_version])
            if (params[:app_version].to_i) < (currentversion.to_i) then
              popup="1"
            else
              popup="0"
            end
          else
            if @user.app_version < (currentversion.to_i) then
              popup="1"
            else
              popup="0"
            end
          end
          render :json => JSON.pretty_generate({"log" => '1', 'useraccesstoken' => @user.user_access_token, 'cc_is_registered' => @user.cc_is_registered, 'username' => @user.username, 'popup' => popup, 'forceupdate' => forceupdate})
        else
          params[:image] = "https://graph.facebook.com/"+params[:fb_id]+'/picture?type=large'
          userExists=User.find_by_email(params[:email])
          if userExists.present? then
            isUpdated=User.where(:id => userExists['id']).update_all(:fb_id => params[:fb_id], :fb_access_token => params[:fb_access_token], :username => params[:username], :device_type => params[:device_type], :device_token => params[:device_token], :image => params[:image], :last_sign_in_at => Time.now());
            if isUpdated then
              @version = Appverion.last
              currentversion = params[:device_type]==1 ? @version.android_app_version : @version.ios_app_version
              forceupdate = params[:device_type]==1 ? @version.android_force_update : @version.ios_force_update
              if userExists['app_version'] != params[:app_version] then
                User.where("email=?",params[:email]).update_all(:app_version=>params[:app_version])
                if (params[:app_version].to_i) < (currentversion.to_i) then
                  popup="1"
                else
                  popup="0"
                end
              else
                if userExists['app_version'] < (currentversion.to_i) then
                  popup="1"
                else
                  popup="0"
                end
              end
              render :json => JSON.pretty_generate({"log" => "1", "useraccesstoken" => userExists['user_access_token'], 'cc_is_registered' => userExists['cc_is_registered'], 'username' => userExists['username'], 'popup' => popup, 'forceupdate' => forceupdate})
            else
              render :json => JSON.pretty_generate({"error" => "Registration failed. Please try again later."})
            end
          else
            user = User.new(person_params)
            if user.save
              userId=User.select("id,user_access_token,cc_is_registered,username").where("email=?", user['email']).limit(1);
              render :json => JSON.pretty_generate({"log" => "1", "useraccesstoken" => userId.first['user_access_token'], 'cc_is_registered' => userId.first['cc_is_registered'], 'username' => userId.first['username'], 'popup' => "0", 'forceupdate' => "0"})
            else
              render :json => JSON.pretty_generate({"error" => (user.errors.first).to_sentence})
            end
          end
        end
      end
    end

  end

#end  create

  private


#error in case unauthorised user attempts to login.
  def invalid_login_attempt
    render :json => JSON.pretty_generate({"error" => "Sorry, you are not registered with our application."})
  end

#error in case to register user(strong_params).
  def person_params
    params.permit(:email, :username, :password, :fb_id, :fb_access_token, :device_type, :device_token, :image, :avtar,:app_version)
  end

end
