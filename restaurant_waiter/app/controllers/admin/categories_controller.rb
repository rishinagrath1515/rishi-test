class Admin::CategoriesController < ApplicationController
  require 'will_paginate/array'

  def allcategories
    @allcategories=Category.find(:all, :conditions => ["restaurant_id =?", session[:restaurant_id]]).paginate(:page => params[:page], :per_page => 7)
    if @allcategories.present? then
      @allcategories.each do |val|
        val['category_name']=to_utf_8(val[:category_name])
        if (val.avatar).present? then
          val['image']=val.avatar.url(:medium)
        else
          val['image']=root_url[0..-1]+"/default.jpg"
        end
      end
    else
      flash.now[:nocategoryerror]="No Categories available"
    end
  end


  def addcategory
    allimages=params[:avatar]
    allcategories=params[:category_name]
    i=0
    if allcategories.nil? && allimages.nil?
      flash[:error]="Some of the values are missing."
      redirect_to :back
    else
      allcategories.each do |val|
        if val.present? then
          categoryName=to_iso8859_1((val.capitalize))
          if (allimages.nil?) || (allimages[i].blank?) then
            @newCategory=Category.new(:category_name => categoryName, :restaurant_id => session[:restaurant_id]);
            @isSaved=@newCategory.save
          else
            @newCategory=Category.new(:category_name => categoryName, :restaurant_id => session[:restaurant_id], :avatar => allimages[i]);
            @isSaved=@newCategory.save
          end
           i+=1
        end
      end
      if @isSaved then
        flash[:notice]="Categories has been added successfully."
      else
        flash[:categoryadderror]=@newCategory.errors['avatar'][0]
      end
      redirect_to :back
    end

  end

  def getcategoryinfo
    categoryInfo=Category.find(params[:categoryid])
    if categoryInfo.present?
      if (categoryInfo.avatar).present? then
        imageUrl=categoryInfo.avatar.url
      else
        imageUrl=root_url[0..-1]+"/default.jpg"
      end
      render json: {'id' => categoryInfo.id, 'categoryname' => categoryInfo.category_name, 'image' => imageUrl}

    else
      render json: {'error' => 'Something went wrong'}
    end
  end

  def updatecategory
    categoryid=params[:update_category_id]
    image=params[:avatar]
    categoryname=params[:update_category_name]
    @categoryInfo=Category.find(categoryid)
    if @categoryInfo.present? then
      isNameAlreadyExists=Category.where('category_name=? && restaurant_id=? && id!= ?', categoryname, session[:restaurant_id], categoryid)
      if isNameAlreadyExists.present? then
        flash[:updatecategoryerror]="Category with this name already exists in this restaurant."
        redirect_to :back and return
      else
        if image.present?
          isUpdated=@categoryInfo.update_attributes(:category_name => categoryname, :avatar => image)
        else
          isUpdated=@categoryInfo.update_attributes(:category_name => categoryname)
        end
        if isUpdated then
          flash[:updatecategorynotice]="Category has been updated successfully."
        else
          flash[:updatecategoryerror]="Unable to update category, please try again later."
        end
        redirect_to :back and return
      end
    else
      flash[:updatecategoryerror]="Unable to update category, please try again later."
      redirect_to :back and return
    end
  end


  def deletecategory
    deleteId= params[:deleteinputbox]
    isDeleted=Category.find(deleteId).destroy
    if isDeleted then
      Fooditem.where("category_id=?", deleteId).destroy_all
      flash[:notice]= "Category has been deleted successfully."
    else
      flash.now[:error]= "Unable to delete category, please try again after some time."
    end
    redirect_to admins_allcategories_path
  end


  def drinks
    @allcategories=Drinkcategory.find(:all, :conditions => ["restaurant_id =?", session[:restaurant_id]]).paginate(:page => params[:page], :per_page => 7)
    if @allcategories.present? then
      @allcategories.each do |val|
        val['drinkcategoryname']=to_utf_8(val[:drinkcategoryname])
        if (val.avatar).present? then
          val['image']= val.avatar.url(:medium)
        else
          val['image']=root_url[0..-1]+"default.jpg"
        end
      end
    else
      flash.now[:error]= "No Categories available."
    end
  end


  def drinkdetail
    drinkcategoryId=params[:id]
    alldrinksInfo=Alldrinks.where("drink_category_id=?", drinkcategoryId)
    if alldrinksInfo.present? then
      alldrinksInfo.each do |val|
        val['drink_name']=to_utf_8(val[:drink_name])
        val['description']=to_utf_8(val[:description])
        if (val.avatar).present? then
          val['image']= val.avatar.url(:thumb)
        else
          val['image']=root_url[0..-1]+ "default.jpg"
        end
      end
      @allDrinksInfoDetails=(alldrinksInfo.to_a.map(&:serializable_hash)).paginate(:page => params[:page], :per_page => 4)
      @allDrinksInfoDetails.each do |val|
        allSizesAndPrices=Drinkpricesandsize.where('drink_id=?', val['id']);
        if allSizesAndPrices.present? then
          val[:allsizes]=Hash.new
          allSizesAndPricesDetails=allSizesAndPrices.to_a.map(&:serializable_hash)
          val[:allsizes]=allSizesAndPricesDetails
        else

          val[:allsizes]=Hash.new
          val[:allsizes]=0
        end
      end
      #render json: JSON.pretty_generate({'here'=>@allFoodItemInfoDetails})and return
    else
      flash[:nodrinkserror]= "No drinks available, please add a drink."
    end
  end


  def getdrinkcategoryinfo
    categoryInfo=Drinkcategory.find(params[:categoryid])
    if categoryInfo.present?
      if (categoryInfo.avatar).present? then
        imageUrl=categoryInfo.avatar.url
      else
        imageUrl=root_url[0..-1]+"default.jpg"
      end
      render json: {'id' => categoryInfo.id, 'categoryname' => categoryInfo.drinkcategoryname, 'image' => imageUrl}

    else
      render json: {'error' => 'Something went wrong'}
    end
  end


  def updatedrinkcategory
    categoryid=params[:update_drinkcategory_id]
    image=params[:avatar]
    categoryname=params[:update_drinkcategory_name]
    @drinkcategoryInfo=Drinkcategory.find(categoryid)
    if @drinkcategoryInfo.present? then
      isNameAlreadyExists=Drinkcategory.where('drinkcategoryname=? && restaurant_id=? && id!= ?', categoryname, session[:restaurant_id], categoryid)
      if isNameAlreadyExists.present? then
        flash[:updatedrinkcategoryerror]="Category with this name already exists in this restaurant."
        redirect_to :back and return
      else
        if image.present?
          isUpdated=@drinkcategoryInfo.update_attributes(:drinkcategoryname => categoryname, :avatar => image)
        else
          isUpdated=@drinkcategoryInfo.update_attributes(:drinkcategoryname => categoryname)
        end
        if isUpdated then
          flash[:updatedrinkcategorynotice]="Category has been updated successfully."
        else
          flash[:updatedrinkcategoryerror]="Unable to update category, please try again later."
        end
        redirect_to :back and return
      end
    else
      flash[:updatedrinkcategoryerror]="Unable to update category, please try again later."
      redirect_to :back and return
    end
  end


  def adddrinkcategory
    allimages=params[:avatar]
    allcategories=params[:category_name]
    i=0
    allcategories.each do |val|
      if val.present? then
        drinkCategoryName=to_iso8859_1((val.capitalize))
        includeinbar=((params[:includeinbar].present?) && (params[:includeinbar][i].present?)) ? params[:includeinbar][i] : 0
        if (allimages.nil?) || allimages[i].nil? then
          @newDrinkCategory=Drinkcategory.new(:drinkcategoryname => drinkCategoryName, :include_in_bar => includeinbar, :restaurant_id => session[:restaurant_id]);
          @newDrinkCategory.save
        else
          @newDrinkCategory=Drinkcategory.new(:drinkcategoryname => drinkCategoryName, :include_in_bar => includeinbar, :restaurant_id => session[:restaurant_id], :avatar => allimages[i]);
          @newDrinkCategory.save
        end
      end
      i+=1
      flash[:notice]="New categories has been added successfully."
    end
    redirect_to :back
  end

  def deletedrinkcategory
    deleteId= params[:deletedrinkinputbox]
    isDeleted=Drinkcategory.find(deleteId).destroy
    if isDeleted then
      Alldrinks.where("drink_category_id=?", deleteId).destroy_all
      flash[:notice]= "Category has been deleted successfully."
    else
      flash[:error]= "Unable to delete category, please try again after some time."
    end
    #redirect_to :back
    redirect_to admins_drinks_path
  end


  def deletedrink
    deleteId= params[:deletedrinkbox]
    isDeleted=Alldrinks.find(deleteId).destroy
    if isDeleted then
      Drinkpricesandsize.where('drink_id=?', deleteId).destroy_all
      render json: {'log' => 1}
    else
      render json: {'log' => 0}
    end

  end

  def adddrink
    drinkName=to_utf_8((params[:name].capitalize))
    drinkCategoryId=params[:categoryid]
    includeinbar=params[:includeinbar]
    image=params[:avatar]
    description= to_utf_8(params[:description])
    allSizes=Array.new
    params[:size].each { |val|
      allSizes<<val
    }

    allPrices=Array.new
    params[:price].each { |val|
      allPrices<<val
    }
    allHappyHoursPrices=Array.new
    params[:happyhourprice].each { |val|
      allHappyHoursPrices<<val
    }
    maxQuantity=params[:maxquantity]

    nameAlreadyExists=Alldrinks.where("drink_name=? && drink_category_id=?", drinkName, drinkCategoryId)
    if nameAlreadyExists.present? then
      flash[:adddrinkerror]= "Drink with this name already exists in this category."
    else
      includeinbar=Drinkcategory.where("id=?",drinkCategoryId).first
      @isSaved=Alldrinks.new(:drink_category_id => drinkCategoryId, :drink_name => drinkName, :include_in_bar => includeinbar.include_in_bar, :avatar => image, :description => description, :restaurant_id => session[:restaurant_id], :max_quantity => maxQuantity)
      if @isSaved.save then
        (0..((allSizes.length)-1)).each do |i|
          if allSizes[i].present? then
            isSizeExists=Size.find_by_size_name((allSizes[i].capitalize))
            if isSizeExists.present? then
              size=isSizeExists['id']
              size_name=isSizeExists['size_name']
            else
              newSize = (Size.new(:size_name => (allSizes[i].capitalize))).save
              size=Size.last.id
              size_name=(allSizes[i].capitalize)
            end
            (Drinkpricesandsize.new(:drink_id => @isSaved.id, :size => size, :size_name => size_name, :price => allPrices[i], :happy_hour_price => allHappyHoursPrices[i])).save
          end
        end
        flash[:adddrinknotice]= "Drink has been added successfully";
      else
        flash[:adddrinkerror]= "Something went wrong, please try again later."
      end
    end
    redirect_to :back
  end


  def updatedrink
    drinkid=params[:updatedrinkbox]
    drinkname=to_iso8859_1((params[:drink_name].capitalize))
    drinkdesc=to_iso8859_1(params[:drink_desc])
    image=params[:avatar]
    @drinkInfo=Alldrinks.find(drinkid)
    if @drinkInfo.present? then
      if image.present? then
        isUpdated=@drinkInfo.update_attributes(:drink_name => drinkname, :description => drinkdesc, :avatar => image)
      else
        isUpdated=@drinkInfo.update_attributes(:drink_name => drinkname, :description => drinkdesc)
      end
      if isUpdated then
        allPrices=Array.new
        allSizes=Array.new
        params[:drink_price].each { |val|
          allPrices<<val
        }
        params[:drink_size].each { |val|
          allSizes<<val
        }
        (0..((allSizes.length)-1)).each do |i|
          Drinkpricesandsize.where(:drink_id => drinkid, :size_name => allSizes[i]).update_all(:price => allPrices[i])
        end
      end
      flash[:updatedrinknotice]="Drink has been updated successfully."
    else
      flash[:updatedrinkerror]="Something went wrong, please try again later."
    end
    redirect_to :back
  end


  def categorydetail
    categoryId=params[:id]
    allFoodItemInfo=Fooditem.where("category_id=?", categoryId)
    if allFoodItemInfo.present? then
      allFoodItemInfo.each do |val|
        val['food_name']=to_utf_8(val[:food_name])
        val['description']=to_utf_8(val[:description])
        if (val.avatar).present? then
          val['image']= val.avatar.url(:thumb)
        else
          val['image']=root_url[0..-1]+ "default.jpg"
        end
      end
      @allFoodItemInfoDetails=(allFoodItemInfo.to_a.map(&:serializable_hash)).paginate(:page => params[:page], :per_page => 4)
      @allFoodItemInfoDetails.each do |val|
        allSizesAndPrices=Fooditemspricesandsize.where('food_id=?', val['id']);
        if allSizesAndPrices.present? then
          val[:allsizes]=Hash.new
          allSizesAndPricesDetails=allSizesAndPrices.to_a.map(&:serializable_hash)
          val[:allsizes]=allSizesAndPricesDetails
        else

          val[:allsizes]=Hash.new
          val[:allsizes]=0
        end
      end
    else
      flash[:nofooditemerror]= "No Food item available, please add a food item."
    end
  end

  def addfooditem
    foodItemName=params[:name]
    categoryId=params[:categoryid]
    image=params[:avatar]
    description= params[:description]
    allSizes=Array.new
    params[:size].each { |val|
      allSizes<<val
    }

    allPrices=Array.new
    params[:price].each { |val|
      allPrices<<val
    }


    allHappyHoursPrices=Array.new
    params[:happyhourprice].each { |val|
      allHappyHoursPrices<<val
    }
    maxQuantity=params[:maxquantity]
    nameAlreadyExists=Fooditem.where("food_name=? && category_id=?", (foodItemName.capitalize), categoryId)
    if nameAlreadyExists.present? then
      flash[:addfooditemerror]= "Food with this name already exists in this category."
    else
      foodName=to_utf_8((foodItemName.capitalize))
      foodDescription=to_utf_8(description)
      @isSaved=Fooditem.new(:food_name => foodName, :avatar => image, :description => foodDescription, :restaurant_id => session[:restaurant_id], :category_id => categoryId, :max_quantity => maxQuantity)
      if @isSaved.save then
        (0..((allSizes.length)-1)).each do |i|
          if allSizes[i].present? then
            isSizeExists=Size.find_by_size_name((allSizes[i].capitalize))
            if isSizeExists.present? then
              size=isSizeExists['id']
              size_name=isSizeExists['size_name']
            else
              newSize = (Size.new(:size_name => (allSizes[i].capitalize))).save
              size=Size.last.id
              size_name=(allSizes[i].capitalize)
            end
            (Fooditemspricesandsize.new(:food_id => @isSaved.id, :size => size, :size_name => size_name, :price => allPrices[i], :happy_hour_price => allHappyHoursPrices[i])).save
          end
        end
        flash[:addfooditemnotice]= "Food item has been added successfully";
      else
        flash[:addfooditemerror]= "Something went wrong, please try again later."
      end
    end
    redirect_to :back
  end


  def updatefooditem
    foodid=params[:updatefooditembox]
    foodname=to_iso8859_1((params[:food_item_name].capitalize))
    fooddesc=to_iso8859_1(params[:food_item_desc])
    image=params[:avatar]
    @foodItemInfo=Fooditem.find(foodid)
    if @foodItemInfo.present? then
      if image.present? then
        isUpdated=@foodItemInfo.update_attributes(:food_name => foodname, :description => fooddesc, :avatar => image)
      else
        isUpdated=@foodItemInfo.update_attributes(:food_name => foodname, :description => fooddesc)
      end
      if isUpdated then
        allPrices=Array.new
        allSizes=Array.new
        params[:food_item_price].each { |val|
          allPrices<<val
        }
        params[:food_item_size].each { |val|
          allSizes<<val
        }
        (0..((allSizes.length)-1)).each do |i|
          Fooditemspricesandsize.where(:food_id => foodid, :size_name => allSizes[i]).update_all(:price => allPrices[i])
        end
      end
      flash[:updatefooditemnotice]="Food Item updated successfully."
    else
      flash[:updatefooditemerror]="Something went wrong, please try again later."
    end
    redirect_to :back
  end


  def deletefooditem
    deleteId= params[:deletefooditembox]
    isDeleted=Fooditem.find(deleteId).destroy
    if isDeleted then
      Fooditemspricesandsize.where('food_id=?', deleteId).destroy_all
      Extra.where('food_id=?', deleteId).destroy_all
      render json: {'log' => 1}
    else
      render json: {'log' => 0}
    end
  end

  def extrasdetail
    @allextrasinfooditem=Extra.find(:all, :conditions => ["food_id =?", params[:foodid]]).paginate(:page => params[:page], :per_page => 4)
    if @allextrasinfooditem.present? then
      @allextrasinfooditem.each do |val|
        val['extra_name']=to_utf_8(val[:extra_name])
        val['description']=to_utf_8(val[:description])
        if (val.avatar).present? then
          val['image']= val.avatar.url(:thumb)
        else
          val['image']=root_url[0..-1]+"default.jpg"
        end
      end
    else
      flash[:noextrasdetailerror]= "No Extras available."
    end
  end


  def deleteextras
    deleteId= params[:deleteextrasbox]
    isDeleted=Extra.find(deleteId).destroy
    if isDeleted then
      render json: {'log' => 1}
    else
      render json: {'log' => 0}
    end
  end

  def addextras
    extrasName=to_iso8859_1((params[:name].capitalize))
    foodId=params[:foodid]
    image=params[:avatar]
    description= to_iso8859_1(params[:description])
    price=params[:price]
    maxQuantity=params[:maxquantity]
    happyHoursPrice=params[:happyhourprice]
    nameAlreadyExists=Extra.where("extra_name=? && food_id=?", (extrasName.capitalize), foodId)
    if nameAlreadyExists.present? then
      flash[:notice]= "Extras with this name alreday exists with this food item."
    else
      @isSaved=Extra.new(:extra_name => (extrasName.capitalize), :avatar => image, :description => description, :restaurant_id => session[:restaurant_id], :food_id => foodId, :price => price, :happy_hour_price => happyHoursPrice, :max_quantity => maxQuantity)
      if @isSaved.save then
        flash[:addextrasnotice]= "Extras have been added successfully";
      else
        flash[:addextraserror]= "Something went wrong, please try again later."
      end
    end
    redirect_to :back
  end

  def updateextras
    extraid=params[:updateextrasbox]
    foodname=to_iso8859_1((params[:extras_name].capitalize))
    fooddesc=to_iso8859_1(params[:extras_desc])
    image=params[:avatar]
    price=params[:extras_price]
    @extrasInfo=Extra.find(extraid)
    if @extrasInfo.present? then
      if image.present? then
        @extrasInfo.update_attributes(:extra_name => foodname, :description => fooddesc, :avatar => image, :price => price)
      else
        @extrasInfo.update_attributes(:extra_name => foodname, :description => fooddesc, :price => price)
      end
      flash[:updateextrasnotice]= "Extras have been updated successfully.";
    else
      flash[:updateextraserror]= "Something went wrong, please try again later.";
    end
    redirect_to :back
  end

  def to_iso8859_1(value)
    finalValue=Iconv.iconv('ISO-8859-1', 'UTF-8', value).join
    return finalValue;
  end


  def to_utf_8(value)
    finalValue= Iconv.iconv('UTF-8', 'ISO-8859-1', value).join
    return finalValue;
  end



end
