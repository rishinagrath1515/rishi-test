class Admin::SessionsController < ApplicationController
  #helper_method  :devise_mapping
  protect_from_forgery :except => [:create]

  def new
=begin
    if cookies[:auth_token].present? then
      @admin=Admin.find_by_authenticity_token(cookies[:auth_token])
      if @admin.present? then
        session[:restaurant_id] = @admin.restaurant_id
        @restaurantInfo=Restaurantdetail.where("id=?", @admin.restaurant_id).limit(1)
        render :action=>'create'
      else
        @admin=Admin.new
      end
    end
=end
    @admin=Admin.new
  end

  def create
    begin
    @admin = Admin.find_for_database_authentication email: params[:admin][:email]
    rescue Exception => e
      @admin = Admin.find_for_database_authentication(restaurant_id: params[:id]) unless params[:id].nil?
    end
    return invalid_login_attempt unless @admin
    return incorrect_password unless @admin.valid_password? params[:admin][:password] if params[:super_admin].nil? and params[:id].nil?
      if @admin.confirm_status == 1 && @admin.is_paid == 1 then
        if @admin.active then
          session[:restaurant_id] = @admin.restaurant_id
          session[:offset]=params[:offset]
          @notPaid=0
          begin
            if params[:admin][:remember_me]=="1" then
               cookies[:auth_token] = params[:admin][:email]
              cookies[:auth_pass] = params[:admin][:password]
            end
          rescue Exception=>e
          end
          sign_in @admin
          redirect_to admins_manageoptions_path
        else
          flash[:disablemsg]="Your account has been disabled, please contact system administrator."
          redirect_to :back and return
        end
      elsif @admin.confirm_status == 0 && @admin.is_paid == 0 then
        session[:id]= @admin.id
        flash[:notpaid]=1
        redirect_to :back and return
      elsif @admin.confirm_status == 0 && @admin.is_paid == 1 then
       flash[:alreadyPaidNotice]="You have successfully paid for this account, please wait for the confirmation."
       redirect_to :back and return
      end
  end

  def destroy
    @admin=Admin.find(current_admin.id);
    (Devise.sign_out_all_scopes ? sign_out(@admin) : sign_out(@admin))
    redirect_to new_admin_session_path
  end



  private
  def invalid_login_attempt
    flash[:signin_notice]="Unauthorized access."
    redirect_to :back and return
  end

  def incorrect_password
    flash[:signin_notice]="Incorrect password."
    render "new"
  end

end
