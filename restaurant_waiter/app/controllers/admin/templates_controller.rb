class Admin::TemplatesController < ApplicationController

  before_filter :authenticate_admin!


  def uploadtemplate
    @excelName=Restaurantdetail.find(session[:restaurant_id])

    if @excelName.present? then
      @url=@excelName.excel.url
      flash[:url]=@url
    else
      render json: {'here'=>@excelName} and return
      @url=root_url+"Restaurant_Waiter_Data.xls"
      flash[:url]=@url
    end
  end

  def downloadtemplate
    @excelName=Restaurantdetail.find(params[:id])
     redirect_to @excelName.excel.url
  end

  def upload
    require 'spreadsheet'
    file=params[:excel];

    spreadsheet = open_spreadsheet(file)
    totalSheets=spreadsheet.sheets.count
    allCategories=Array.new
    allFoodItemInfo=Array.new
    allextras=Array.new
    allDrinkCatgories=Array.new
    allDrinkOtherInfo=Array.new
    (0..(totalSheets-1)).each do |j|
      sheetInfo=spreadsheet.sheet(j)
      header = sheetInfo.row(1)

      if j==0
        if header[0] !='Food Item Name' || header[1]!='Food Item Category' || header[2]!='Size' || header[3]!='Price(in dollars)' || header[4]!='Description' || header[5]!='Max Quantity' || header[6]!='Happy Hour Price' || header[7]!='CheckValid' then
          flash[:excelerror]="Please download the  provided template"
          redirect_to :back and return false
        end
      elsif j==1
        if header[0] !='Extra Name' || header[1]!='Extras_Food Item Category' || header[2]!='Description' || header[3]!='Price' || header[4]!='Max Quantity' || header[5]!='Happy Hour Price' || header[6]!='CheckValid' then
          flash[:excelerror]="Please download the provided template"
          redirect_to :back and return false
        end
      elsif j==2
        if header[0] !='Name' || header[1]!='Drink Category' || header[2]!='Size' || header[3]!='Description' || header[4]!='Max Quantity' || header[5]!='Price' || header[6]!='Happy Hour Price' || header[7]!='Include in bar drinks' || header[8]!='CheckValid' then
          flash[:excelerror]="Please download the provided template"
          redirect_to :back and return false
        end
      end
    end
    @restaurant_info = Restaurantdetail.find(session[:restaurant_id])
    @restaurant_info.update_attribute(:excel, file)
    (0..(totalSheets-1)).each do |j|
      sheetInfo=spreadsheet.sheet(j)
      header = sheetInfo.row(1)
      (2..sheetInfo.last_row).each do |i|
        row = Hash[[header, sheetInfo.row(i)].transpose]
        if j==0 then
          if row['Food Item Category'].present? then
            categoryName=to_iso8859_1((row['Food Item Category'].capitalize))
            allCategories<<categoryName
            if row['Food Item Name'].present? && row['Description'].present? && row['Max Quantity'].present? && row['Size'].present? && row['Happy Hour Price'].present?
              if (row['Description'].size) > 50 then
                flash[:excelerror]="Description of food item must be of 50 characters."
                redirect_to :back and return false
              else
                allFoodItemInfo<<row
              end
            else
              flash[:excelerror]="Some of the enteries are missing, please upload the file again."
              redirect_to :back and return false
            end
          end
        elsif j==1
          if row['Extra Name'].present? && row['Extras_Food Item Category'].present? && row['Description'].present? && row['Price'].present? && row['Max Quantity'].present? && row['Happy Hour Price'].present?
            if (row['Description'].size) > 50 then
              flash[:excelerror]="Description of Extras must be of 50 characters."
              redirect_to :back and return false
            else
              allextras<<row
            end
            #encodedFoodItem=to_iso8859_1((row['Extras_Food Item Category'].capitalize))
            # foodId=Fooditem.find_by_food_name(encodedFoodItem)
            # if foodId.present?
            #  encodedExtrasName=to_iso8859_1((row['Extra Name'].capitalize))
            #  encodedDescription=to_iso8859_1(row['Description'])
            #extrasId=Extra.select('id').where('extra_name=? && restaurant_id =?', encodedExtrasName, session[:restaurant_id]).limit(1);
            #if extrasId.present? then
            # Extra.where('extra_name=? && restaurant_id =?', encodedExtrasName, session[:restaurant_id]).update_all(:extra_name => encodedExtrasName, :image => 'default.jpg', :price => row['Price'], :description => encodedDescription, :happy_hour_price => row['Happy Hour Price'], :updated_at => Time.now, :restaurant_id => session[:restaurant_id], :food_id => foodId['id'], :max_quantity => row['Max Quantity'])
            # else
            # (Extra.new(:extra_name => encodedExtrasName, :image => 'default.jpg', :price => row['Price'], :description => encodedDescription, :happy_hour_price => row['Happy Hour Price'], :created_at => Time.now, :updated_at => Time.now, :restaurant_id => session[:restaurant_id], :food_id => foodId['id'], :max_quantity => row['Max Quantity'])).save
            #end
            #end
          else
            flash[:excelerror]="Some of the enteries are missing, please upload the file again."
            redirect_to :back and return false
          end
        elsif j==2
          if row['Drink Category'].present? then
            encodedDrinkCategory=to_iso8859_1((row['Drink Category'].capitalize))
            allDrinkCatgories<<(encodedDrinkCategory)
            if row['Name'].present? && row['Drink Category'].present? && row['Size'].present? && row['Description'].present? && row['Max Quantity'].present? && row['Happy Hour Price'].present? && row['Include in bar drinks'].present?
              if (row['Description'].size) > 50 then
                flash[:excelerror]="Description of drinks must be of 50 characters."
                redirect_to :back and return false
              else
                allDrinkOtherInfo<<row
              end
              #allDrinkOtherInfo<<row
            else
              flash[:excelerror]="Some of the enteries are missing, please upload the file again."
              redirect_to :back and return false
            end
          end
        else
          flash[:excelerror]="Something went wrong, please check your file and try again later."
          redirect_to :back and return false
        end
      end
      if j==0
        allUniqCategories=allCategories.uniq
        allUniqCategories.each do |val|
          checkCategoryName=to_iso8859_1((val.capitalize))
          categoryId=Category.select('id').where('category_name=? && restaurant_id =?', checkCategoryName, session[:restaurant_id]).limit(1);
          if categoryId.present? then
           Category.where("id = ? && restaurant_id =?", categoryId.first['id'], session[:restaurant_id]).update_all(:category_name => checkCategoryName, :updated_at => Time.now, :restaurant_id => session[:restaurant_id], :image => 'default.jpg');
          else
          (Category.new(:category_name => checkCategoryName, :created_at => Time.now, :updated_at => Time.now, :restaurant_id => session[:restaurant_id], :image => 'default.jpg')).save;
           end
        end
        allFoodItemInfo.each do |value|
          getCategoryId=to_iso8859_1((value['Food Item Category'].capitalize))
          categoryId=Category.where("category_name=? && restaurant_id=?", getCategoryId, session[:restaurant_id])
          encodedFoodItemName=to_iso8859_1((value['Food Item Name'].capitalize))
          fooditemId=Fooditem.select('id').where('food_name=? && restaurant_id =?', encodedFoodItemName, session[:restaurant_id]).limit(1);
          if fooditemId.present? then
            isSizeExists=Size.find_by_size_name((value['Size'].capitalize))
            if isSizeExists.present? then
              size=isSizeExists['id']
              size_name=isSizeExists['size_name']
            else
              newSize = (Size.new(:size_name => (value['Size'].capitalize))).save
              size=Size.last.id
              size_name=(value['Size'].capitalize)
            end
            pricesInfo=Fooditemspricesandsize.select('id').where("food_id = ? && size=?", fooditemId.first['id'], size)
            if pricesInfo.present? then
              Fooditemspricesandsize.where("food_id = ? && size=?", fooditemId.first['id'], size).update_all(:size => size, :price => value['Price(in dollars)'], :happy_hour_price => value['Happy Hour Price'], :updated_at => Time.now, :size_name => size_name)
            else
              (Fooditemspricesandsize.new(:food_id => fooditemId.first['id'], :size => size, :price => value['Price(in dollars)'], :happy_hour_price => value['Happy Hour Price'], :created_at => Time.now, :updated_at => Time.now, :size_name => size_name)).save
            end
          else
            encodedDescription=to_iso8859_1(value['Description'])
            @isSaved = Fooditem.new(:food_name => encodedFoodItemName, :image => 'default.jpg', :description => encodedDescription, :created_at => Time.now, :updated_at => Time.now, :restaurant_id => session[:restaurant_id], :category_id => categoryId[0]['id'], :max_quantity => value['Max Quantity'])
            if @isSaved.save then
              isSizeExists=Size.find_by_size_name((value['Size']))
              if isSizeExists.present? then
                size=isSizeExists['id']
                size_name=isSizeExists['size_name']
              else
                @newSize = (Size.new(:size_name => (value['Size']))).save
                size=Size.last.id
                size_name=(value['Size'])
              end
              (Fooditemspricesandsize.new(:food_id => @isSaved.id, :size => size, :price => value['Price(in dollars)'], :happy_hour_price => value['Happy Hour Price'], :created_at => Time.now, :updated_at => Time.now, :size_name => size_name)).save
            end
          end
        end
      elsif j==1
        allextras.each do |value|
          encodedFoodItem=to_iso8859_1((value['Extras_Food Item Category'].capitalize))
          foodId=Fooditem.find_by_food_name(encodedFoodItem)
          if foodId.present?
            encodedExtrasName=to_iso8859_1((value['Extra Name'].capitalize))
            encodedDescription=to_iso8859_1(value['Description'])
            (Extra.new(:extra_name => encodedExtrasName, :image => 'default.jpg', :price => value['Price'], :description => encodedDescription, :happy_hour_price => value['Happy Hour Price'], :created_at => Time.now, :updated_at => Time.now, :restaurant_id => session[:restaurant_id], :food_id => foodId['id'], :max_quantity => value['Max Quantity'])).save
          end
        end
      elsif j==2
        allUniqDrinkCategories=allDrinkCatgories.uniq
        allUniqDrinkCategories.each do |val|
          encodeddrinkname=to_iso8859_1((val.capitalize))
          categoryId=Drinkcategory.select('id').where('drinkcategoryname=? && restaurant_id =?', encodeddrinkname, session[:restaurant_id]).limit(1);
          if categoryId.present? then
            Drinkcategory.where("id = ? && restaurant_id =?", categoryId.first['id'], session[:restaurant_id]).update_all(:drinkcategoryname => encodeddrinkname, :restaurant_id => session[:restaurant_id], :updated_at => Time.now, :image => 'default.jpg', :include_in_bar => 0)
          else
          (Drinkcategory.new(:drinkcategoryname => encodeddrinkname, :restaurant_id => session[:restaurant_id], :created_at => Time.now, :updated_at => Time.now, :image => 'default.jpg', :include_in_bar => 0)).save
          end
        end
        allDrinkOtherInfo.each do |value|
          encodeddrinkCategoryname=to_iso8859_1((value['Drink Category'].capitalize))
          drinkCatgeoryId=Drinkcategory.where("drinkcategoryname=? && restaurant_id=?", encodeddrinkCategoryname, session[:restaurant_id])
          encodeddrinkname=to_iso8859_1((value['Name'].capitalize))
          alldrinksId=Alldrinks.select('id').where('drink_name=? && restaurant_id =?', encodeddrinkname, session[:restaurant_id]).limit(1);
          if alldrinksId.present? then
            isSizeExists=Size.find_by_size_name((value['Size']))
            if isSizeExists.present? then
              size=isSizeExists['id']
              size_name=isSizeExists['size_name']
            else
              @newSize = (Size.new(:size_name => (value['Size']))).save
              size=Size.last.id
              size_name=(value['Size'])
            end
            pricesInfo=Drinkpricesandsize.where("drink_id = ? && size=?", alldrinksId.first['id'], size)
            if pricesInfo.present? then
              Drinkpricesandsize.where("drink_id = ? && size=?", alldrinksId.first['id'], size).update_all(:size => size, :size_name => size_name, :price => value['Price'], :happy_hour_price => value['Happy Hour Price'], :created_at => Time.now, :updated_at => Time.now)
            else
              (Drinkpricesandsize.new(:drink_id => alldrinksId.first['id'], :size => size, :size_name => size_name, :price => value['Price'], :happy_hour_price => value['Happy Hour Price'], :created_at => Time.now, :updated_at => Time.now)).save
            end
          else

            encodedDescription=to_iso8859_1(value['Description'])
            includeInBar=value['Include in bar drinks']=='Yes' ? 1 : 0
            isSizeExists=Size.find_by_size_name((value['Size'].capitalize))
            if isSizeExists.present? then
              size=isSizeExists['id']
              size_name=isSizeExists['size_name']
            else
              newSize = Size.new(:size_name => (value['Size'].capitalize))
              size=Size.last.id
              size_name=(value['Size'].capitalize)
            end
            @isSaved=Alldrinks.new(:drink_category_id => drinkCatgeoryId[0]['id'], :drink_name => encodeddrinkname, :image => 'default.jpg', :description => encodedDescription, :restaurant_id => session[:restaurant_id], :created_at => Time.now, :updated_at => Time.now, :max_quantity => value['Max Quantity'], :include_in_bar => includeInBar)
            if @isSaved.save then
              (Drinkpricesandsize.new(:drink_id => @isSaved.id, :size => size, :size_name => size_name, :price => value['Price'], :happy_hour_price => value['Happy Hour Price'], :created_at => Time.now, :updated_at => Time.now)).save
            end
          end
        end
        allUniqDrinkCategories.each do |val|
          encodeddrinkname=to_iso8859_1((val.capitalize))
          categoryId=Drinkcategory.select('id').where('drinkcategoryname=? && restaurant_id =?', encodeddrinkname, session[:restaurant_id]).limit(1);
          if categoryId.present? then
            allDrinksInCategory=Alldrinks.where('drink_category_id=?', categoryId.first['id'])
            allDrinksInbar=Alldrinks.where('drink_category_id=? && include_in_bar=?', categoryId.first['id'], 1)
            if (allDrinksInCategory.size)==(allDrinksInbar.size) then
              Drinkcategory.where('id=?', categoryId.first['id']).update_all(:include_in_bar => 1)
            else
              Drinkcategory.where('id=?', categoryId.first['id']).update_all(:include_in_bar => 0)
            end
          end
        end
      end
    end
    flash[:log]="File uploaded successfully."
    redirect_to :back
  end

  def open_spreadsheet(file)
    require 'iconv'
    require 'spreadsheet'
    require 'roo'
    case File.extname(file.original_filename)
      when ".csv" then
        Roo::Csv.new(file.path, nil, :ignore)
      when ".xls" then
        Roo::Excel.new(file.path, nil, :ignore)
      when ".xlsx" then
        Roo::Excelx.new(file.path, nil, :ignore)
      else
        raise "Unknown file type: #{file.original_filename}"
    end

  end


  def updateoptions
    displayPrice=params[:displayprice]
    gratuity=params[:display_gratuity]
    if (gratuity=='1') || (gratuity=='2') then
      if params[:gratuity_val].present? then
        isUpdated=Restaurantdetail.where('id=?', session[:restaurant_id]).update_all(:display_price_outside_restaurant => displayPrice, :gratuity_status => gratuity, :gratuity => params[:gratuity_val]);
      end
    else
      isUpdated=Restaurantdetail.where('id=?', session[:restaurant_id]).update_all(:display_price_outside_restaurant => displayPrice, :gratuity_status => gratuity);
    end
    if isUpdated then
      @restaurantInfo = Restaurantdetail.where("id=?", session[:restaurant_id]).limit(1)
      @messages_rendered=true
      flash[:updatenotice]="Manage options has been updated successfully."
      redirect_to :back
    else
      flash[:updateerror]="Unable to update options."
      redirect_to :back
    end
  end


  def uploadallimages
    if params[:type]== "1" then
      params[:avatar].each { |image|
        categoryName=(image.original_filename).split('.')
        @allInfo=Category.where(category_name: categoryName[0].capitalize, restaurant_id: session[:restaurant_id])
        if @allInfo.present? then
          @allInfo.each do |val|
            val.update_attribute(:avatar, image)
          end
        end
      }
      flash[:imageuploadnotice]="Images Uploaded Successfully."
      redirect_to :back and return
    elsif params[:type]== "2" then
      params[:avatar].each { |image|
        foodItemName=(image.original_filename).split('.')
        @allInfo=Fooditem.where(food_name: foodItemName[0].capitalize, restaurant_id: session[:restaurant_id])
        if @allInfo.present? then
          @allInfo.each do |val|
            val.update_attribute(:avatar, image)
          end
        end
      }
      flash[:imageuploadnotice]="Images Uploaded Successfully."
      redirect_to :back and return
    elsif params[:type]== "3" then
      params[:avatar].each { |image|
        extrasName=(image.original_filename).split('.')
        @allInfo=Extra.where(extra_name: extrasName[0].capitalize, restaurant_id: session[:restaurant_id])
        if @allInfo.present? then
          @allInfo.each do |val|
            val.update_attribute(:avatar, image)
          end
        end
      }
      flash[:imageuploadnotice]="Images Uploaded Successfully."
      redirect_to :back and return
    elsif params[:type]== "4" then
      params[:avatar].each { |image|
        drinkCategoryName=(image.original_filename).split('.')
        @allInfo=Drinkcategory.where(drinkcategoryname: drinkCategoryName[0].capitalize, restaurant_id: session[:restaurant_id])
        if @allInfo.present? then
          @allInfo.each do |val|
            val.update_attribute(:avatar, image)
          end
        end
      }
      flash[:imageuploadnotice]="Images Uploaded Successfully."
      redirect_to :back and return
    elsif params[:type]== "5" then
      params[:avatar].each { |image|
        drinkName=(image.original_filename).split('.')
        @allInfo=Alldrinks.where(drink_name: drinkName[0].capitalize, restaurant_id: session[:restaurant_id])
        if @allInfo.present? then
          @allInfo.each do |val|
            val.update_attribute(:avatar, image)
          end
        end
      }
      flash[:imageuploadnotice]="Images Uploaded Successfully."
      redirect_to :back and return
    else
      flash[:imageuploaderror]="Unable to upload images, please try again later."
      redirect_to :back and return
    end
  end


  def getcategorycount
    if params[:type]=="1"
      totalCount=Category.count(:conditions => {restaurant_id: params[:restaurantid]})
    elsif params[:type]=="2"
      totalCount=Fooditem.count(:conditions => {restaurant_id: params[:restaurantid]})
    elsif params[:type]=="3"
      totalCount=Extra.count(:conditions => {restaurant_id: params[:restaurantid]})
    elsif params[:type]=="4"
      totalCount=Drinkcategory.count(:conditions => {restaurant_id: params[:restaurantid]})
    elsif params[:type]=="5"
      totalCount=Alldrinks.count(:conditions => {restaurant_id: params[:restaurantid]})
    end
    if (totalCount.to_i) != (params[:imagecount].to_i) then
      render json: {'here' => totalCount.to_i, 'check' => params[:imagecount].to_i} and return
      if (totalCount.to_i) > (params[:imagecount].to_i)
        render :json => JSON.pretty_generate({"log" => "1", "message" => "Some of the images are missing,do you want to continue?"})
      else
        render :json => JSON.pretty_generate({"log" => "1", "message" => "Images are more then required, do you want to continue?"})
      end
    else
      render :json => JSON.pretty_generate({"log" => "0"})
    end
  end

  def manageoptions
    @restaurantInfo=Restaurantdetail.where("id=?", session[:restaurant_id]).limit(1)
  end

  def to_iso8859_1(value)
    finalValue=Iconv.iconv('ISO-8859-1', 'UTF-8', value).join
    return finalValue;
  end

  def to_utf_8(value)
    Iconv.iconv('UTF-8', 'ISO-8859-1', value).join
  end
end
