class Admin::PasswordsController < ApplicationController
  def update
    @admin = Admin.find_by_reset_password_token(params[:token])
    isupdated=@admin.update_attribute(:password,params[:password])
    if isupdated then
      flash[:resetpassword]=1
    else
      flash[:resetpassworderror]="Unable to reset password, please try after some time."
    end
    redirect_to :back
    #else
    #   render :json => JSON.pretty_generate({"error" => "Password reset has expired."})
    # end
  end
end
