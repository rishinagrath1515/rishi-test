class Admin::SettingsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  require 'will_paginate/array'

  def restaurantsettings
    @ishappyhouravailable=Restaurantdetail.where('id=?', session[:restaurant_id])
    timings= Restauranttiming.where('restaurant_id=?', session[:restaurant_id])
    if timings.present? then
      @timings=timings
      @timings.each do |val|
        offset=session[:offset]
        if offset[0]=="-" then
          toaddoffset=offset.split("-")
          if val["opening_time"].present? && val["closing_time"].present? then
            val["opening_time"] = val["opening_time"] + (toaddoffset[1].to_i).to_i.minutes
            val["closing_time"] = val["closing_time"] + (toaddoffset[1].to_i).to_i.minutes
          else
            val["opening_time"] = val["opening_time"]
            val["closing_time"] = val["closing_time"]
          end
        elsif offset[0]=="+" then
          toaddoffset=offset.split("+")
          if val["opening_time"].present? && val["closing_time"].present? then
            val["opening_time"] = val["opening_time"] - (toaddoffset[1].to_i).to_i.minutes
            val["closing_time"] = val["closing_time"] - (toaddoffset[1].to_i).to_i.minutes
          else
            val["opening_time"] = val["opening_time"]
            val["closing_time"] = val["closing_time"]
          end
        else
          if val["opening_time"].present? && val["closing_time"].present? then
            val["opening_time"] = val["opening_time"] - (offset.to_i).to_i.minutes
            val["closing_time"] = val["closing_time"] - (offset.to_i).to_i.minutes
          else
            val["opening_time"] = val["opening_time"]
            val["closing_time"] = val["closing_time"]
          end
        end
        #if offset[0]=="+" then
        #  toaddoffset=offset.split("+")
        #  if val["opening_time"].present? && val["closing_time"].present? then
        #  val["opening_time"] = val["opening_time"] - (toaddoffset[1].to_i).to_i.minutes
        #  val["closing_time"] = val["closing_time"] - (toaddoffset[1].to_i).to_i.minutes
        #  else
        #    val["opening_time"] = val["opening_time"]
        #    val["closing_time"] = val["closing_time"]
        #  end
        #end
      end


      @timings.each do |val|
        offset=session[:offset]
        if offset[0]=="-" then
          toaddoffset=offset.split("-")
          if val["bar_opening_time"].present? && val["bar_opening_time"].present?  then
          val["bar_opening_time"] = val["bar_opening_time"] + (toaddoffset[1].to_i).to_i.minutes
          val["bar_closing_time"] = val["bar_closing_time"] + (toaddoffset[1].to_i).to_i.minutes
          else
            val["bar_opening_time"] = val["bar_opening_time"]
            val["bar_closing_time"] = val["bar_closing_time"]
          end
        elsif offset[0]=="+" then
          toaddoffset=offset.split("+")
          if val["bar_opening_time"].present? && val["bar_closing_time"].present? then
            val["bar_opening_time"] = val["bar_opening_time"] - (toaddoffset[1].to_i).to_i.minutes
            val["bar_closing_time"] = val["bar_closing_time"] - (toaddoffset[1].to_i).to_i.minutes
          else
            val["bar_opening_time"] = val["bar_opening_time"]
            val["bar_closing_time"] = val["bar_closing_time"]
          end
        else
          if val["bar_opening_time"].present? && val["bar_closing_time"].present? then
            val["bar_opening_time"] = val["bar_opening_time"] - (offset.to_i).to_i.minutes
            val["bar_closing_time"] = val["bar_closing_time"] - (offset.to_i).to_i.minutes
          else
            val["bar_opening_time"] = val["bar_opening_time"]
            val["bar_closing_time"] = val["bar_closing_time"]
          end
        end
        #if offset[0]=="+" then
        #  toaddoffset=offset.split("+")
        #  if val["bar_opening_time"].present? && val["bar_closing_time"].present? then
        #  val["bar_opening_time"] = val["bar_opening_time"] - (toaddoffset[1].to_i).to_i.minutes
        #  val["bar_closing_time"] = val["bar_closing_time"] - (toaddoffset[1].to_i).to_i.minutes
        #  else
        #    val["bar_opening_time"] = val["bar_opening_time"]
        #    val["bar_closing_time"] = val["bar_closing_time"]
        #  end
        #end
      end


      #@ishappyhouravailable=Restaurantdetail.where('id=?', session[:restaurant_id])
      @happyhourstimings=Restauranthappyhour.where('restaurant_id=?', session[:restaurant_id])
      if @happyhourstimings.present? then
        @happyhourstimings.each do |val|
          offset=session[:offset]
          if offset[0]=="-" then
            toaddoffset=offset.split("-")
            val["start_time"] = val['start_time'] + (toaddoffset[1].to_i).to_i.minutes
            val["end_time"] = val['end_time'] + (toaddoffset[1].to_i).to_i.minutes
          elsif offset[0]=="+" then
            toaddoffset=offset.split("+")
            val["start_time"] = val['start_time'] - (toaddoffset[1].to_i).to_i.minutes
            val["end_time"] = val['end_time'] - (toaddoffset[1].to_i).to_i.minutes
          else
            val["start_time"] = val['start_time'] + (offset.to_i).to_i.minutes
            val["end_time"] = val['end_time'] + (offset.to_i).to_i.minutes
          end
        end
      end
    else
      flash[:timingerror]="Something went wrong, please try again later."
    end
  end


  def othersettings
    @restaurantInfo=Restaurantdetail.where("id=?", session[:restaurant_id]).limit(1)
    @restaurantTables = Restauranttable.find(:all, :conditions => ["restaurant_id = ?", @restaurantInfo.first.id])
  end

  def download_qr_code
    qr = RQRCode::QRCode.new(params[:data], :size => 4, :level => :h)
    png = qr.to_img # returns an instance of ChunkyPNG
    png.resize(200, 200).save("#{params[:data]}.png")
    file_path = "#{Rails.root}/#{params[:data]}.png"
    File.open(file_path, 'r') do |f|
      send_data f.read, :filename => "#{params[:data]}.png", type: 'image/png'
    end
    File.delete(file_path)
  end

  def updatesettingoptions
    displayPrice=params[:radiodisplayprices]
    gratuity=params[:radiochargeGratuity]
    if (gratuity=='1') || (gratuity=='2') then
      if params[:gratuity_val].present? then
        isUpdated=Restaurantdetail.where('id=?', session[:restaurant_id]).update_all(:display_price_outside_restaurant => displayPrice, :gratuity_status => gratuity, :gratuity => params[:gratuity_val]);
      end
    else
      isUpdated=Restaurantdetail.where('id=?', session[:restaurant_id]).update_all(:display_price_outside_restaurant => displayPrice, :gratuity_status => gratuity);
    end
    flash[:displayOption]=1
    if isUpdated then
      @restaurantInfo = Restaurantdetail.where("id=?", session[:restaurant_id]).limit(1)
      @messages_rendered=true
      flash[:updatesettingnotice]="Manage options has been updated successfully."
      redirect_to :back
    else
      flash[:updatesettingerror]="Unable to update options."
      redirect_to :back
    end
  end


  def updaterestaurantoptions
    restaurantName=params[:restaurantname]
    address1=params[:address1]
    address2=params[:address2]
    city=params[:city]
    zip=params[:zip]
    state=params[:state]
    country=params[:country]
    phone1=params[:phone1]
    phone2=params[:phone2]
    isUpdated=Restaurantdetail.where('id=?', session[:restaurant_id]).update_all(:restaurant_name => restaurantName, :address1 => address1, :address2 => address2, :phone1 => phone1, :phone2 => phone2, :city => city, :state => state, :country => country, :zip => zip)
    flash[:displayRestaurant]=1
    if isUpdated then
      flash[:updaterestaurantnotice]="Restaurant details has been updated successfully."
      redirect_to :back
    else
      flash[:updaterestauranterror]="Unable to update restaurant details."
      redirect_to :back
    end
  end

  def addTabledetail
    flash[:displayRestaurantTable]=1
    restaurant = Restaurantdetail.find_by_id(params[:restaurant_id])
    redirect_to :back, :updaterestaurantnotice => "Restaurant not found" if restaurant.nil? && return
    existing_qr_codes = Restauranttable.find(:all, :conditions => ["restaurant_id = ? and qrcode in (?)", restaurant.id, params[:qrcodes]]).map { |r| r.qrcode }
    params[:qrcodes].each do |qrcode|
      unless existing_qr_codes.include?(qrcode)
        Restauranttable.find_or_create_by(restaurant_id: restaurant.id, qrcode: qrcode) unless qrcode.blank?
      end
    end
    flash[:updaterestaurantnotice]="Restaurant tables details has been saved successfully."
    redirect_to :back
  end

  def adsubscription
    @allsubscriptions=AdsSubscription.find(:all)
    @allAdDetailsInfo=Advertisement.where('restaurant_id=? && ad_settings != -2', session[:restaurant_id])
  end


  def alladvertisements
    # params.permit!
    #status = params[:payment_status]
    # transactionId=params[:txn_id]
    #render json: {'heer'=>status} and return

    isSubscribed=Restaurantdetail.find(session[:restaurant_id]);
    if isSubscribed.subscription_type == -1 then
      flash[:subscription_type] = -1;
    else
      @allAdDetailsInfo=Advertisement.where('restaurant_id=? && ad_settings!=-2', session[:restaurant_id]) #.paginate(:page => params[:page], :per_page => 7)
      @countVideos=0
      @countImages=0;
      if @allAdDetailsInfo.present? then
        @allAdDetailsInfo.each do |val|
          if (val.avatar).present? then
            val['image']=val.avatar.url(:medium)
          else
            val['image']=root_url[0..-1]+"default.jpg"
          end
          if val['ad_type']== 0 then
            @countVideos=@countVideos+1
          else
            @countImages=@countImages+1
          end
        end
      end
      flash[:subscription_type]=isSubscribed.subscription_type
    end
  end


  def addadvertisement
    allimages=params[:avatar]
    alladnames=params[:ad_name]
    timeperiod=params[:timeperiod]
    subscription_type=params[:subscriptiontype]
    Advertisement.where("ad_settings=? && restaurant_id=?", -2, session[:restaurant_id]).destroy_all
    i=0
    @count=0
    @subscriptionInfo=AdsSubscription.find(:all, :conditions => {:id => subscription_type})
    endDate=Time.now + ((timeperiod[i].to_i)*(@subscriptionInfo[0]['no_of_days'].to_i)).days
    alladnames.each do |val|
      if val.present? then
        name=to_iso8859_1((val.capitalize))
        if allimages.present? && allimages[i].present? then
          if  ((allimages[i].content_type) =~ %r(video)) then
            result = %x(#{avprobe_grab_duration_command(allimages[i].path)})
            videoSeconds=result.split(":")
            seconds=videoSeconds[2].split(".")
            if (videoSeconds[0]=="00" && videoSeconds[1]=="00" && seconds[0]<= "30") then
              @newad=Advertisement.new(:name => name, :ad_type => 0, :ad_settings => -2, :restaurant_id => session[:restaurant_id], :avatar => allimages[i], :ad_subscription_end => endDate, :time_period => timeperiod[i], :subscription_type => subscription_type);
            else
              flash[:largeVideoerror]="Video must be of 30 seconds."
              redirect_to :back and return
            end
          else
            @newad=Advertisement.new(:name => name, :ad_type => 1, :ad_settings => -2, :restaurant_id => session[:restaurant_id], :avatar => allimages[i], :ad_subscription_end => endDate, :time_period => timeperiod[i], :subscription_type => subscription_type);
          end
        else
          @newad=Advertisement.new(:name => name, :ad_type => 1, :ad_settings => -2, :restaurant_id => session[:restaurant_id], :ad_subscription_end => endDate, :time_period => timeperiod[i], :subscription_type => subscription_type);
        end
        if @newad.save then
          @count+=1
        else
          flash[:largeImageerror]=@newad.errors['avatar'][0]
          redirect_to :back and return
        end
      end
      i+=1
    end
    #flash[:adddadvertisementnotice]="Advertisement has been added successfully."
    @object=Advertisement.where('restaurant_id=? && ad_settings = -2', session[:restaurant_id])
    @allsubscriptions=AdsSubscription.find(:all)
    @allAdDetailsInfo=Advertisement.where('restaurant_id=? && ad_settings != -2', session[:restaurant_id])
    #flash[:continue]=1;
    flash.now[:continue]=1
    cookies[:totalads]=@count
    render :action => :adsubscription
  end


  def avprobe_grab_duration_command(raw_file_path)
    command="avprobe " + raw_file_path.shellescape + " 2>&1 | grep -Eo 'Duration: [0-9:.]*' | cut -c 11-"
    return command
  end


  def deleteadvertisement
    deleteId= params[:deleteinputbox]
    isDeleted=Advertisement.find(deleteId).destroy
    if isDeleted then
      flash[:deleteadnotice]= "Advertisement has been deleted successfully."
    else
      flash[:deleteaderror]= "Unable to delete advertisement, please try again after some time."
    end
    redirect_to :back
  end

  def disableadvertisement
    disableId= params[:disableinputbox]
    isdisabled=Advertisement.where("id=?", disableId).update_all(:ad_settings => 0)
    if isdisabled then
      flash[:disablednotice]= "Advertisement has been disabled successfully."
    else
      flash[:disablederror]= "Unable to disable advertisement, please try again after some time."
    end
    redirect_to :back
  end

  def enableadvertisement
    enableId= params[:enableinputbox]
    isenabled=Advertisement.where("id=?", enableId).update_all(:ad_settings => 1)
    if isenabled then
      flash[:enablednotice]= "Advertisement has been enabled successfully."
    else
      flash[:enablederror]= "Unable to enable advertisement, please try again after some time."
    end
    redirect_to :back
  end


  def updatekitchentime
    dayname=["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    kitchenOpeningTimings=Array.new;
    kitchenClosingTimings=Array.new

    #params[:openingtime].each { |val|
    #  #newdate=val.gsub("/","-");
    #  time= val.split(" ");
    #  #finaldatetime= time[0]+" "+ (Time.parse((time[1]+" "+time[2])).strftime("%H:%M:%S"))
    #  #kitchenOpeningTimings<< finaldatetime
    #  if time[2]=="PM" then
    #    from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p').utc
    #  else
    #    from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p').utc
    #  end
    #  #utcDate=from_date
    #  render json: {"here"=>from_date} and return
    #}
    params[:openingtime].each { |val|
      time= val.split(" ");
      if time[2]=="PM" then
        from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
      else
        from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
      end
      #num=(from_date.to_i)
      offset=session[:offset]
      if offset[0]=="-" then
        toaddoffset=offset.split("-")
        utctime= from_date - (toaddoffset[1].to_i).to_i.minutes
      elsif offset[0]=="+" then
        toaddoffset=offset.split("+")
        utctime=from_date + (toaddoffset[1].to_i).to_i.minutes
      else
        utctime=from_date + (offset.to_i).to_i.minutes
      end
      #render json: {"here"=>Time.at(utctime)} and return
      finaltime=(Time.at(utctime)).strftime("%Y-%m-%d %H:%M:%S")
      kitchenOpeningTimings<<finaltime
    }


    params[:closingtime].each { |val|
      time1= val.split(" ");
      if time1[2]=="PM" then
        from_date1 = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
      else
        from_date1 = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
      end
      #num=(from_date.to_i)
      offset1=session[:offset]
      if offset1[0]=="-" then
        toaddoffset1=offset1.split("-")
        utctime1= from_date1 - (toaddoffset1[1].to_i).to_i.minutes
      elsif offset1[0]=="+" then
        toaddoffset1=offset1.split("+")
        utctime1=from_date1 + (toaddoffset1[1].to_i).to_i.minutes
      else
        utctime1=from_date1 + (offset1.to_i).to_i.minutes
      end

      finaltime1=(Time.at(utctime1)).strftime("%Y-%m-%d %H:%M:%S")
      kitchenClosingTimings<<finaltime1
    }
    
    

    (0..((kitchenOpeningTimings.length)-1)).each do |i|
      openingTime= kitchenOpeningTimings[i]
      closingTime= kitchenClosingTimings[i]
      isExisting=Restauranttiming.where("day=? && restaurant_id=?", i, session[:restaurant_id])
      if isExisting.present? then
        Restauranttiming.where("day=? && restaurant_id=?", i, session[:restaurant_id]).update_all(:opening_time => openingTime, :closing_time => closingTime);
      else
        newTime=Restauranttiming.new(:day => i, :is_happy_hours_available => 0, :opening_time => openingTime, :closing_time => closingTime, :restaurant_id => session[:restaurant_id], :day_name => dayname[i])
        newTime.save
      end

    end
    flash[:displayKitchentimings]=1
    flash[:kitchenTimeUpdated]="Restaurant kitchen timings have been updated successfully."
    redirect_to :back
  end


  def updatebartime
    dayname=["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    barOpeningTimings=Array.new;
    barClosingTimings=Array.new
    params[:baropeningtime].each { |val|
      time= val.split(" ");
      if time[2]=="PM" then
        from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
      else
        from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
      end

      offset=session[:offset]

      if offset[0]=="-" then
        toaddoffset=offset.split("-")
        utctime= from_date - (toaddoffset[1].to_i).to_i.minutes
      elsif offset[0]=="+" then
        toaddoffset=offset.split("+")
        utctime=from_date + (toaddoffset[1].to_i).to_i.minutes
      else
        utctime=from_date + (offset.to_i).to_i.minutes
      end
      #render json: {"here"=>Time.at(utctime)} and return
      finaltime=(Time.at(utctime)).strftime("%Y-%m-%d %H:%M:%S")
      barOpeningTimings<<finaltime
    }

    params[:barclosingtime].each { |val|
      time1= val.split(" ");
      if time1[2]=="PM" then
        from_date1 = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
      else
        from_date1 = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
      end


      offset1=session[:offset]
      if offset1[0]=="-" then
        toaddoffset1=offset1.split("-")
        utctime1= from_date1 - (toaddoffset1[1].to_i).to_i.minutes
      elsif offset1[0]=="+" then
        toaddoffset1=offset1.split("+")
        utctime1=from_date1 + (toaddoffset1[1].to_i).to_i.minutes
      else
        utctime1=from_date1 + (offset1.to_i).to_i.minutes
      end
      #render json: {"here"=>Time.at(utctime)} and return
      finaltime1=(Time.at(utctime1)).strftime("%Y-%m-%d %H:%M:%S")
      barClosingTimings<<finaltime1
    }


    (0..((barOpeningTimings.length)-1)).each do |i|
      openingTime= barOpeningTimings[i]
      closingTime=barClosingTimings[i]
      isExisting=Restauranttiming.where("day=? && restaurant_id=?", i, session[:restaurant_id])
      if isExisting.present? then
        Restauranttiming.where("day=? && restaurant_id=?", i, session[:restaurant_id]).update_all(:bar_opening_time => openingTime, :bar_closing_time => closingTime);
      else
        newTime=Restauranttiming.new(:day => i, :is_happy_hours_available => 0, :restaurant_id => session[:restaurant_id], :day_name => dayname[i], :bar_opening_time => openingTime, :bar_closing_time => closingTime)
        newTime.save
      end
    end
    flash[:displayBartimings]=1
    flash[:barTimeUpdated]="Restaurant bar timings have been updated successfully."
    redirect_to :back
  end

  def updatehappyhourtime
    happyhours=params[:radioHappyHours]
    openingTimings=Array.new;
    closingTimings=Array.new
    allIds=Array.new

    if params[:starttime].present? then
      params[:starttime].each { |val|
        if val.present? then
          time= val.split(" ");
          if time[2]=="PM" then
            from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
          else
            from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
          end

          offset=session[:offset]

          if offset[0]=="-" then
            toaddoffset=offset.split("-")
            utctime= from_date - (toaddoffset[1].to_i).to_i.minutes
          elsif offset[0]=="+" then
            toaddoffset=offset.split("+")
            utctime=from_date + (toaddoffset[1].to_i).to_i.minutes
          else
            utctime=from_date + (offset.to_i).to_i.minutes
          end
          #render json: {"here"=>Time.at(utctime)} and return
          finaltime=(Time.at(utctime)).strftime("%Y-%m-%d %H:%M:%S")
          openingTimings<<finaltime
        end
      }
      params[:endtime].each { |val|
        if val.present? then
          time= val.split(" ");
          if time[2]=="PM" then
            from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
          else
            from_date = DateTime.strptime(val, '%m/%d/%Y %H:%M %p')
          end

          offset=session[:offset]

          if offset[0]=="-" then
            toaddoffset=offset.split("-")
            utctime= from_date - (toaddoffset[1].to_i).to_i.minutes
          elsif offset[0]=="+" then
            toaddoffset=offset.split("+")
            utctime=from_date + (toaddoffset[1].to_i).to_i.minutes
          else
            utctime=from_date + (offset.to_i).to_i.minutes
          end
          #render json: {"here"=>Time.at(utctime)} and return
          finaltime=(Time.at(utctime)).strftime("%Y-%m-%d %H:%M:%S")
          closingTimings<<finaltime
        end
      }
      params[:id].each { |val|
        if val.present? then
          allIds<<val
        end
      }

      (0..((openingTimings.length)-1)).each do |i|
        openingTime= openingTimings[i]
        closingTime= closingTimings[i]
        if allIds[i].present? then
          isExists=Restauranthappyhour.where("id=? && restaurant_id=? ", allIds[i], session[:restaurant_id])
          if isExists.present? then
            Restauranthappyhour.where("id=? && restaurant_id=?", allIds[i], session[:restaurant_id]).update_all(:start_time => openingTime, :end_time => closingTime)
          else
            newSession=Restauranthappyhour.create(:session_name => "Session"+((i+1).to_s), :start_time => openingTime, :end_time => closingTime, :restaurant_id => session[:restaurant_id])
            newSession.save
          end
        else
          newSession=Restauranthappyhour.create(:session_name => "Session"+((i+1).to_s), :start_time => openingTime, :end_time => closingTime, :restaurant_id => session[:restaurant_id])
          newSession.save
        end
      end
    end
    Restaurantdetail.where('id=?', session[:restaurant_id]).update_all(:is_happy_hour_available => happyhours)
    flash[:displayHappyHourtimings]=1
    flash[:happyhourTimeUpdated]="Restaurant Happy hour timings have been updated successfully."
    redirect_to :back
  end


  def forgotpassword
    @admin = Admin.find_for_database_authentication email: params[:resetemail]
    if @admin.present? then
      @admin.send_password_reset
      if (Usermailer.forgot_admin_password(@admin).deliver) then
        flash[:sucessmsg]= "1"
      else
        flash[:mailerror]= "0"
      end
    else
      flash[:unauthorizedmailerror]= "0"
    end
    redirect_to :back
  end

  def resetpassword

  end

  def state_options
    render partial: 'getstate'
  end


=begin
def paybill
    subscritionTime=params[:priceMenuradio]
    if (subscritionTime == "1") then
      totalbill=(9.95*4)
      name="Weekly ad images"
    elsif (subscritionTime=="2") then
      totalbill=(500*4)
      name="Yearly ad images"
    elsif (subscritionTime=="3") then
      totalbill=(15.95*4)
      name="Weekly ad videos"
    elsif (subscritionTime=="4") then
      totalbill=(800*4)
      name="Yearly ad videos"
    end
    #render json: {'here'=>subscritionTime} and return
    values = {
        business: "chirag.gupta@clicklabs.in",
        cmd: "_xclick",
        upload: 1,
        return: "http://54.191.77.101:3000/admins/updatesubscription",
        amount: 0.50, #totalbill
        item_name: name,
        item_number: session[:restaurant_id],
        notify_url: "http://54.191.77.101:3000/admins/updatesubscription"
    }
    redirect_to "https://www.sandbox.paypal.com/cgi-bin/webscr?" + values.to_query
  end

  protect_from_forgery except: [:updatesubscription]

  def updatesubscription
    params.permit! # Permit all Paypal input params
    status = params[:payment_status]
    #render json:{'here'=>params[:item_number]} and return
    if status == "Completed"
      @details = Restaurantdetail.find((params[:item_number].to_i))
      Restaurantdetail.where("id=?", (params[:item_number].to_i)).update_all(:is_paid => 1, :transaction_id => params[:txn_id])
    end
    redirect_to admins_alladvertisements_path
  end


  protect_from_forgery except: [:updatetransactionid]

  def updatetransactionid
    params.permit! # Permit all Paypal input params
    status = params[:payment_status]
    if status == "Completed"
      @admin = Admin.find(params[:invoice])
      @admin.update_attributes is_paid: 1, transaction_id: params[:txn_id]
    end
    redirect_to new_admin_session_path
  end
=end


  def makepayment
    Stripe.api_key = "sk_live_eFr3TqziO0TnMT3xMxaAeeSt"
    require 'stripe'
    token=params[:stripeToken]
    id= params[:id]
    
    begin
      customer = Stripe::Customer.create(
          :card => token,
          :description => 'Payment for admin'
      )
    rescue Stripe::CardError => e
      # Since it's a decline, Stripe::CardError will be caught
      body = e.json_body
      err = body[:error]
      render :json => {"error" => err[:message]} and return
    end 
    begin
      # Charge the Customer instead of the card
      charge=Stripe::Charge.create(
          :amount => (((300).to_f)*100).to_i, # in cents
          :currency => "usd",
          :customer => customer.id
      )
    rescue Stripe::CardError => e
      # Since it's a decline, Stripe::CardError will be caught
      body = e.json_body
      err = body[:error]
      render :json => {"error" => err[:message]} and return
    end

    if charge['paid'] then
      session[:id]=nil
      Admin.where("id=?", id).update_all(:is_paid => 1, :transaction_id => customer.id);
      flash[:paymentsuccess]="You have successfully paid for this account, please wait for confirmation."
    else
      flash[:paymenterror]="Something went wrong, please try again later."
    end
    redirect_to :back
  end


  def paymentdetails
  end


  def makesubscription
    #render json: {'here'=>params[:subscriptiontype]} and return
    totalbill=params[:totalbill]
    Stripe.api_key = "sk_live_eFr3TqziO0TnMT3xMxaAeeSt"
    require 'stripe'
    token=params[:stripeToken]
    id= params[:id]
    begin
      customer = Stripe::Customer.create(
          :card => token,
          :description => 'Payment for admin'
      )
    rescue Stripe::CardError => e
      # Since it's a decline, Stripe::CardError will be caught
      body = e.json_body
      err = body[:error]
      render :json => {"error" => err[:message]} and return
    end
    begin
      # Charge the Customer instead of the card
      charge=Stripe::Charge.create(
          :amount => ((params[:totalbill].to_f)*100).to_i, # in cents
          :currency => "usd",
          :customer => customer.id
      )
    rescue Stripe::CardError => e
      # Since it's a decline, Stripe::CardError will be caught
      body = e.json_body
      err = body[:error]
      render :json => {"error" => err[:message]} and return
    end

    #if charge['paid'] then
    if charge['paid'] then
      allInfo=Restaurantdetail.where('id=?', session[:restaurant_id]).limit(1);
      if params[:subscriptiontype]=="Image" then
        subscription=(allInfo.first['subscription_type'] == -1 || allInfo.first['subscription_type'] == 1) ? 1 : 2
      elsif params[:subscriptiontype]=="Video" then
        subscription=(allInfo.first['subscription_type'] == -1 || allInfo.first['subscription_type'] == 0) ? 0 : 2
      end
      Restaurantdetail.where('id=?', session[:restaurant_id]).update_all(:subscription_type => subscription)
      Advertisement.where("restaurant_id=? && ad_settings=?", session[:restaurant_id], -2).update_all(:ad_settings => 1)
      flash[:paymentsuccessmessage]=1
    else
      flash[:paymenterror]="Something went wrong, please try again later."
    end
    #render json: {'here'=>flash[:paymentsuccessmessage]} and return
    redirect_to :back
  end

  def to_iso8859_1(value)
    finalValue=Iconv.iconv('ISO-8859-1', 'UTF-8', value).join
    return finalValue;
  end


  def to_utf_8(value)
    finalValue= Iconv.iconv('UTF-8', 'ISO-8859-1', value).join
    return finalValue;
  end

  def is_video?
    avatar.content_type =~ %r(video)
  end


  def person_params
    params.permit(:opening_time, :closing_time, :bar_opening_time, :bar_closing_time, :is_happy_hours_available)
  end

end
