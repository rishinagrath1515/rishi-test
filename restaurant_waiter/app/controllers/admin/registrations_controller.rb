class Admin::RegistrationsController < Devise::RegistrationsController
  before_filter :authenticate_super_admin!
  before_filter :get_super_admin_notifications
  before_filter :configure_sign_up_params, only: [:create]
# before_filter :configure_account_update_params, only: [:update]
  layout 'admin_dashboard'
  # GET /resource/sign_up
  def new
     @register_class = 'active'
     build_resource({})
     @validatable = devise_mapping.validatable?
     if @validatable
       @minimum_password_length = resource_class.password_length.min
     end

     respond_with self.resource
  end

  # POST /resource
  def create
    build_resource(sign_up_params)
    admin_password = sign_up_params['password']
    begin
      Admin.transaction do
        restaurant = Restaurantdetail.new((params.require(:restaurantdetail).permit(:restaurant_name, :restaurant_email,:name, :address1, :address2, :address3, :city, :state, :zip, :country, :phone1, :phone2)))
        restaurant.display_price_outside_restaurant = 0
        restaurant.gratuity_status = 0
        restaurant.subscription_type = -1
        restaurant.save
        resource.restaurant_id= restaurant.id
        resource.confirm_status = 0
        resource.is_paid = 0
        Gimbaldetail.create!(:gimbal_id=> params[:restaurantdetail]['beacon_id'].to_i , :restaurant_id => restaurant.id)
        @resource_saved = resource.save!
        # Sending mail to user who is registered
        SuperAdminMailer.admin_creation_mail(resource,request,admin_password).deliver
        SuperAdminMailer.send_mail_to_sales(resource,request,admin_password).deliver
        # TO DO
        # send mail to sales department
      end
    rescue Exception => e
        resource.errors.add(:base, e.to_s) if resource.errors.empty?
    end
    yield resource if block_given?
    if @resource_saved
      if resource.active_for_authentication?
        flash[:success] = "Details Registered successfully." if is_flashing_format?
        #sign_up(resource_name, resource)
        respond_with resource, location: after_sign_in_path_for_super_admin(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_sign_in_path_for_super_admin(resource)
      end
    else
      clean_up_passwords resource
      @validatable = devise_mapping.validatable?
      if @validatable
        @minimum_password_length = resource_class.password_length.min
      end
      flash[:notice]= "#{resource.errors.full_messages.join(',<br>')}".html_safe
      respond_with resource, location: after_sign_in_path_for_super_admin(resource)
    end
  end

  def subregion_options
    render partial: 'subregion_select'
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

   protected

  # You can put the params you want to permit in the empty array.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit( :name,:email, :password, :password_confirmation,:restaurantdetail, restaurantdetail: [:restaurant_name, :restaurant_email,:name, :address1, :address2, :address3, :city, :state, :zip, :country, :phone1, :phone2, :beacon_id]) }
  end

  # You can put the params you want to permit in the empty array.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.for(:account_update) << :attribute
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
  def require_no_authentication
    assert_is_devise_resource!
    return unless is_navigational_format?
    no_input = devise_mapping.no_input_strategies
    authenticated = if no_input.present?
                      args = no_input.dup.push scope: resource_name
                      warden.authenticate?(*args)
                    else
                      warden.authenticated?(resource_name)
                    end
    if authenticated && resource = warden.user(resource_name)
      #flash[:alert] = I18n.t("devise.failure.already_authenticated")
      #redirect_to after_sign_in_path_for_super_admin(resource)
    end
  end
end
