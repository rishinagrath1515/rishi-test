class RolesController < ApplicationController
  authorize_resource
  before_filter :authenticate_super_admin!
  before_filter :get_super_admin_notifications
  before_action :set_role, only: [:show, :edit, :update, :destroy]

  layout 'admin_dashboard'
  # GET /roles
  # GET /roles.json
  def index
    @role_class = 'active'
    @role_path_class = 'active'
    @open_role = 'open'
    @roles = Role.all
  end

  # GET /roles/1
  # GET /roles/1.json
  def show
  end

  # GET /roles/new
  def new
    @role = Role.new
  end

  # GET /roles/1/edit
  def edit
  end

  # POST /roles
  # POST /roles.json
  def create
    @role = Role.new(role_params)
    respond_to do |format|

      if @role.save!
        format.html { redirect_to @role, notice: 'Role was successfully created.' }
        format.json { render :json =>  {:success => true , :role_id => @role.id} }
      else
        puts @role.errors.full_messages
        format.html { render action: 'new' }
        format.json { render :json =>  {:success => false } }
      end
    end
  end

  # PATCH/PUT /roles/1
  # PATCH/PUT /roles/1.json
  def update
    respond_to do |format|
      puts role_params
      if @role.update(role_params)
        format.html { redirect_to @role, notice: 'Role was successfully updated.' }
        format.json { render :json =>  {:success => true } }
      else
        format.html { render action: 'edit' }
        format.json { render :json =>  {:success => false } }
      end
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    associated = SuperAdmin.find_by_role_id(@role.id)
    unless associated
      @role.destroy
    else
      render :json =>  {:success => false , :message => "Already Associated"}
      return
    end
    respond_to do |format|
      format.html { redirect_to roles_url }
      format.json { render :json =>  {:success => true } }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role
      @role = Role.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def role_params
      params.require(:role).permit(:name)
    end
end
