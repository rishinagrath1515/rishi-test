=begin
  @ Description :This Contains all the methods related to the advertisements.
  @ Copyright :
  @ Developed by : Click Labs Pvt. Ltd
=end

class AdvertisementsController < ApplicationController

#to test methods of advertisement class(shift control to test file)
  def ad_details
  end


=begin
  @ Description :  Get all advertisements associated with a restaurant
  @input : useraccesstoken,restaurant_id
  @output : display all the ads(these can be images or videos)
=end
=begin
  def get_ad_details
    if (params[:useraccesstoken].blank?) || (params[:restaurant_id].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      checkSubscription=Restaurantdetail.select('subscription_type').where("id=?", params[:restaurant_id]).limit(1)
      if checkSubscription.present? then
        allAdDetails=Advertisement.where('restaurant_id=? && ad_settings = 1', params[:restaurant_id]);
        if allAdDetails.present? then
          allAdDetailsInfo=allAdDetails.to_a.map(&:serializable_hash)
          allAdDetailsInfo.each do |val|
            if val['ad_settings']==1 then
              if val['ad_type']==1 then
                val['image']=root_url[0..-1]+"adimage/"+val['image']
                val.delete('video_name')
                val.delete('video_thumb')
              else
                val['video_name']=root_url[0..-1]+"advideo/"+val['video_name']
                val['video_thumb']=root_url[0..-1]+"advideo/videothumb/"+val['video_thumb']
                val.delete('image')
              end
            end
          end
          render :json => JSON.pretty_generate({"data" =>{'alladInfo'=>allAdDetailsInfo,'subscription_type'=>checkSubscription[0]['subscription_type']}})
        else
          render :json => JSON.pretty_generate({"error" => "No advertisement available."})
        end
      else
        render :json => JSON.pretty_generate({"error" => "Something went wrong."})
      end
    end
  end
=end

 def get_ad_details
    if (params[:useraccesstoken].blank?) || (params[:restaurant_id].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      checkSubscription=Restaurantdetail.select('subscription_type').where("id=?", params[:restaurant_id]).limit(1)
      if checkSubscription.present? then
        time=(Time.now.in_time_zone(params[:timezone])).strftime("%Y-%m-%d %H:%M:%S")
        if checkSubscription[0]['subscription_type'] == 0 then
            #@allAdDetails = Advertisement.find(:all, :conditions => {:restaurant_id => params[:restaurant_id],:ad_settings => 1,:ad_type=> 0,:ad_subscription_end=> })
             @allAdDetails = Advertisement.find(:all, :conditions => ["restaurant_id=? && ad_settings=? && ad_type=? && ad_subscription_end >=?", params[:restaurant_id], 1,0,time])
        elsif checkSubscription[0]['subscription_type'] == 1 then
            #@allAdDetails = Advertisement.find(:all, :conditions => {:restaurant_id => params[:restaurant_id],:ad_settings => 1,:ad_type=> 1 })
            @allAdDetails = Advertisement.find(:all, :conditions => ["restaurant_id=? && ad_settings=? && ad_type=? && ad_subscription_end >=?", params[:restaurant_id], 1,1,time])
        else
            #@allAdDetails = Advertisement.find(:all, :conditions => {:restaurant_id => params[:restaurant_id],:ad_settings => 1 })
            @allAdDetails = Advertisement.find(:all, :conditions => ["restaurant_id=? && ad_settings=? && ad_subscription_end >=?", params[:restaurant_id], 1,time])
        end
        if @allAdDetails.present? then
          @allAdDetails.each do |val|
                 if (val.avatar).present? then
                   val['image']=val.avatar.url
                   val['video_thumb']=val.avatar.url(:thumb)
                 else
                   val['image']=root_url[0..-1]+"default.jpg"
                   val['video_thumb']=root_url[0..-1]+"default.jpg"
                 end
          end
          render :json => JSON.pretty_generate({"data" =>{'alladInfo'=>@allAdDetails.to_a.map(&:serializable_hash),'subscription_type'=>checkSubscription[0]['subscription_type']}})
        else
          render :json => JSON.pretty_generate({"error" => "No advertisement available."})
        end
      else
        render :json => JSON.pretty_generate({"error" => "Something went wrong."})
      end
    end
  end
#end  get_ad_details

  private
#error in case invalid parameters passed.
  def invalid_params_msg
    render :json => JSON.pretty_generate({"error" => "some parameter missing"})
  end

#error in case invalid access token(unauthorized access).
  def unauthorized_user
    render :json => JSON.pretty_generate({"error" => "Unauthorized access."})
  end
end
