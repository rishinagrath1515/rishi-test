=begin
  @ Description :This Contains all the methods related to the Drinks.
  @ Copyright :
  @ Developed by : Click Labs Pvt. Ltd
=end

class DrinksController < ApplicationController

#to test methods of Drink class(shift control to test file)
  def drinkdetails
  end


=begin
  @ Description :  Get list of all categories available in drinks
  @input : useraccesstoken,restaurantid
  @output : display list of catgories if available else error message
=end
   def getalldrinkcategories

    if (params[:useraccesstoken].blank?) || (params[:restaurantid].blank?) || (params[:timezone].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      today=((Time.now.in_time_zone(params[:timezone])).wday)
      restaurantDetails=Restauranttiming.select('id,opening_time, closing_time,bar_opening_time,bar_closing_time').where("restaurant_id=? && day=? ", params[:restaurantid], today).limit(1)
      if restaurantDetails.present? then
        currentTime=(Time.now.in_time_zone(params[:timezone]))

        localrestaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
        localrestaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
        localbarOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
        localbarClosingTime=convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])
        #render json: {"finalopeningtime"=>localrestaurantOpeningTime,"finalclosingtime"=>localrestaurantClosingTime,"finalbaropeningtime"=>localbarOpeningTime,"finalbarclosingtime"=>localbarClosingTime} and return
        if ((localrestaurantClosingTime.strftime("%Y-%m-%d")) == (localrestaurantOpeningTime.strftime("%Y-%m-%d"))) && ((localbarClosingTime.strftime("%Y-%m-%d")) == (localbarOpeningTime.strftime("%Y-%m-%d"))) then
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))

        elsif ((localrestaurantClosingTime.strftime("%Y-%m-%d")) > (localrestaurantOpeningTime.strftime("%Y-%m-%d"))) && ((localbarClosingTime.strftime("%Y-%m-%d")) == (localbarOpeningTime.strftime("%Y-%m-%d"))) then
          nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))

        elsif ((localrestaurantClosingTime.strftime("%Y-%m-%d")) == (localrestaurantOpeningTime.strftime("%Y-%m-%d"))) && ((localbarClosingTime.strftime("%Y-%m-%d")) > (localbarOpeningTime.strftime("%Y-%m-%d"))) then
          nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))

        elsif ((localrestaurantClosingTime.strftime("%Y-%m-%d")) > (localrestaurantOpeningTime.strftime("%Y-%m-%d"))) && ((localbarClosingTime.strftime("%Y-%m-%d")) > (localbarOpeningTime.strftime("%Y-%m-%d"))) then
          nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))
        else
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))
        end

        if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalopeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalclosingtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalbaropeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalbarclosingtime) then
          allcategories=Drinkcategory.find(:all, :conditions => ["restaurant_id =?", params[:restaurantid]])
        elsif ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalopeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalclosingtime) && (((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalbaropeningtime) || ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalbarclosingtime)) then
          allcategories=Drinkcategory.find(:all, :conditions => ["restaurant_id =? && include_in_bar=?", params[:restaurantid], 0])
        elsif (((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalopeningtime) || ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalclosingtime)) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalbaropeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalbarclosingtime) then
          allcategories=Drinkcategory.find(:all, :conditions => ["restaurant_id =? && include_in_bar=?", params[:restaurantid], 1])
        else
          allcategories=Drinkcategory.find(:all, :conditions => ["restaurant_id =? && include_in_bar=?", params[:restaurantid], 0])
        end
        #allcategories=Drinkcategory.find(:all, :conditions => ["restaurant_id =? && include_in_bar=?", params[:restaurantid], 1])
        if allcategories.present? then
          allcategories.each do |val|
            val[:drinkcategoryname]=to_utf_8(val[:drinkcategoryname])
            if (val.avatar).present? then
              val['image']=val.avatar.url
            else
              val['image']=root_url[0..-1]+"default.jpg"
            end
          end
          render :json => JSON.pretty_generate({'data' => (allcategories.to_a.map(&:serializable_hash))})
        else
          render :json => JSON.pretty_generate({'error' => 'Some information not found.'})
        end
      else
        render :json => JSON.pretty_generate({'error' => 'No categories available.'})
      end
    end
  end

#end  getalldrinkcategories


=begin
  @ Description :  Get all drinks available in a particular drink category
  @input : useraccesstoken,drinkcategoryid
  @output : display list of drinks available in a category
=end
  def getalldrinks
    if (params[:useraccesstoken].blank?) || (params[:drinkcategoryid].blank?) || (params[:timezone].blank?) || (params[:restaurantid].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      today=((Time.now.in_time_zone(params[:timezone])).wday)
      restaurantDetails=Restauranttiming.select('id,opening_time,closing_time,bar_opening_time,bar_closing_time').where("restaurant_id=? && day=? ", params[:restaurantid], today).limit(1)
      if restaurantDetails.present? then
        currentTime=(Time.now.in_time_zone(params[:timezone]))

        localrestaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
        localrestaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
        localbarOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
        localbarClosingTime=convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])

        if ((localrestaurantClosingTime.strftime("%Y-%m-%d")) == (localrestaurantOpeningTime.strftime("%Y-%m-%d"))) && ((localbarClosingTime.strftime("%Y-%m-%d")) == (restaurantDetails[0]['bar_opening_time'].strftime("%Y-%m-%d"))) then
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))

        elsif ((localrestaurantClosingTime.strftime("%Y-%m-%d")) > (localrestaurantOpeningTime.strftime("%Y-%m-%d"))) && ((localbarClosingTime.strftime("%Y-%m-%d")) == (restaurantDetails[0]['bar_opening_time'].strftime("%Y-%m-%d"))) then
          nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))

        elsif ((localrestaurantClosingTime.strftime("%Y-%m-%d")) == (localrestaurantOpeningTime.strftime("%Y-%m-%d"))) && ((localbarClosingTime.strftime("%Y-%m-%d")) > (restaurantDetails[0]['bar_opening_time'].strftime("%Y-%m-%d"))) then
          nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))

        elsif ((localrestaurantClosingTime.strftime("%Y-%m-%d")) > (localrestaurantOpeningTime.strftime("%Y-%m-%d"))) && ((localbarClosingTime.strftime("%Y-%m-%d")) > (restaurantDetails[0]['bar_opening_time'].strftime("%Y-%m-%d"))) then
          nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))
        else
          finalopeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantOpeningTime.strftime("%H:%M:%S"))
          finalclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localrestaurantClosingTime.strftime("%H:%M:%S"))
          finalbaropeningtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarOpeningTime.strftime("%H:%M:%S"))
          finalbarclosingtime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localbarClosingTime.strftime("%H:%M:%S"))

        end


        #render json: {'here'=>(currentTime.strftime("%Y-%m-%d %H:%M:%S")),'finalopeningtime'=>finalopeningtime,'finalclosingtime'=>finalclosingtime,'finalbaropeningtime'=>finalbaropeningtime,'finalbarclosingtime'=>finalbarclosingtime} and return
        if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalopeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalclosingtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalbaropeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalbarclosingtime) then
          alldrinks=Alldrinks.select(" id, drink_name,avatar_file_name,avatar_content_type,avatar_file_size,avatar_updated_at").where("drink_category_id=?", params[:drinkcategoryid])
        elsif ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalopeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalclosingtime) && !(((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalbaropeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalbarclosingtime)) then
          alldrinks=Alldrinks.select(" id, drink_name,avatar_file_name,avatar_content_type,avatar_file_size,avatar_updated_at").where("drink_category_id=? && include_in_bar=?", params[:drinkcategoryid], 0)
        elsif !(((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalopeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalclosingtime)) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalbaropeningtime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalbarclosingtime) then
          alldrinks=Alldrinks.select(" id, drink_name,avatar_file_name,avatar_content_type,avatar_file_size,avatar_updated_at").where("drink_category_id=? && include_in_bar=?", params[:drinkcategoryid], 1)
        else
          alldrinks=Alldrinks.select(" id, drink_name,avatar_file_name,avatar_content_type,avatar_file_size,avatar_updated_at").where("drink_category_id=? && include_in_bar=?", params[:drinkcategoryid], 0)
        end
        if alldrinks.present? then
          alldrinks.each do |val|
            val[:drink_name]=to_utf_8(val[:drink_name])
            if (val.avatar).present? then
              val['image']=val.avatar.url
            else
              val['image']=root_url[0..-1]+"default.jpg"
            end
          end
          render :json => JSON.pretty_generate({'data' => (alldrinks.to_a.map(&:serializable_hash))})
        else
          render :json => JSON.pretty_generate({'error' => 'Some information not found.'})
        end
      else
        render :json => JSON.pretty_generate({'error' => 'No drinks available in this category.'})
      end
    end
  end


#end  getalldrinks


=begin
  @ Description :  Get Details of the a drink
  @input : useraccesstoken,drinkid
  @output : display details of drink with bar opening and closing time
=end
  def getdrinkdetail
    if (params[:useraccesstoken].blank?) || (params[:drinkid].blank?) || (params[:timezone].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      alldrinkdetail=Alldrinks.find(:all, :conditions => ["id =?", params[:drinkid]])
      if alldrinkdetail.present? then
        if (alldrinkdetail[0].avatar).present? then
          alldrinkdetail[0]['image']=alldrinkdetail[0].avatar.url
        else
          alldrinkdetail[0]['image']=root_url[0..-1]+"default.jpg"
        end
        alldrinkdetail[0]['drink_name']=to_utf_8(alldrinkdetail[0]['drink_name'])
        alldrinkdetail[0]['description']=to_utf_8(alldrinkdetail[0]['description'])
        allSizesAndPrices= Drinkpricesandsize.where('drink_id=?', alldrinkdetail[0]['id']);
        if allSizesAndPrices.present? then
          alldrinkdetail=alldrinkdetail.to_a.map(&:serializable_hash)
          alldrinkdetail[0][:allsizes]=Hash.new
          allSizesAndPricesInfo=allSizesAndPrices.to_a.map(&:serializable_hash)
          today=((Time.now.in_time_zone(params[:timezone])).wday)
          nextday = today+1
          nextDayCheck = nextday==7 ? 0 : nextday
          restaurantDetails=Restauranttiming.select('id,day,opening_time,closing_time,bar_opening_time,bar_closing_time,is_happy_hours_available,happy_hour_start_time,happy_hour_end_time').where("(restaurant_id=? && day=?) || (restaurant_id=? && day=?) ", alldrinkdetail[0]['restaurant_id'], today, alldrinkdetail[0]['restaurant_id'], nextDayCheck)
          if restaurantDetails.present?
            if restaurantDetails[0]['day']== today then
              kitchenopentime = convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
              kitchenclosetime = convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
              restaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
              restaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])
            else
              kitchenopentime = convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time'])
              kitchenclosetime = convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time'])
              restaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[1]['bar_opening_time'])
              restaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[1]['bar_closing_time'])
            end
            #render json: {"kitchenopentime"=>kitchenopentime,"kitchenclosetime"=>kitchenclosetime} and return
            currentTime= (Time.now.in_time_zone(params[:timezone]))
            ishappyhouravailable=Restaurantdetail.where('id=?', alldrinkdetail[0]['restaurant_id']).limit(1)
            if ishappyhouravailable[0]['is_happy_hour_available']== 1 then
              happyhourstimings=Restauranthappyhour.where('restaurant_id=?', alldrinkdetail[0]['restaurant_id'])
              isHappyHoursStarted=0;
              happyhourstimings.each do |val|
                localStartTime=convertutctolocal(params[:timezone],val['start_time'])
                localEndTime=convertutctolocal(params[:timezone],val['end_time'])
                if (localEndTime.strftime("%Y-%m-%d")) == (localStartTime.strftime("%Y-%m-%d")) then
                  finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
                  finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
                elsif (localEndTime.strftime("%Y-%m-%d")) > (localStartTime.strftime("%Y-%m-%d")) then
                  nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
                  finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
                  finalEndTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
                else
                  finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
                  finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
                end
                if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalStartTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalEndTime) then
                  isHappyHoursStarted=1
                  break
                else
                  isHappyHoursStarted=0
                end
              end
            else
              isHappyHoursStarted=0
            end
            #time_diff_components = Time.diff(restaurantClosingTime, currentTime)
            #if (currentTime >= restaurantOpeningTime) && (currentTime <= (restaurantClosingTime)) then
            #  if time_diff_components[:hour]== 0 && time_diff_components[:minute]<=15 then
            #    flag=0
            #    next_day_opening_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[1]['bar_opening_time'] : restaurantDetails[0]['bar_opening_time']
            #    next_day_closing_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[1]['bar_closing_time'] : restaurantDetails[0]['bar_closing_time']
            #  else
            #    flag=1
            #    next_day_opening_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[1]['bar_opening_time'] : restaurantDetails[0]['bar_opening_time']
            #    next_day_closing_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[1]['bar_closing_time'] : restaurantDetails[0]['bar_closing_time']
            #  end
            #elsif (currentTime < restaurantOpeningTime) then
            #  flag=0;
            #  next_day_opening_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[0]['bar_opening_time'] : restaurantDetails[1]['bar_opening_time']
            #  next_day_closing_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[0]['bar_closing_time'] : restaurantDetails[1]['bar_closing_time']
            #else
            #  flag=0
            #  next_day_opening_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[1]['bar_opening_time'] : restaurantDetails[0]['bar_opening_time']
            #  next_day_closing_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[1]['bar_closing_time'] : restaurantDetails[0]['bar_closing_time']
            #end

            if ((restaurantClosingTime.strftime("%Y-%m-%d")) == (restaurantOpeningTime.strftime("%Y-%m-%d"))) && ((kitchenopentime.strftime("%Y-%m-%d")) == (kitchenopentime.strftime("%Y-%m-%d"))) then
              finalRestaurantClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
              finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
              finalKitchenOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (kitchenopentime.strftime("%H:%M:%S"))
              finalKitchenClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (kitchenclosetime.strftime("%H:%M:%S"))
            elsif ((restaurantClosingTime.strftime("%Y-%m-%d")) > (restaurantOpeningTime.strftime("%Y-%m-%d"))) && ((kitchenopentime.strftime("%Y-%m-%d")) == (kitchenopentime.strftime("%Y-%m-%d"))) then
              nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
              finalRestaurantClosingTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
              finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
              finalKitchenOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (kitchenopentime.strftime("%H:%M:%S"))
              finalKitchenClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (kitchenclosetime.strftime("%H:%M:%S"))
            elsif ((restaurantClosingTime.strftime("%Y-%m-%d")) == (restaurantOpeningTime.strftime("%Y-%m-%d"))) && ((kitchenopentime.strftime("%Y-%m-%d")) > (kitchenopentime.strftime("%Y-%m-%d"))) then
              nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
              finalRestaurantClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
              finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
              finalKitchenOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (kitchenopentime.strftime("%H:%M:%S"))
              finalKitchenClosingTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (kitchenclosetime.strftime("%H:%M:%S"))
            else
              nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
              finalRestaurantClosingTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
              finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
              finalKitchenOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (kitchenopentime.strftime("%H:%M:%S"))
              finalKitchenClosingTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (kitchenclosetime.strftime("%H:%M:%S"))
            end
            #render json: {"currentTime"=>(currentTime.strftime("%Y-%m-%d %H:%M:%S")),"finalRestaurantOpeningTime"=>finalRestaurantOpeningTime,"finalRestaurantClosingTime"=>finalRestaurantClosingTime,"finalKitchenOpeningTime"=>finalKitchenOpeningTime,"finalKitchenClosingTime"=>finalKitchenClosingTime} and return
            time_diff_components = Time.diff(finalRestaurantClosingTime, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))
            time_diff_components1 = Time.diff(finalKitchenClosingTime, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))
            if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalRestaurantOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalRestaurantClosingTime)) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalKitchenOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalKitchenClosingTime)) then
              if time_diff_components[:day]== 0 && time_diff_components[:hour]== 0 && time_diff_components[:minute]<=15 then
                flag=0
                next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
                next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])
              else
                flag=1
                next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
                next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])
              end
            elsif ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalRestaurantOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalRestaurantClosingTime)) && !(((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalKitchenOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalKitchenClosingTime))) then

              if time_diff_components[:day]== 0 && time_diff_components[:hour]== 0 && time_diff_components[:minute]<=15 then
                flag=0
                next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
                next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])
              else
                flag=1
                next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
                next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_closing_time']) : convertutctolocal(params[:timezone], restaurantDetails[0]['bar_closing_time'])
              end
            elsif  !(((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalRestaurantOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalRestaurantClosingTime))) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalKitchenOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalKitchenClosingTime)) then
              if time_diff_components[:day]== 0 && time_diff_components1[:hour]== 0 && time_diff_components1[:minute]<=15 then
                flag=0
                next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
                next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_closing_time']): convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])
              else
                flag=1
                next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
                next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])
              end
              #elsif (currentTime < restaurantOpeningTime) then
              #  flag=0;
              #  next_day_opening_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[0]['bar_opening_time'] : restaurantDetails[1]['bar_opening_time']
              #  next_day_closing_time = (restaurantDetails[0]['day']== today) ? restaurantDetails[0]['bar_closing_time'] : restaurantDetails[1]['bar_closing_time']
            else
              flag=0
                next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_opening_time'])
                next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['bar_closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['bar_closing_time'])
            end
            alldrinkdetail[0][:allsizes]=allSizesAndPricesInfo
            render :json => JSON.pretty_generate({'data' => alldrinkdetail, 'flag' => flag, 'isHappyHoursStarted' => isHappyHoursStarted, 'next_day_opening_time' => (next_day_opening_time.strftime("%H:%M:%S")), 'next_day_closing_time' => (next_day_closing_time.strftime("%H:%M:%S"))})
          else
            render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
          end
        else
          render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
        end
      else
        render :json => JSON.pretty_generate({'error' => 'An error occurred. Please try again later.'})
      end
    end
  end

#end  getdrinkdetail
  def convertutctolocal(timezone, date)
    offset= Time.now.in_time_zone(timezone).utc_offset
    totaltime = Time.now.in_time_zone(timezone)
    utctime=''
    if (totaltime.to_s).include? "+"
      if (offset.to_s).include? "+" then
        finaloffset=(offset.to_s).split("+")
        utctime= date + (finaloffset[1].to_i).to_i.seconds
      else
        utctime= date + (offset.to_i).to_i.seconds
      end
    else
      if (offset.to_s).include? "-" then
        finaloffset=(offset.to_s).split("-")
        utctime= date - (finaloffset[1].to_i).to_i.seconds
      else
        utctime= date - (offset.to_i).to_i.seconds
      end
    end
    return utctime
  end

  private

  #error in case invalid parameters passed.
  def invalid_params_msg
    render :json => JSON.pretty_generate({"error" => "some parameter missing"})
  end

#error in case invalid access token(unauthorized access).
  def unauthorized_user
    render :json => JSON.pretty_generate({"error" => "Unauthorized access."})
  end

  def to_utf_8(value)
    finalValue= Iconv.iconv('UTF-8', 'ISO-8859-1', value).join
    return finalValue;
  end
end
