=begin
  @ Description :This Contains all the methods related to the Categories(Food Items + Extras).
  @ Copyright :
  @ Developed by : Click Labs Pvt. Ltd
=end

class AllcategoriesController < ApplicationController

#to test methods of categories class(shift control to test file)
  def getallcategories
  end


=begin
  @ Description :  Get all the categories available in restaurants
  @input : useraccesstoken,restaurantid
  @output : display list of categories, if available
=end

  def getcategories
    if (params[:useraccesstoken].blank?) || (params[:restaurantid].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      restaurantName=Restaurantdetail.find(params[:restaurantid])
      allcategories=Category.find(:all, :conditions => ["restaurant_id =?", params[:restaurantid]])
      if allcategories.present? then
        allcategories.each do |val|
          val[:category_name]=to_utf_8(val[:category_name])
          if (val.avatar).present? then
            val['image']=val.avatar.url
          else
            val['image']=root_url[0..-1]+"default.jpg"
          end
        end
        render :json => JSON.pretty_generate({'data' => {"allcategories" => (allcategories.to_a.map(&:serializable_hash)), "restaurant_name" => restaurantName['restaurant_name']}})
      else
        render :json => JSON.pretty_generate({'error' => 'No categories available.'})
      end

    end
  end

#end  getrestaurantlist

=begin
  @ Description :  Get all the food items available in a category
  @input : useraccesstoken,categoryid
  @output : display list of all food items, if available
=end
  def getfooditemsincategory
    if (params[:useraccesstoken].blank?) || (params[:categoryid].blank?) then
      return invalid_params_msg
    else
      catgoryId=params[:categoryid];
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      allFoodDetails=Fooditem.select(" id,food_name,avatar_file_name,avatar_content_type,avatar_file_size,avatar_updated_at").where("category_id=?", catgoryId)
      if allFoodDetails.present? then
        allFoodDetails.each do |val|
          val[:food_name]=to_utf_8(val[:food_name])
          if (val.avatar).present? then
            val['image']=val.avatar.url
          else
            val['image']=root_url[0..-1]+"default.jpg"
          end
        end
        render :json => JSON.pretty_generate({'data' => (allFoodDetails.to_a.map(&:serializable_hash))})
      else
        render :json => JSON.pretty_generate({"error" => "No food items are available in this category."})
      end
    end
  end

#end  getfooditemsincategory


=begin
  @ Description :  Get details of food item in a category
  @input : useraccesstoken,fooditemid
  @output : display all details of food item with opening and closing timings of restaurant kitchen
=end
  def getfooditemsdetails
    if (params[:useraccesstoken].blank?) || (params[:fooditemid].blank?) || (params[:timezone].blank?) then
      return invalid_params_msg
    else
      foodItemId=params[:fooditemid]
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      allFoodDetails=Fooditem.find(:all, :conditions => ["id=?", foodItemId])
      if allFoodDetails.present? then
        if (allFoodDetails[0].avatar).present? then
          allFoodDetails[0]['image']=allFoodDetails[0].avatar.url
        else
          allFoodDetails[0]['image']=root_url[0..-1]+"default.jpg"
        end
        allFoodDetails[0]['food_name']=to_utf_8(allFoodDetails[0]['food_name'])
        allFoodDetails[0]['description']=to_utf_8(allFoodDetails[0]['description'])
        allSizesAndPrices=Fooditemspricesandsize.where('food_id=?', allFoodDetails[0]['id']);
        if allSizesAndPrices.present? then
          allFoodDetails=allFoodDetails.to_a.map(&:serializable_hash)
          allFoodDetails[0][:allsizes]=Hash.new
          allSizesAndPricesInfo=allSizesAndPrices.to_a.map(&:serializable_hash)
          today=((Time.now.in_time_zone(params[:timezone])).wday)
          nextday = today+1
          nextDayCheck = nextday==7 ? 0 : nextday

          restaurantDetails=Restauranttiming.select('id,day,opening_time,closing_time').where("(restaurant_id=? && day=?) || (restaurant_id=? && day=?) ", allFoodDetails[0]['restaurant_id'], today, allFoodDetails[0]['restaurant_id'], nextDayCheck)
          if restaurantDetails.present?
            if restaurantDetails[0]['day']== today then
              restaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
              restaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
            else
              restaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time'])
              restaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time'])
            end

            currentTime= (Time.now.in_time_zone(params[:timezone]))
            ishappyhouravailable=Restaurantdetail.where('id=?', allFoodDetails[0]['restaurant_id']).limit(1)
            if ishappyhouravailable[0]['is_happy_hour_available']== 1 then
              happyhourstimings=Restauranthappyhour.where('restaurant_id=?', allFoodDetails[0]['restaurant_id'])
              isHappyHoursStarted=0;
              happyhourstimings.each do |val|
                localStartTime=convertutctolocal(params[:timezone],val['start_time'])
                localEndTime=convertutctolocal(params[:timezone],val['end_time'])
                if (localEndTime.strftime("%Y-%m-%d")) == (localStartTime.strftime("%Y-%m-%d")) then
                  finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
                  finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
                elsif (localEndTime.strftime("%Y-%m-%d")) > (localStartTime.strftime("%Y-%m-%d")) then
                  nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
                  finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
                  finalEndTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
                else
                  finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
                  finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
                end
                if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalStartTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalEndTime) then
                  isHappyHoursStarted=1
                  break
                else
                  isHappyHoursStarted=0
                end
              end
            else
              isHappyHoursStarted=0
            end


            if (restaurantClosingTime.strftime("%Y-%m-%d")) == (restaurantOpeningTime.strftime("%Y-%m-%d")) then
              finalRestaurantClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
              finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
            elsif (restaurantClosingTime.strftime("%Y-%m-%d")) > (restaurantOpeningTime.strftime("%Y-%m-%d")) then
              nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
              finalRestaurantClosingTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
              finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
            else
              finalRestaurantClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
              finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
            end


            time_diff_components = Time.diff(finalRestaurantClosingTime, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))
            if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalRestaurantOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalRestaurantClosingTime)) then
              if time_diff_components[:day]== 0 && time_diff_components[:hour]== 0 && time_diff_components[:minute]<=15 then
                flag=0
                next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
                next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
              else
                flag=1
                next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
                next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
              end
            elsif ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) < finalRestaurantOpeningTime) then
              flag=0;
              next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time'])
              next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time'])
            else
              flag=0
              next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
              next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
            end



            allFoodDetails[0][:allsizes]=allSizesAndPricesInfo
            render :json => JSON.pretty_generate({'data' => allFoodDetails, 'flag' => flag, 'isHappyHoursStarted' => isHappyHoursStarted, 'next_day_opening_time' => (next_day_opening_time.strftime("%H:%M:%S")), 'next_day_closing_time' => (next_day_closing_time.strftime("%H:%M:%S"))})
          else
            render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
          end
        else
          render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
        end

      else
        render :json => JSON.pretty_generate({"error" => "No details available."})
      end

    end
  end

#end  getfooditemsdetails  


=begin
  @ Description :  Get list of extras available with a food item of a category
  @input : useraccesstoken,fooditemid
  @output : display list of all extras availabale with food item
=end

  def getextraslist
    if (params[:useraccesstoken].blank?) || (params[:fooditemid].blank?) then
      return invalid_params_msg
    else
      foodItemId=params[:fooditemid]
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      allExtraDetails=Extra.select(" id, extra_name, avatar_file_name,avatar_content_type,avatar_file_size,avatar_updated_at").where("food_id =?", foodItemId)
      if allExtraDetails.present? then
        allExtraDetails.each do |val|
          val[:extra_name]=to_utf_8(val[:extra_name])
          if (val.avatar).present? then
            val['image']=val.avatar.url
          else
            val['image']=root_url[0..-1]+"default.jpg"
          end
        end
        render :json => JSON.pretty_generate({'data' => (allExtraDetails.to_a.map(&:serializable_hash))})
      else
        render :json => JSON.pretty_generate({"error" => "No extras available."})
      end

    end
  end

#end  getextraslist


=begin
  @ Description :  Get details of extras with food item
  @input : useraccesstoken,extrasid
  @output : display all details of extras with opening and closing timings of restaurant kitchen
=end
  def getextrasdetails
    if (params[:useraccesstoken].blank?) || (params[:extrasid].blank?) || (params[:timezone].blank?) then
      return invalid_params_msg
    else
      extrasId=params[:extrasid]
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      allExtraDetails=Extra.find(:all, :conditions => ["id=?", extrasId])
      if allExtraDetails.present? then
        if (allExtraDetails[0].avatar).present? then
          allExtraDetails[0]['image']=allExtraDetails[0].avatar.url
        else
          allExtraDetails[0]['image']=root_url[0..-1]+"default.jpg"
        end
        allExtraDetails[0]['extra_name']=to_utf_8(allExtraDetails[0]['extra_name'])
        allExtraDetails[0]['description']=to_utf_8(allExtraDetails[0]['description'])
        allExtrasInfo=allExtraDetails.to_a.map(&:serializable_hash)
        today=((Time.now.in_time_zone(params[:timezone])).wday)
        nextday = today+1
        nextDayCheck = nextday==7 ? 0 : nextday
        restaurantDetails=Restauranttiming.select('id,day,opening_time,closing_time').where("(restaurant_id=? && day=?) || (restaurant_id=? && day=?) ", allFoodDetails[0]['restaurant_id'], today, allFoodDetails[0]['restaurant_id'], nextDayCheck)
        if restaurantDetails.present?
          if restaurantDetails[0]['day']== today then
            restaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
            restaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
          else
            restaurantOpeningTime = convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time'])
            restaurantClosingTime = convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time'])
          end
          currentTime= (Time.now.in_time_zone(params[:timezone]))
          ishappyhouravailable=Restaurantdetail.where('id=?', allFoodDetails[0]['restaurant_id']).limit(1)
          if ishappyhouravailable[0]['is_happy_hour_available']== 1 then
            happyhourstimings=Restauranthappyhour.where('restaurant_id=?', allFoodDetails[0]['restaurant_id'])
            isHappyHoursStarted=0;
            happyhourstimings.each do |val|
              localStartTime=convertutctolocal(params[:timezone],val['start_time'])
              localEndTime=convertutctolocal(params[:timezone],val['end_time'])
              if (localEndTime.strftime("%Y-%m-%d")) == (localStartTime.strftime("%Y-%m-%d")) then
                finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
                finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
              elsif (localEndTime.strftime("%Y-%m-%d")) > (localStartTime.strftime("%Y-%m-%d")) then
                nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
                finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
                finalEndTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
              else
                finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
                finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
              end
              if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalStartTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalEndTime) then
                isHappyHoursStarted=1
                break
              else
                isHappyHoursStarted=0
              end
            end
          else
            isHappyHoursStarted=0
          end

          if (restaurantClosingTime.strftime("%Y-%m-%d")) == (restaurantOpeningTime.strftime("%Y-%m-%d")) then
            finalRestaurantClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
            finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
          elsif (restaurantClosingTime.strftime("%Y-%m-%d")) > (restaurantOpeningTime.strftime("%Y-%m-%d")) then
            nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
            finalRestaurantClosingTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
            finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
          else
            finalRestaurantClosingTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantClosingTime.strftime("%H:%M:%S"))
            finalRestaurantOpeningTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (restaurantOpeningTime.strftime("%H:%M:%S"))
          end

          time_diff_components = Time.diff(finalRestaurantClosingTime, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))
          if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalRestaurantOpeningTime) && ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= (finalRestaurantClosingTime)) then
            if time_diff_components[:day]== 0 &&time_diff_components[:hour]== 0 && time_diff_components[:minute]<=15 then
              flag=0
              next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
              next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
            else
              flag=1
              next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
              next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
            end
          elsif ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) < finalRestaurantOpeningTime) then
            flag=0;
            next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time'])
            next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time'])
          else
            flag=0
            next_day_opening_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['opening_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['opening_time'])
            next_day_closing_time = (restaurantDetails[0]['day']== today) ? convertutctolocal(params[:timezone],restaurantDetails[1]['closing_time']) : convertutctolocal(params[:timezone],restaurantDetails[0]['closing_time'])
          end

          render :json => JSON.pretty_generate({"data" => allExtrasInfo, 'flag' => flag, 'isHappyHoursStarted' => isHappyHoursStarted, 'next_day_opening_time' => (next_day_opening_time.strftime("%H:%M:%S")), 'next_day_closing_time' => (next_day_closing_time.strftime("%H:%M:%S"))})
        else
          render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
        end

      else
        render :json => JSON.pretty_generate({"error" => "No details available."})
      end

    end
  end

#end  getextrasdetails

  def convertutctolocal(timezone, date)
    offset= Time.now.in_time_zone(timezone).utc_offset
    totaltime = Time.now.in_time_zone(timezone)
    utctime=''
    if (totaltime.to_s).include? "+"
      if (offset.to_s).include? "+" then
        finaloffset=(offset.to_s).split("+")
        utctime= date + (finaloffset[1].to_i).to_i.seconds
      else
        utctime= date + (offset.to_i).to_i.seconds
      end
    else
      if (offset.to_s).include? "-" then
        finaloffset=(offset.to_s).split("-")
        utctime= date - (finaloffset[1].to_i).to_i.seconds
      else
        utctime= date - (offset.to_i).to_i.seconds
      end
    end
    return utctime
  end

  private

  #error in case invalid parameters passed.
  def invalid_params_msg
    render :json => JSON.pretty_generate({"error" => "some parameter missing"})
  end

  #error in case invalid access token(unauthorized access).
  def unauthorized_user
    render :json => JSON.pretty_generate({"error" => "Unauthorized access."})
  end

  def to_utf_8(value)
    finalValue= Iconv.iconv('UTF-8', 'ISO-8859-1', value).join
    return finalValue;
  end
end
