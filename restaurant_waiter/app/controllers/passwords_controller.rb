class PasswordsController < Devise::PasswordsController
def new
end

def update
  @user = User.find_by_reset_password_token(params[:token])
  return unauthorized_user unless @user
  #if @user.reset_password_sent_at < 2.hours.ago
       isupdated=@user.update_attribute(:password,params[:password])
       if isupdated then
           #render :json => JSON.pretty_generate({"log" => "Your password has been updated sucessfully."})
         flash[:resetpassword]=1
       else
         flash[:resetpassworderror]="Unable to change password. Please try again later"
         # render :json => JSON.pretty_generate({"error" => "Unable to change password. Please try again later"})
       end
  #else
  #   render :json => JSON.pretty_generate({"error" => "Password reset has expired."})
 # end
  redirect_to :back and return
end

  private
  def invalid_params_msg
    flash[:resetpassworderror]="some parameter missing"
    redirect_to :back and return
    #render :json => JSON.pretty_generate({"error" => "some parameter missing"})
  end

  def unauthorized_user
    flash[:resetpassworderror]="Unauthorized access."
    redirect_to :back and return
    #render :json => JSON.pretty_generate({"error" => "Unauthorized access."})
  end
end
