class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead

  before_filter :set_cache_buster
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to new_admin_registration_path, :notice => exception.message
  end

  def current_ability
    @current_ability ||= Ability.new(current_super_admin) rescue nil
  end

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
  
  protect_from_forgery with: :null_session

  before_action :configure_permitted_parameters, if: :devise_controller?

  
  rescue_from ActionController::RoutingError, :with => :render_404
  private
  def render_404(exception = nil)
    if exception
        logger.info "Rendering 404: #{exception.message}"
    end

    render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
  end
  
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [:username,:fb_id, :fb_access_token,:device_type,:device_token]
  end
  
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:admin]
  end
  
  def after_sign_in_path_for_super_admin(resource)
    new_admin_registration_path
  end

  def after_sign_out_path_for_super_admin(resource)
    new_super_admin_session_path
  end

  private
  def authenticate_super_admin!
    if current_super_admin.nil?
      flash[:error] = "Unauthorized access!"
      redirect_to new_super_admin_session_path
    end
  end

  def get_super_admin_notifications
    @admin_notification = Admin.where(:is_paid => 1 , :confirm_status=> 0)
  end
  
  
end
