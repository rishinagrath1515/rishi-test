=begin
  @ Description :This Contains all the methods related to the restaurant.
  @ Copyright :
  @ Developed by : Click Labs Pvt. Ltd
=end
require "ethor_data.rb"
class AllrestaurantsController < ApplicationController

#to test methods of restaurant class(shift control to test file)
  def restaurantdetails
  end


=begin
  @ Description :  Get list of all restaurants on the basis of user location(within 5 miles)
  @input : useraccesstoken,latitude,longitude
  @output : display all restaurant within 5 miles of the user location
=end

  def getrestaurantlist
    if (params[:useraccesstoken].blank?) || (params[:latitude].blank?) || (params[:longitude].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      lat=params[:latitude]
      lng=params[:longitude]
      alldetails= Restaurantdetail.within(5, :origin => [lat, lng])
      if alldetails.present?
        alldetails=alldetails.to_a.map(&:serializable_hash)
        alldetails.each do |val|
          distance = Restaurantdetail.distance_between [lat, lng], [val['latitude'], val['longitude']], :units => :miles, :formula => :sphere
          val[:distance]=distance.round(2);
        end

        sortedDetails = alldetails.sort_by do |value|
          value[:distance]
        end
        render :json => JSON.pretty_generate({'data' => sortedDetails})
      else
        alldetails= Restaurantdetail.find(:all)
        if alldetails.present? then
          alldetails=alldetails.to_a.map(&:serializable_hash)
          alldetails.each do |val|
            distance = Restaurantdetail.distance_between [lat, lng], [val['latitude'], val['longitude']], :units => :miles, :formula => :sphere
            val[:distance]=distance.round(2);
          end

          sortedDetails = alldetails.sort_by do |value|
            value[:distance]
          end
          render :json => JSON.pretty_generate({'data' => sortedDetails})
        else
          render :json => JSON.pretty_generate({"error" => "Unable to locate any restaurant."})
        end
      end
    end
  end

#end  getrestaurantlist


=begin
    @ Description :  Get details of a restaurant
    @input : useraccesstoken,restaurant_id
    @output : display all details of the restaurant(general+timing)
=end
  def getrestaurantdetail
    if (params[:useraccesstoken].blank?) || (params[:restaurant_id].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      allDetails=Restaurantdetail.where("id=?", params[:restaurant_id])
      if allDetails.present? then
        allDetails=allDetails.to_a.map(&:serializable_hash)
        if allDetails[0]['image'].present? then
          imageName=allDetails[0]['image']
          allDetails[0].delete('image');
          allDetails[0][:image_url]=root_url[0..-1]+"restaurants/"+imageName
        else
          allDetails.delete('image');
          allDetails[0][:image_url]=root_url[0..-1]+"restaurants/default.jpg"
        end
        timings= Restauranttiming.where('restaurant_id=?', params[:restaurant_id]) #.group('opening_time,closing_time')
        if timings.present? then
=begin
           #for kitchen timings
          allTimingInfo=timings.to_a.map(&:serializable_hash);
          temp=Array.new();
          temp<<allTimingInfo[0];
          (1..((allTimingInfo.count)-1)).each do |i|
            isValInserted=0;
            (0..((temp.count)-1)).each do |j|
              if ((allTimingInfo[i]['opening_time']==temp[j]['opening_time']) && (allTimingInfo[i]['closing_time']==temp[j]['closing_time'])) then
                temp[j]['day']=(temp[j]['day'].to_s)<<','<< (allTimingInfo[i]['day'].to_s)
                temp[j]['day_name']=(temp[j]['day_name'].to_s)<<','<< (allTimingInfo[i]['day_name'])
                
                isValInserted=1;
              end
            end
            if isValInserted==0 then
              temp<<allTimingInfo[i];
            end
          end
          allDetails[0][:restaurant_timings]=Hash.new
          allDetails[0][:restaurant_timings]=temp
          
          #for bar timings
          bartimings= Restauranttiming.select('id,day,day_name,bar_opening_time,bar_closing_time,created_at,updated_at').where('restaurant_id=?', params[:restaurant_id])
          allBarTimingInfo=bartimings.to_a.map(&:serializable_hash);
          temp=Array.new();
          temp<<allBarTimingInfo[0];
         (1..((allBarTimingInfo.count)-1)).each do |i|
            isValInserted=0;
            (0..((temp.count)-1)).each do |j|
              if ((allBarTimingInfo[i]['bar_opening_time']==temp[j]['bar_opening_time']) && (allBarTimingInfo[i]['bar_closing_time']==temp[j]['bar_closing_time'])) then
                temp[j]['day']=(temp[j]['day'].to_s)<<','<< (allBarTimingInfo[i]['day'].to_s)
                temp[j]['day_name']=(temp[j]['day_name'].to_s)<<','<< (allBarTimingInfo[i]['day_name'])
                isValInserted=1;
              end
            end
            if isValInserted==0 then
              temp<<allBarTimingInfo[i];
            end
          end
          allDetails[0][:bar_timings]=Hash.new
          allDetails[0][:bar_timings]=temp
          
          #for Happy Hours timings
          happyHourtimings= Restauranttiming.select('id,day,day_name,is_happy_hours_available,happy_hour_start_time,happy_hour_end_time,created_at,updated_at').where('restaurant_id=?', params[:restaurant_id])
          allHappyHourTimingInfo=happyHourtimings.to_a.map(&:serializable_hash);
          temp=Array.new();
          temp<<allHappyHourTimingInfo[0];
         (1..((allHappyHourTimingInfo.count)-1)).each do |i|
            isValInserted=0;
            (0..((temp.count)-1)).each do |j|
              if ((allHappyHourTimingInfo[i]['happy_hour_start_time']==temp[j]['happy_hour_start_time']) && (allHappyHourTimingInfo[i]['happy_hour_end_time']==temp[j]['happy_hour_end_time'])) then
                temp[j]['day']=(temp[j]['day'].to_s)<<','<< (allHappyHourTimingInfo[i]['day'].to_s)
                temp[j]['day_name']=(temp[j]['day_name'].to_s)<<','<< (allHappyHourTimingInfo[i]['day_name'])
                isValInserted=1;
              end
            end
            if isValInserted==0 then
              temp<<allHappyHourTimingInfo[i];
            end
          end
          allDetails[0][:happy_hour_timings]=Hash.new
          allDetails[0][:happy_hour_timings]=temp
=end
          allTimings=timings.to_a.map(&:serializable_hash);
          allTimings.each do |val|
            val.delete('is_happy_hours_available')
            val.delete('happy_hour_start_time')
            val.delete('happy_hour_end_time')
          end
          allDetails[0][:restaurant_timings]=allTimings
          happyhourstimings=Restauranthappyhour.where('restaurant_id=?',params[:restaurant_id])
          allDetails[0][:happy_hours_timings]=happyhourstimings.to_a.map(&:serializable_hash);
          render :json => JSON.pretty_generate({"data" => allDetails})
        else
          render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
        end
      else
        render :json => JSON.pretty_generate({"error" => "No restaurant details available."})
      end
    end
  end

#end  getrestaurantdetail


=begin
    @ Description :  Scan qr code on the restaurant table
    @input : useraccesstoken,qrcode,orderid
    @output : update table number for order of the user
=end

  def getqrcode
    if (params[:useraccesstoken].blank?) || (params[:qrcode].blank?) || (params[:orderid].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      restaurantTableDetails=Restauranttable.find_by_qrcode(params[:qrcode])
      if restaurantTableDetails.present?
        order= Order.includes([:orderdetails,:fooditems, :orderdetailswithextras, :orderdetailswithdrinkses,:drinks]).where("id = ? && restaurant_id =?", params[:orderid], restaurantTableDetails['restaurant_id']).first

        isUpdated=Order.where("id=?",params[:orderid]).update_all(:table_id => restaurantTableDetails['id'])
        if isUpdated then
          success =true
          #success = send_order_to_pos(order)
          if success
            render :json => JSON.pretty_generate({"log" => "1"})
          else
            render :json => JSON.pretty_generate({"log" => "0"})
          end
        else
          render :json => JSON.pretty_generate({"log" => "0"})
        end
      else
        render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
      end
    end
  end

#end  getqrcode


=begin
    @ Description :  Get restaurant details from gimbal device(gimbal_id)
    @input : useraccesstoken,gimbalid
    @output : display all restaurant details on the basis of gimbal                               
=end
 def getrestaurantdetailfromgimbal
    if (params[:useraccesstoken].blank?) || (params[:gimbalid].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      restaurantId=Gimbaldetail.select('restaurant_id').where('gimbal_id=?', params[:gimbalid]).limit(1)
      if restaurantId.present? then
        gimbalInfo=Gimbaldetail.select('id,gimbal_id').where('restaurant_id=?', restaurantId[0]['restaurant_id'])
        allDetails=Restaurantdetail.where("id=?", restaurantId[0]['restaurant_id']).limit(1)
        render :json => JSON.pretty_generate({"data" => {'restaurantid'=>restaurantId[0]['restaurant_id'],'restaurantname'=>allDetails[0]['restaurant_name'],'alldevices'=>gimbalInfo.to_a.map(&:serializable_hash)}})
=begin
        allDetails=Restaurantdetail.where("id=?", restaurantId[0]['restaurant_id'])
        if allDetails.present? then
          allDetails=allDetails.to_a.map(&:serializable_hash)
          if allDetails[0]['image'].present? then
            imageName=allDetails[0]['image']
            allDetails[0].delete('image');
            allDetails[0][:image_url]=root_url[0..-1]+"restaurants/"+imageName
          else
            allDetails.delete('image');
            allDetails[0][:image_url]=root_url[0..-1]+"restaurants/default.jpg"
          end
          timimgs= Restauranttiming.where('restaurant_id=?', restaurantId[0]['restaurant_id'])
          if timimgs.present? then
            allTimingInfo=timimgs.to_a.map(&:serializable_hash);
            temp=Array.new();
            temp<<allTimingInfo[0];
            (1..((allTimingInfo.count)-1)).each do |i|
              isValInserted=0;
              (0..((temp.count)-1)).each do |j|
                if ((allTimingInfo[i]['opening_time']==temp[j]['opening_time']) && (allTimingInfo[i]['closing_time']==temp[j]['closing_time']) && (allTimingInfo[i]['happy_hour_start_time']==temp[j]['happy_hour_start_time']) && (allTimingInfo[i]['happy_hour_end_time']==temp[j]['happy_hour_end_time']) && (allTimingInfo[i]['bar_opening_time']==temp[j]['bar_opening_time']) && (allTimingInfo[i]['bar_closing_time']==temp[j]['bar_closing_time'])) then
                  temp[j]['day']=(temp[j]['day'].to_s)<<','<< (allTimingInfo[i]['day'].to_s)
                  temp[j]['day_name']=(temp[j]['day_name'].to_s)<<','<< (allTimingInfo[i]['day_name'].to_s)
                  isValInserted=1;
                end
              end
              if isValInserted==0 then
                temp<<allTimingInfo[i];
              end
            end
            allDetails[0][:restaurant_timings]=Hash.new
            allDetails[0][:restaurant_timings]=temp
            render :json => JSON.pretty_generate({"data" => allDetails})
          else
            render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
          end
        else
          render :json => JSON.pretty_generate({"error" => "No restaurant details available."})
        end
=end
      else
        render :json => JSON.pretty_generate({"error" => "Unable to detect smart beacon."})
      end
    end
  end
#end  getrestaurantdetailfromgimbal


=begin
    @ Description :  Check whether happy hour started ,happy hour closed and 15 minutes left in Happy hours closing
    @input : useraccesstoken,restaurant_id
    @output : Send flag for each time                              
=end
  def sendnotifications
    if (params[:useraccesstoken].blank?) || (params[:restaurant_id].blank?) || (params[:timezone].blank?)  then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      today=((Time.now.in_time_zone(params[:timezone])).wday)
      restaurantDetails=Restauranttiming.select('id,is_happy_hours_available,happy_hour_start_time,happy_hour_end_time').where("restaurant_id=? && day=?",params[:restaurant_id], today) 
      if restaurantDetails.present?
        currentTime= (Time.now.in_time_zone(params[:timezone]))
        ishappyhouravailable=Restaurantdetail.where('id=?', params[:restaurant_id]).limit(1)
        if ishappyhouravailable[0]['is_happy_hour_available']== 1 then
          happyhourstimings=Restauranthappyhour.where('restaurant_id=?',  params[:restaurant_id])
          isHappyHoursStarted=0;
          happyHourEndTime='';
          happyHourEndTime1=''
          happyHourStartTime='';
          happyhourstimings.each do |val|
             localStartTime=convertutctolocal(params[:timezone],val['start_time'])
             localEndTime=convertutctolocal(params[:timezone],val['end_time'])
            if (localStartTime.strftime("%Y-%m-%d")) == (localEndTime.strftime("%Y-%m-%d")) then
              finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
              finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
            elsif (localEndTime.strftime("%Y-%m-%d")) > (localStartTime.strftime("%Y-%m-%d")) then
              nextDay= Time.now.in_time_zone(params[:timezone]) + 1.day
              finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
              finalEndTime=(nextDay.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
            else
              finalStartTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localStartTime.strftime("%H:%M:%S"))
              finalEndTime=(currentTime.strftime("%Y-%m-%d"))+" "+ (localEndTime.strftime("%H:%M:%S"))
            end

            if ((currentTime.strftime("%Y-%m-%d %H:%M:%S")) >= finalStartTime && (currentTime.strftime("%Y-%m-%d %H:%M:%S")) <= finalEndTime) then
              isHappyHoursStarted=1
              happyHourEndTime=(localEndTime.strftime("%H:%M:%S"));
              happyHourEndTime1=finalEndTime
              happyHourStartTime=(localStartTime.strftime("%H:%M:%S"));
              break
            else
              isHappyHoursStarted=0
            end
          end
        else
          isHappyHoursStarted=0
        end
         
        if isHappyHoursStarted == 1 then
              time_diff_components_for_restaurant = Time.diff(happyHourEndTime1, (currentTime.strftime("%Y-%m-%d %H:%M:%S")))
              if time_diff_components_for_restaurant[:day]== 0 && time_diff_components_for_restaurant[:hour]== 0 && time_diff_components_for_restaurant[:minute]<=15 then
                   minsleft=1
                   isHappyHoursClosed=0
                   timeleftinhours=time_diff_components_for_restaurant[:hour]
                   timeleftinmins=time_diff_components_for_restaurant[:minute]
              else
                   minsleft=0
                   isHappyHoursClosed=0
                   timeleftinhours=time_diff_components_for_restaurant[:hour]
                   timeleftinmins=time_diff_components_for_restaurant[:minute]
              end
        else
             isHappyHoursClosed=1
             minsleft=0
             timeleftinhours=0
             timeleftinmins=0
        end
         render :json => JSON.pretty_generate({"data" => {'isHappyHoursStarted' => isHappyHoursStarted, 'minsleft' => minsleft, 'isHappyHoursClosed' =>isHappyHoursClosed,'happy_hour_start_time'=>happyHourStartTime,'happy_hour_end_time'=>happyHourEndTime,"timeleftinhours"=>timeleftinhours,"timeleftinmins" =>timeleftinmins}})
        else
         render :json => JSON.pretty_generate({"error" => "An error occurred. Please try again later."})
        end
    end
  end
#end  sendnotifications

  def convertutctolocal(timezone, date)
    offset= Time.now.in_time_zone(timezone).utc_offset
    totaltime = Time.now.in_time_zone(timezone)
    utctime=''
    if (totaltime.to_s).include? "+"
      if (offset.to_s).include? "+" then
        finaloffset=(offset.to_s).split("+")
        utctime= date + (finaloffset[1].to_i).to_i.seconds
      else
        utctime= date + (offset.to_i).to_i.seconds
      end
    else
      if (offset.to_s).include? "-" then
        finaloffset=(offset.to_s).split("-")
        utctime= date - (finaloffset[1].to_i).to_i.seconds
      else
        utctime= date - (offset.to_i).to_i.seconds
      end
    end
    return utctime
  end

#for testing current time
def getcurrenttime
   time=Time.now.in_time_zone("Asia/Calcutta")
  render :json => {'currenttime'=>time.strftime("%H:%M:%S")}
end

  def send_order_to_pos(order)
    user = order.user
    restaurant  = order.restaurant
    Rails.logger.info "restaurant  #{restaurant.rid}"
    ticket_id = EthorData::Ticket.info(restaurant)
    customer_info = EthorData::Customer.info
    order_info = EthorData::Order.info(order)
    Ethor::Ticket.add_items(order_info,'store' => restaurant.rid, 'ticket_id' => ticket_id)
    payment_info = EthorData::Payment.info(order)
    payment_added = Ethor::Ticket.add_payments(payment_info,'store' => restaurant.rid, 'ticket_id' => ticket_id)
    resp = Ethor::Ticket.submit_order(customer_info,'store' => restaurant.rid , 'ticket_id' => ticket_id)
    unless resp.has_key? "error"
      order_details = order.orderdetails.update_all(:submitted => true)
      order_with_extras = order.orderdetailswithextras.update_all(:submitted => true)
      order_with_drinks = order.orderdetailswithdrinkses.update_all(:submitted => true)
      Rails.logger.info " ====================================== #{resp} ==============================================="
      Rails.logger.info " ====================================== Order sent to POS ==============================================="
      return true
    else
      Rails.logger.info " ====================================== #{resp['error']} ================================================"
      return false
    end
  end
=begin
    @ Description :  methods to display errors
=end

  private


  #error in case invalid parameters passed.
  def invalid_params_msg
    render :json => JSON.pretty_generate({"error" => "some parameter missing"})
  end

#error in case invalid access token(unauthorized access).
  def unauthorized_user
    render :json => JSON.pretty_generate({"error" => "Unauthorized access."})
  end
end
