=begin
  @ Description :This Contains all the methods related to the User Resgistration .
   This is to override the Devise Registration Controller.
  @ Copyright :
  @ Developed by : Click Labs Pvt. Ltd
=end


class RegistrationsController < Devise::RegistrationsController

  def new
    super
  end


=begin
  @ Description :  Register Using Email
  @input : email,password,username,device_type and device_token
  @output : send usesaccesstoken in case of successful registration else error
=end
  def create
    user = User.new(person_params)
     User.skip_callbacks = false
    if user.save
      userId=User.select("id,user_access_token,username,app_version").where("email=?", user['email']).limit(1);
      appVersion=Appverion.last
      currentVersion= user.device_type == 1 ? appVersion['android_app_version'] : appVersion['ios_app_version']
      forceUpdate = user.device_type == 1 ? appVersion['android_force_update'] : appVersion['ios_force_update']
      if userId.first['app_version'] != user.app_version then
         User.where('id=?',userId.first['id']).update_all(:app_version=>user.app_version)
         if user.app_version <= currentVersion
            showPopup=1
         else
            showPopup=0
         end
      else
        if userId.first['app_version'] < currentVersion then
          showPopup=1
        else
          showPopup=0
        end
      end
      render :json => JSON.pretty_generate({"log" => "Registered successfully.", "useraccesstoken" => userId.first['user_access_token'], "username"=>userId.first['username'],"popup"=>showPopup,"forceupdate"=>forceUpdate})

#render :json => JSON.pretty_generate({"log" => "Registered successfully.", "useraccesstoken" => userId.first['user_access_token'], "username"=>userId.first['username']})
    else
      render :json => JSON.pretty_generate({"error" => (user.errors.first).to_sentence})
    end
  end

#end  create

  private
#error in case to register user(strong_params).
  def person_params
    #params.permit(:email, :username, :password, :fb_id, :fb_access_token, :device_type, :device_token, :app_version)
    params.permit(:email, :username, :password, :fb_id, :fb_access_token, :device_type, :device_token,:app_version)
  end


end
