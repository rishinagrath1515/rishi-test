class SuperAdmin::ReportController < ApplicationController
  before_filter :authenticate_super_admin!
  before_filter :get_super_admin_notifications
  before_filter :set_instance_variable, :only => [:report, :restaurant_report]
  layout 'admin_dashboard'

  def report
    @user_class = 'active'
    evaluate_admins_graph(nil,nil,nil,Date.today.beginning_of_month.to_time, DateTime.now)
    render :template => 'super_admin/report'
  end

  def restaurant_report
    @restaurant_class = 'active'
    evaluate_restaurant_graph(nil, nil, nil, nil,Date.today.beginning_of_month.to_time, DateTime.now)
    render :template => 'super_admin/restaurant_report'
  end

  def subregion_options
    country = Carmen::Country.coded(params[:parent_region])
    states =  Restaurantdetail.where(:country => params[:parent_region]).select('state')
    @states = states.collect{|s| {s.state => (country.subregions.coded(s.state).nil? ? s.state : country.subregions.coded(s.state).name)}}.uniq
    render :partial => 'super_admin/report/subregion_select'
  end

  def get_city
    @cities = Restaurantdetail.where(:state => params[:state], :country => params[:country]).select("city").uniq
    render :partial => 'super_admin/report/city_detail'
  end

  def get_restaurant
    @restaurants = Restaurantdetail.where(:state => params[:state], :country => params[:country], :city => params[:city])
    render :partial => 'super_admin/report/restaurant_detail'
  end

  def get_graphs
    country = params[:country]
    state = params[:state]
    city = params[:city]
    time = params[:time]
    case time
      when "3"
        last_year_start = DateTime.parse((Date.today.beginning_of_year).to_s).to_time
        last_year_end = DateTime.now
        evaluate_admins_graph(country, state, city, last_year_start, last_year_end)
      when "4"
        evaluate_all_time_admins_graph(country, state, city)
      when "5"
        last_quarter_start = DateTime.parse((Date.today.beginning_of_month - 2.month).to_s).to_time
        last_quarter_end = DateTime.now
        evaluate_admins_graph(country, state, city, last_quarter_start, last_quarter_end)
      when "7"
        evaluate_admins_graph(country, state, city,Date.today.beginning_of_month.to_time, DateTime.now)
      when "8"
        last_month_start_date = DateTime.parse((Date.today.beginning_of_month - 1.month).to_s).to_time
        last_month_end_date = DateTime.parse(((Date.today.beginning_of_month - 1.month).end_of_month).to_s).to_time
        evaluate_admins_graph(country, state, city, last_month_start_date, last_month_end_date)
      else
        start_date= (DateTime.parse(params[:time].scan(/(\d{4}\-\d{2}\-\d{2})/).flatten[0])).to_time rescue Date.today.beginning_of_month.to_time
        end_date= (DateTime.parse(params[:time].scan(/(\d{4}\-\d{2}\-\d{2})/).flatten[1]) + 1.day).to_time rescue DateTime.now
        evaluate_admins_graph(country, state, city, start_date, end_date)
    end
    render :partial => 'super_admin/report/admin' and return
  end

  def get_restaurants_graphs
    country = params[:country]
    state = params[:state]
    city = params[:city]
    restaurant = Restaurantdetail.find_by_id(params[:restaurant])
    time = params[:time]
    case time
      when "3"
        last_year_start = DateTime.parse((Date.today.beginning_of_year).to_s).to_time
        last_year_end = DateTime.now
        evaluate_restaurant_graph(country, state, city,restaurant, last_year_start, last_year_end)
      when "4"
        evaluate_all_time_restaurant_graph(country, state, city,restaurant)
      when "5"
        last_quarter_start = DateTime.parse((Date.today.beginning_of_month - 2.month).to_s).to_time
        last_quarter_end = DateTime.now
        evaluate_restaurant_graph(country, state, city,restaurant, last_quarter_start, last_quarter_end)
      when "7"
        evaluate_restaurant_graph(country, state, city,restaurant, Date.today.beginning_of_month.to_time, DateTime.now)
      when "8"
        last_month_start_date = DateTime.parse((Date.today.beginning_of_month - 1.month).to_s).to_time
        last_month_end_date = DateTime.parse(((Date.today.beginning_of_month - 1.month).end_of_month).to_s).to_time
        evaluate_restaurant_graph(country, state, city,restaurant, last_month_start_date, last_month_end_date)
      else
        start_date= (DateTime.parse(params[:time].scan(/(\d{4}\-\d{2}\-\d{2})/).flatten[0])).to_time rescue Date.today.beginning_of_month.to_time
        end_date= (DateTime.parse(params[:time].scan(/(\d{4}\-\d{2}\-\d{2})/).flatten[1]) + 1.day).to_time rescue DateTime.now
        evaluate_restaurant_graph(country, state, city,restaurant, start_date, end_date)
    end
    render :partial => 'super_admin/report/restaurant' and return

  end



  def evaluate_admins_graph(country,state,city,start_time, end_time)
    all_admins = Admin.all
    @current_month_dates = (start_time.to_date..end_time.to_date).collect{|date| date }
    if country.blank?
      admins = Admin.find_by_sql("Select admins.created_at, admins.amount_paid, admins.amount_paid_date from admins where created_at >= '#{start_time.beginning_of_day}' and created_at <= '#{end_time.end_of_day}' order by admins.created_at asc")
    end
    if !country.blank? and state.blank?
      admins =Admin.find_by_sql("Select admins.created_at, admins.amount_paid, admins.amount_paid_date from admins inner join restaurantdetails on restaurantdetails.id=admins.restaurant_id where restaurantdetails.country='#{country}' and admins.created_at >=  '#{start_time.beginning_of_day}' and admins.created_at <= '#{end_time.end_of_day}' order by admins.created_at asc")
    end
    if !country.blank? and !state.blank? and city.blank?
      admins =Admin.find_by_sql("Select admins.created_at, admins.amount_paid, admins.amount_paid_date from admins inner join restaurantdetails on restaurantdetails.id=admins.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' and admins.created_at >=  '#{start_time.beginning_of_day}' and admins.created_at <= '#{end_time.end_of_day}' order by admins.created_at asc")
    end
    if !country.blank? and !state.blank? and !city.blank?
      admins =Admin.find_by_sql("Select admins.created_at, admins.amount_paid, admins.amount_paid_date from admins inner join restaurantdetails on restaurantdetails.id=admins.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' and restaurantdetails.city='#{city}' and admins.created_at >=  '#{start_time.beginning_of_day}' and admins.created_at <= '#{end_time.end_of_day}' order by admins.created_at asc")
    end
    @count = {}
    @amount_paid = {}
    @amount_paid_date = all_admins.collect{|a| (a.amount_paid_date.to_date unless a.amount_paid_date.nil?)}.uniq.compact
    @current_month_dates.each do |date|
      @count[date] = 0
      @amount_paid[date] = 0
      admins.each{|admin| @count[date]+=1 if admin.created_at.to_date == date.to_date}
      all_admins.each do |admin|
        unless admin.amount_paid_date.nil?
          if admin.amount_paid_date.to_date == date.to_date
            @amount_paid[date]+= admin.amount_paid.nil? ? 0 : admin.amount_paid.to_f
          end
        end
      end
    end
    country = Carmen::Country.coded(country)
    @country_name = country.name rescue ''
    @state_name =country.subregions.coded(state).name rescue state
    @city_name = city
  end

  def evaluate_all_time_admins_graph(country, state, city)
    all_admins = Admin.all
    if country.blank?
      admins = Admin.find_by_sql("Select admins.created_at, admins.amount_paid, admins.amount_paid_date from admins order by admins.created_at asc")
    end
    if !country.blank? and state.blank?
      admins =Admin.find_by_sql("Select admins.created_at, admins.amount_paid, admins.amount_paid_date from admins inner join restaurantdetails on restaurantdetails.id=admins.restaurant_id where restaurantdetails.country='#{country}' order by admins.created_at asc")
    end
    if !country.blank? and !state.blank? and city.blank?
      admins =Admin.find_by_sql("Select admins.created_at, admins.amount_paid, admins.amount_paid_date from admins inner join restaurantdetails on restaurantdetails.id=admins.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' order by admins.created_at asc")
    end
    if !country.blank? and !state.blank? and !city.blank?
      admins =Admin.find_by_sql("Select admins.created_at, admins.amount_paid, admins.amount_paid_date from admins inner join restaurantdetails on restaurantdetails.id=admins.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' and restaurantdetails.city='#{city}' order by admins.created_at asc")
    end
    @count = {}
    @amount_paid = {}
    @current_month_dates = (admins.collect{|a| a.created_at.to_date}.uniq + admins.collect{|a| a.amount_paid_date.to_date unless a.amount_paid_date.nil?}.uniq.compact).uniq
    @amount_paid_date = all_admins.collect{|a| (a.amount_paid_date.to_date unless a.amount_paid_date.nil?)}.uniq.compact
    @current_month_dates.each do |date|
      @count[date] = 0
      @amount_paid[date] = 0
      admins.each{|admin| @count[date]+=1 if admin.created_at.to_date == date.to_date}
      all_admins.each do |admin|
        unless admin.amount_paid_date.nil?
          if admin.amount_paid_date.to_date == date.to_date
            @amount_paid[date]+= admin.amount_paid.nil? ? 0 : admin.amount_paid.to_f
          end
        end
      end
    end
    country = Carmen::Country.coded(country)
    @country_name = country.name rescue ''
    @state_name =country.subregions.coded(state).name rescue state
    @city_name = city
  end

  def evaluate_restaurant_graph(country, state, city, restaurant, start_time, end_time)
    @current_month_dates = (start_time.to_date..end_time.to_date).collect{|date| date }
    if country.blank?
      orders = Order.find_by_sql("Select orders.created_at, orders.total_bill from orders where created_at >= '#{start_time.beginning_of_day}' and created_at <= '#{end_time.end_of_day}' order by orders.created_at asc")
    end
    if !country.blank? and state.blank?
      orders = Order.find_by_sql("Select orders.created_at, orders.total_bill from orders inner join restaurantdetails on restaurantdetails.id=orders.restaurant_id where restaurantdetails.country='#{country}' and orders.created_at >=  '#{start_time.beginning_of_day}' and orders.created_at <= '#{end_time.end_of_day}' order by orders.created_at asc")
    end
    if !country.blank? and !state.blank? and city.blank?
      orders = Order.find_by_sql("Select orders.created_at, orders.total_bill from orders inner join restaurantdetails on restaurantdetails.id=orders.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' and orders.created_at >=  '#{start_time.beginning_of_day}' and orders.created_at <= '#{end_time.end_of_day}' order by orders.created_at asc")
    end
    if !country.blank? and !state.blank? and !city.blank? and restaurant.blank?
      orders = Order.find_by_sql("Select orders.created_at, orders.total_bill from orders inner join restaurantdetails on restaurantdetails.id=orders.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' and restaurantdetails.city='#{city}' and orders.created_at >=  '#{start_time.beginning_of_day}' and orders.created_at <= '#{end_time.end_of_day}' order by orders.created_at asc")
    end
    if !country.blank? and !state.blank? and !city.blank? and !restaurant.blank?
      orders = Order.find_by_sql("Select orders.created_at, orders.total_bill from orders inner join restaurantdetails on restaurantdetails.id=orders.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' and restaurantdetails.city='#{city}' and restaurantdetails.id='#{restaurant.id}' and orders.created_at >=  '#{start_time.beginning_of_day}' and orders.created_at <= '#{end_time.end_of_day}' order by orders.created_at asc")
    end
    @count = {}
    @amount_paid = {}
    @current_month_dates = orders.collect{|a| a.created_at.to_date}.uniq
    @current_month_dates.each do |date|
      @count[date] = 0
      @amount_paid[date] = 0
      orders.each{|order| @count[date]+=1 if order.created_at.to_date == date.to_date}
      orders.each{|order| @amount_paid[date]+= (order.total_bill.nil? ? 0 : order.total_bill.to_f) if order.created_at.to_date == date.to_date}
    end
    country = Carmen::Country.coded(country)
    @country_name = country.name rescue ''
    @state_name =country.subregions.coded(state).name rescue state
    @city_name = city
    @restaurant_name = restaurant.restaurant_name.html_safe rescue ""
  end

  def evaluate_all_time_restaurant_graph(country, state, city,restaurant)
    if country.blank?
      orders = Order.find_by_sql("Select orders.created_at, orders.total_bill from orders order by orders.created_at asc")
    end
    if !country.blank? and state.blank?
      orders =Order.find_by_sql("Select orders.created_at, orders.total_bill from orders inner join restaurantdetails on restaurantdetails.id=orders.restaurant_id where restaurantdetails.country='#{country}' order by orders.created_at asc")
    end
    if !country.blank? and !state.blank? and city.blank?
      orders =Order.find_by_sql("Select orders.created_at, orders.total_bill from orders inner join restaurantdetails on restaurantdetails.id=orders.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' order by orders.created_at asc")
    end
    if !country.blank? and !state.blank? and !city.blank? and restaurant.blank?
      orders =Order.find_by_sql("Select orders.created_at, orders.total_bill from orders inner join restaurantdetails on restaurantdetails.id=orders.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' and restaurantdetails.city='#{city}' order by orders.created_at asc")
    end
    if !country.blank? and !state.blank? and !city.blank? and !restaurant.blank?
      orders = Order.find_by_sql("Select orders.created_at, orders.total_bill from orders inner join restaurantdetails on restaurantdetails.id=orders.restaurant_id where restaurantdetails.country='#{country}' and restaurantdetails.state='#{state}' and restaurantdetails.city='#{city}' and restaurantdetails.id='#{restaurant.id}' and orders.created_at >=  '#{start_time.beginning_of_day}' and orders.created_at <= '#{end_time.end_of_day}' order by orders.created_at asc")
    end
    @count = {}
    @amount_paid = {}
    @current_month_dates = orders.collect{|a| a.created_at.to_date}.uniq
    @current_month_dates.each do |date|
      @count[date] = 0
      @amount_paid[date] = 0
      orders.each{|order| @count[date]+=1 if order.created_at.to_date == date.to_date}
      orders.each{|order| @amount_paid[date]+= (order.total_bill.nil? ? 0 : order.total_bill.to_f) if order.created_at.to_date == date.to_date}
    end
    country = Carmen::Country.coded(country)
    @country_name = country.name rescue ''
    @state_name =country.subregions.coded(state).name rescue state
    @city_name = city
    @restaurant_name = restaurant.restaurant_name.html_safe rescue ""
  end

  private

  def set_instance_variable
    @report_class = 'active'
    @open = 'open'
    @block = 'block'
    @countries = Restaurantdetail.all.collect{|r| {r.country => (Carmen::Country.coded(r.country).nil? ? r.country : Carmen::Country.coded(r.country).name)}}.uniq

  end
end