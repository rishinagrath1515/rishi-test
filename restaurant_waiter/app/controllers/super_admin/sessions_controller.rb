class SuperAdmin::SessionsController < Devise::SessionsController
  skip_before_filter :authenticate_super_admin!, :only => [ :new ]

  layout 'login'

  def self.permission
    return "super_admin/sessions"
  end

  # GET /resource/sign_in
  def new
    super
    #self.resource = resource_class.new(sign_in_params)
    #clean_up_passwords(resource)
    #respond_with resource, location: new_user_registration_path
  end

  # POST /resource/sign_in
  def create
    #sign_out current_admin if current_admin
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:success, :signed_in) if is_flashing_format?
    sign_in(resource_name, resource)
    yield resource if block_given?
    respond_with resource, location: after_sign_in_path_for_super_admin(resource)
  end

  # DELETE /resource/sign_out
  def destroy
    #signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    signed_out = sign_out(resource_name)
    set_flash_message :notice, :signed_out if signed_out && is_flashing_format?
    yield if block_given?
    respond_to_on_destroy_admin
  end


  def require_no_authentication
    assert_is_devise_resource!
    return unless is_navigational_format?
    no_input = devise_mapping.no_input_strategies
    authenticated = if no_input.present?
                      args = no_input.dup.push scope: resource_name
                      warden.authenticate?(*args)
                    else
                      warden.authenticated?(resource_name)
                    end
    if authenticated && resource = warden.user(resource_name)
      flash[:alert] = I18n.t("devise.failure.already_authenticated")
      redirect_to after_sign_in_path_for_super_admin(resource)
    end
  end

  def respond_to_on_destroy_admin
    # We actually need to hardcode this as Rails default responder doesn't
    # support returning empty response on GET request
    respond_to do |format|
      format.all { head :no_content }
      format.any(*navigational_formats) { redirect_to after_sign_out_path_for_super_admin(resource_name) }
    end
  end
end
