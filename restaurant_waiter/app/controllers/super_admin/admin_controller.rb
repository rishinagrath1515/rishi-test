class SuperAdmin::AdminController < ApplicationController
  authorize_resource :class => false
  before_filter :authenticate_super_admin!
  before_filter :get_super_admin_notifications
  before_filter :define_instance_variables, :only => [:view_customers, :new_customers]
  layout 'admin_dashboard'

  def index
    @confirm_class= 'active'
    @restaurants = Restaurantdetail.find_by_sql("Select DISTINCT * from restaurantdetails inner join admins on restaurantdetails.id= admins.restaurant_id where admins.is_paid=1")
    render :template => 'super_admin/index'
  end

  def update
    admin = Admin.where(:id=> params[:id]).first
    admin.update_attributes(:confirm_status => params[:status],:amount_paid => (params[:status]=="0" ? "" : params[:amount_paid]), :amount_paid_date => DateTime.now) unless admin.nil?
    SuperAdminMailer.send_confirmation_mail(admin).deliver if admin.confirm_status
    WebsocketRails[:posts].trigger 'new', admin  if admin.confirm_status
    render :json => {:success => admin.confirm_status.to_s}
  end

  def ad_controller
    @ads_class = 'active'
    @cities =[]
    @ads =[]
    @restaurants =[]
    render :template => 'super_admin/ads'
  end

  def get_city
    @cities = Restaurantdetail.where(:state => params[:state], :country => params[:country]).select("city").uniq
    render :partial => 'super_admin/city_detail'
  end

  def get_restaurantdetails
    @restaurants = Restaurantdetail.where(:state => params[:state], :country => params[:country], :city => params[:city]).uniq
    render :partial => 'super_admin/restaurant_detail'
  end

  def get_adsdetails
    @ads = Advertisement.where(:restaurant_id => params[:id]) rescue []
    render :partial => 'super_admin/ad_details'
  end

  def ad_settings
    if request.method=='POST'
      ad = Advertisement.where("id=?", params[:id]).first
      updated = ad.update_attributes(:ad_settings => (params[:ad_setting]=="0" ? 1 : 0))
      respond_to do |format|
        if updated
          #flash[:notice]= "Advertisement has been updated successfully."
          format.html {redirect_to :back}
          format.json {render :json => {:success => true, :settings => ad.ad_settings}}
        else
          #flash[:error]= "Unable to delete advertisement, please try again after some time."
          format.html {redirect_to :back}
          format.json {render :json => {:success => false, :settings => ad.ad_settings}}
        end
      end
    else
      isDeleted=Advertisement.find_by_id(params[:id]).destroy
      respond_to do |format|
      if isDeleted
          #flash[:notice]= "Advertisement has been deleted successfully."
          format.html {redirect_to :back}
          format.json {render :json => {:success => true}}
      else
          flash[:error]= "Unable to delete advertisement, please try again after some time."
          format.html {redirect_to :back}
          format.json {render :json => {:success => false}}
        end
      end
    end
  end

  def view_customers
    @ec = 'active'      # @ec implies Existing Customer
    @restaurants = Restaurantdetail.find_by_sql("Select DISTINCT * from restaurantdetails inner join admins on admins.restaurant_id=restaurantdetails.id where admins.is_paid=1 and admins.confirm_status=1")
    render :template => 'super_admin/customers'
  end

  def new_customers
    @nc = 'active'      # @nc implies Existing Customer
    @restaurants = Restaurantdetail.find_by_sql("Select DISTINCT * from restaurantdetails inner join admins on admins.restaurant_id=restaurantdetails.id where admins.is_paid=0 and admins.confirm_status=0")
    render :template => 'super_admin/customers'
  end

  def update_admin_status
    admin = Admin.where(:id=>params[:id]).first
    admin.update_attributes(:active => (admin.active ? false : true))
    render :json => {:success => true ,:confirmed => admin.active}
  end

  def get_notification
    render :partial => 'super_admin/shared/notification'
  end

  private

  def define_instance_variables
    @customer_open = 'open'
    @customer_block = 'block'
    @li_active = 'active'
  end
end