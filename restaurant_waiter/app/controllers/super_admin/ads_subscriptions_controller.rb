class SuperAdmin::AdsSubscriptionsController < ApplicationController
  load_and_authorize_resource
  before_filter :authenticate_super_admin!
  before_filter :get_super_admin_notifications
  before_action :set_super_admin_ads_subscription, only: [:show, :edit, :update, :destroy]

  layout 'admin_dashboard'

  # GET /super_admin/ads_subscriptions
  # GET /super_admin/ads_subscriptions.json
  def index
    @subscription_class = 'active'
    @super_admin_ads_subscriptions = AdsSubscription.all
  end

  # GET /super_admin/ads_subscriptions/1
  # GET /super_admin/ads_subscriptions/1.json
  def show
  end

  # GET /super_admin/ads_subscriptions/new
  def new
    @super_admin_ads_subscription = AdsSubscription.new
  end

  # GET /super_admin/ads_subscriptions/1/edit
  def edit
  end

  # POST /super_admin/ads_subscriptions
  # POST /super_admin/ads_subscriptions.json
  def create
    @super_admin_ads_subscription = AdsSubscription.new(super_admin_ads_subscription_params)

    respond_to do |format|
      if @super_admin_ads_subscription.save
        format.html { redirect_to @super_admin_ads_subscription, notice: 'Ads subscription was successfully created.' }
        format.json { render :json => {:success => true, :ad_id => @super_admin_ads_subscription.id} }
      else
        format.html { render action: 'new' }
        format.json { render :json => {:success => false} }
      end
    end
  end

  # PATCH/PUT /super_admin/ads_subscriptions/1
  # PATCH/PUT /super_admin/ads_subscriptions/1.json
  def update
    respond_to do |format|
      if @super_admin_ads_subscription.update(super_admin_ads_subscription_params)
        format.html { redirect_to @super_admin_ads_subscription, notice: 'Ads subscription was successfully updated.' }
        format.json { render :json => {:success => true} }
      else
        format.html { render action: 'edit' }
        format.json { render :json => {:success => false} }
      end
    end
  end

  # DELETE /super_admin/ads_subscriptions/1
  # DELETE /super_admin/ads_subscriptions/1.json
  def destroy
    @super_admin_ads_subscription.destroy
    respond_to do |format|
      format.html { redirect_to super_admin_ads_subscriptions_url }
      format.json { render :json => {:success => true} }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_super_admin_ads_subscription
      @super_admin_ads_subscription = AdsSubscription.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def super_admin_ads_subscription_params
      params[:super_admin_ads_subscription].permit([:name, :ad_type, :price, :id, :short_name, :no_of_days])
    end
end
