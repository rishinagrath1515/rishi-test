=begin
  @ Description :This Contains all the methods related to the payment(Stripe is used for payments).
  @ Copyright :
  @ Developed by : Click Labs Pvt. Ltd
=end

class PaymentController < ApplicationController

#API key For stripe
#Stripe.api_key = "sk_test_4RvArL8TjPYVMnjkm4XhXr1c"
  Stripe.api_key = "sk_live_eFr3TqziO0TnMT3xMxaAeeSt"
  require 'stripe'

#to test methods of payment class(shift control to test file)
  def paymentdetails
  end

=begin
  @ Description :  Payment transactions for order
  @input : useraccesstoken,tip,orderid,totalamount,flag
           {flag=>1 in case authorized user pay bill,0->in case of split bill}
   @output : transact total bill amount from user default card
=end

  def paymenttransactions
    if (params[:useraccesstoken].blank?) || (params[:tip].blank?)||(params[:orderid].blank?) || (params[:totalamount].blank?) ||(params[:flag].blank?) || (params[:totalbill].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized

      if params[:flag] then
        customerId=(@isUseAuthorized.customer_id_default)==0 ? (@isUseAuthorized.customer_id1) : (@isUseAuthorized.customer_id2)
        begin
          charge=Stripe::Charge.create(
              :amount => ((params[:totalamount].to_f)*100).to_i, # in cents
              :currency => "usd",
              :customer => customerId
          )
        rescue Stripe::CardError => e
          # Since it's a decline, Stripe::CardError will be caught
          body = e.json_body
          err = body[:error]
          render :json => {"error" => err[:message]} and return
        end

      else
        # email=params['email']
        # if email.blank?
        #  return invalid_params_msg
        #end
        begin
          customer = Stripe::Customer.create(
              :card => params[:stripe_token],
              :description => 'Payment for user'
          )
        rescue Stripe::CardError => e
          # Since it's a decline, Stripe::CardError will be caught
          body = e.json_body
          err = body[:error]
          render :json => {"error" => err[:message]} and return
        end

        begin
          # Charge the Customer instead of the card
          charge=Stripe::Charge.create(
              :amount => ((params[:totalamount].to_f)*100).to_i, # in cents
              :currency => "usd",
              :customer => customer.id
          )
        rescue Stripe::CardError => e
          # Since it's a decline, Stripe::CardError will be caught
          body = e.json_body
          err = body[:error]
          render :json => {"error" => err[:message]} and return
        end
      end

      if charge['paid'] then
        Order.where("id = ?", params[:orderid]).update_all(:total_bill => params[:totalbill])
        orderInfo=Order.find(params[:orderid])
        if orderInfo.pay_status then
          pendingBill=((orderInfo.pending_bill).to_f)- (params[:totalamount].to_f)
          pendingBill=pendingBill < 0 ? 0 : (pendingBill.round(2))
        else
          pendingBill=((orderInfo.total_bill).to_f)- (params[:totalamount].to_f)
          pendingBill=pendingBill < 0 ? 0 : (pendingBill.round(2))
        end
        Order.where("id = ?", params[:orderid]).update_all(:pending_bill => pendingBill, :tip => params[:tip], :pay_status => 1, :confirm_status => 1)
        render :json => JSON.pretty_generate({"log" => "You have successfully paid your bill."})
      else
        render :json => JSON.pretty_generate({"error" => charge['error']['message']})
      end
    end

  end

#end  paymenttransactions


=begin
  @ Description :  User can add atmost two credit card numbers
  @input : useraccesstoken,stripetoken
   @output : save last 4 digits of credit card number and also register user on stripe and get customer id
=end

  def addcardnumber
    if (params[:useraccesstoken].blank?) || (params[:stripetoken].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      begin
        customer = Stripe::Customer.create(
            :card => params[:stripetoken],
            :description => @isUseAuthorized.email
        )
      rescue Stripe::CardError => e
        # Since it's a decline, Stripe::CardError will be caught
        body = e.json_body
        err = body[:error]
        render :json => {"error" => err[:message]} and return
      end

      cardNumber=customer['cards']['data'][0]['last4']
      if (@isUseAuthorized.card1).blank? || @isUseAuthorized.card1==0
        isUpdated=User.where("id = ?", @isUseAuthorized.id).update_all(:cc_is_registered => 1, :card1 => cardNumber, :customer_id1 => customer.id, :customer_id_default => 0)
      else
        if (@isUseAuthorized.card1)==(cardNumber.to_i) then
          render :json => JSON.pretty_generate({"error" => "This card number has already added."}) and return
        else
          isUpdated=User.where("id = ?", @isUseAuthorized.id).update_all(:card2 => cardNumber, :customer_id2 => customer.id)
        end

      end
      if isUpdated then
        render :json => JSON.pretty_generate({"log" => "1"})
      else
        render :json => JSON.pretty_generate({"error" => "Unable to add card number. Please try again later."})
      end
    end
  end

#end  addcardnumber


=begin
  @ Description :  Get list of card numbers added by the user
  @input : useraccesstoken
  @output : display list of card numbers
=end

  def getcardlist
    if (params[:useraccesstoken].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      if @isUseAuthorized.cc_is_registered then
        card1=((@isUseAuthorized.card1).blank?) ? 0 : @isUseAuthorized.card1
        card2=((@isUseAuthorized.card2).blank?) ? 0 : @isUseAuthorized.card2
        default=((@isUseAuthorized.customer_id_default).blank?) ? 0 : @isUseAuthorized.customer_id_default
        render :json => JSON.pretty_generate({"data" => {'card1' => card1, 'card2' => card2, "default" => default}})
      else
        render :json => JSON.pretty_generate({"error" => "No card is added yet."})
      end
    end
  end

#end  getcardlist


=begin
  @ Description :  Set a default card
  @input : useraccesstoken,cardnumber{0->to set first card as default,1->to set second card as default}
  @output : set given card number as default card
=end
  def setdefaultcard
    if (params[:useraccesstoken].blank?) || (params[:cardnumber].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      isUpdated=User.where("id = ?", @isUseAuthorized.id).update_all(:customer_id_default => params[:cardnumber])
      if isUpdated then
        cardNumber=params[:cardnumber]=="0" ? @isUseAuthorized.card1 : @isUseAuthorized.card2
        render :json => JSON.pretty_generate({"log" => "1", "default_card_number" => cardNumber})
      else
        render :json => JSON.pretty_generate({"error" => "Unable to update default card. Please try again later."})
      end
    end
  end

#end  setdefaultcard


=begin
  @ Description :  Delete card number except default card
  @input : useraccesstoken,cardnumber{0->to delete first card ,1->to delete second card}
  @output : delete given card number
=end
  def deletecard
    if (params[:useraccesstoken].blank?) || (params[:cardnumber].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      if @isUseAuthorized.customer_id_default==(params[:cardnumber].to_i) then
        render :json => JSON.pretty_generate({"error" => "Default card cannot be deleted."}) and return
      end
      if params[:cardnumber]=="0" then
        isUpdated=User.where("id = ?", @isUseAuthorized.id).update_all(:card1 => '', :customer_id1 => '')
      else
        isUpdated=User.where("id = ?", @isUseAuthorized.id).update_all(:card2 => '', :customer_id2 => '')
      end
      if isUpdated then
        render :json => JSON.pretty_generate({"log" => "1"})
      else
        render :json => JSON.pretty_generate({"error" => "Unable to delete card. Please try again later."})
      end
    end
  end

#end  deletecard


=begin
  @ Description :  In case user chose pay later option. This method will be called by CRON and deduct bill
                   amount from user's default card after 24 hours. 
  @output : deduct total bill from users's default card after 24 hours of order.
=end
  def paylater
    @pendingPayments=Order.where("pending_bill > ? ", 0)
    @pendingPayments.each do |val|
      currentDate=Time.now
      created_at = val['created_at']
      #hours = (((currentDate.to_time) - (created_at.to_time)) / 1.hour).round
      time_diff_components = Time.diff(currentDate, created_at)
      if time_diff_components[:hour] >= 24 then
        userInfo=User.find(val['user_id']);
        customerId=(userInfo['customer_id_default']) == 0 ? (userInfo['customer_id1']) : (userInfo['customer_id2'])
        if customerId then
          charge=Stripe::Charge.create(
              :amount => ((val['pending_bill'].to_f)*100).to_i, # in cents
              :currency => "usd",
              :customer => customerId
          )
          if charge['paid'] then
            Order.where("id = ?", val[:id]).update_all(:pending_bill => 0,:pay_status => 1)
          end
        end
      end
    end
  end

#end  paylater


=begin
  @ Description :  In case user chose pay later option. This method will be used to update tip provided by user
  @output : add tip to the user order
=end
  def updatetipinfo
    if (params[:useraccesstoken].blank?) || (params[:tip].blank?) || (params[:orderid].blank?) then
      return invalid_params_msg
    else
      @isUseAuthorized=User.find_by_user_access_token(params[:useraccesstoken])
      return unauthorized_user unless  @isUseAuthorized
      isUpdated=Order.where("id = ?", params[:orderid]).update_all(:tip => params[:tip]);
      if isUpdated then
        render :json => JSON.pretty_generate({"log" => "1"})
      else
        render :json => JSON.pretty_generate({"log" => "0"})
      end
    end
  end

#end  updatetipinfo

  private

  #error in case invalid parameters passed.
  def invalid_params_msg
    render :json => JSON.pretty_generate({"error" => "some parameter missing"})
  end

  #error in case invalid access token(unauthorized access).
  def unauthorized_user
    render :json => JSON.pretty_generate({"error" => "Unauthorized access."})
  end
end
