module Ethor
  API_KEY="XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n"
  ETHOR_URL="https://ethor-test.apigee.net:443/v1"

  module Chain

    class << self.extend(Ethor)

     # Retrieve a list of chains
     # API: Ethor::Chain.get_list('fields' => ["id","subdomain"], 'limit' => 20)
     # https://ethor-test.apigee.net:443/v1/chains?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=id%2Csubdomain&offset=0&limit=20
      def get_list(parameters = {})
        extra_params parameters
        response = RestClient.get "#{ETHOR_URL}/chains?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
        data = JSON.parse(response)
        return data
      end

     # Retrieve a list of stores for the requested chain ID
     # API: Ethor::Chain.get_stores_list('fields' => ["id", "chain_id"], 'limit' => 20, 'offset' => 1, 'chain_id' => 'YCJC0YLTNG')
     # https://ethor-test.apigee.net:443/v1/chains/YCJC0YLTNG/stores?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&offset=0&limit=10
      def get_stores_list(parameters = {})
        extra_params parameters
        return {"error" => "Chain Id is required"} if @chain.blank?
        response = RestClient.get "#{ETHOR_URL}/chains/#{@chain}/stores?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
        data = JSON.parse(response)
        return data
      end

     # Retrieve a store by its unique ID
     # Ethor::Chain.get_store('chain_id' => 'YCJC0YLTNG', 'store' => 'RURZ978VFI', 'fields' => ["id","chain_id"])
     # https://ethor-test.apigee.net:443/v1/chains/YCJC0YLTNG/stores/RURZ978VFI?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=chain_id
      def get_store(parameters = {})
        extra_params parameters
        return {"error" => "Chain Id is required"} if @chain.blank?
        return {"error" => "Store Id is required"} if @store.blank?
        response = RestClient.get "#{ETHOR_URL}/chains/#{@chain}/stores/#{@store}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
        data = JSON.parse(response)
        return data
      end

     # Retrieve a list of customers for the requested chain ID
     # API: Ethor::Chain.get_customers_list('fields' => ["first_name", "last_name"], 'limit' => 20, 'offset' => 1, 'chain_id' => 'YCJC0YLTNG')
     # https://ethor-test.apigee.net:443/v1/chains/YCJC0YLTNG/customers?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=customer_id%2Cfirst_name&offset=0&limit=10
      def get_customers_list(parameters = {})
        extra_params parameters
        return {"error" => "Chain Id is required"} if @chain.blank?
        response = RestClient.get "#{ETHOR_URL}/chains/#{@chain}/customers?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
        data = JSON.parse(response)
        return data
      end

     # Retrieve a customer by its unique ID
     # API: Ethor::Chain.get_customer('fields' => ["customer_id", "first_name"], 'limit' => 20, 'offset' => 1, 'chain_id' => 'YCJC0YLTNG', 'customer_id' => 'P2AMIDOZA8')
     # https://ethor-test.apigee.net:443/v1/chains/YCJC0YLTNG/customers/P2AMIDOZA8?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=first_name%2Clast_name%2Cemail_address
      def get_customer(parameters = {})
        extra_params parameters
        return {"error" => "Chain Id is required"} if @chain.blank?
        return {"error" => "Customer Id is required"} if @customer.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/chains/#{@chain}/customers/#{@customer}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

    end # Class Ends Here

  end

  module Menu

    class << self.extend(Ethor)

     # Retrieve a menu for the given store ID
     # API: Ethor::Menu.get_list('store' => 'HI6PIDO5JS', 'categories' => ['3WRPIBR4FS', 'LTZR0RJRN8'])
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/menu?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&categories=3WRPIBR4FS%2CLTZR0RJRN8
      def get_list(parameters = {})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/menu?apikey=#{API_KEY}#{@categories}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve a menu item by its unique ID
     # API: Ethor::Menu.get_menu_item('store' => 'RURZ978VFI', 'menu_item_id' => 'QXD7I1ATD4', 'fields' => ["menu_item_id","menu_item_name"])
     # https://ethor-test.apigee.net:443/v1/stores/RURZ978VFI/menu/items/QXD7I1ATD4?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=menu_item_id%2Cmenu_item_name
      def get_menu_item(parameters = {})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Menu Item Id is required"} if @item.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/menu/items/#{@item}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve the menu categories for the given store ID
     # API: Ethor::Menu.get_menu_categories('store' => 'HI6PIDO5JS', 'fields' => ["category_name"])
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/menu/categories?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=category_name&offset=0&limit=10
      def get_menu_categories(parameters = {})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/menu/categories?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
        data = JSON.parse(response)
        return data
      end

    end # Class Ends Here

  end

  module Customer

    class << self.extend(Ethor)

     # Retrieve a list of customers for the requested store ID
     # API: Ethor::Customer.get_restaurant_customers('restaurant_id' => 'HI6PIDO5JS', 'fields' => ["first_name", "last_name"])
     # https://ethor-test.apigee.net:443/v1/customers?restaurant_id=HI6PIDO5JS&apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&offset=0&limit=10
      def get_restaurant_customers(parameters={})
        extra_params parameters
        return {"error" => "Restaurant Id is required"} if @restaurant.blank?
        response = RestClient.get "#{ETHOR_URL}/customers?restaurant_id=#{@restaurant}&apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
        data = JSON.parse(response)
        return data
      end

     # Create a new customer
     # body = {"first_name"=> "Braham","last_name"=> "Shakti","gender"=> "M","birth_date"=> "1983-06-19","email_address"=> "brahamshakti@gmail.com","phone_number"=> {"type"=> "home", "area_code"=> "011","number"=> "786786"}}
     # API: Ethor::Customer.create(body, 'restaurant_id' => 'HI6PIDO5JS')
     # https://ethor-test.apigee.net:443/v1/customers/create?restaurant_id=HI6PIDO5JS&apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def create(body, parameters={})
        extra_params parameters
        return {"error" => "Restaurant Id is required"} if @restaurant.blank?
        begin
          response = RestClient.post "#{ETHOR_URL}/customers/create?restaurant_id=#{@restaurant}&apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Update a existing customer
     # body = {"first_name"=> "Lela","last_name"=> "Wati","gender"=> "F","birth_date"=> "2001-06-15","email_address"=> "leelawati@testmail.com","phone_number"=> {"type"=> "home", "area_code"=> "011","number"=> "786786"}}
     # API: Ethor::Customer.update(body, 'restaurant_id' => 'HI6PIDO5JS', 'customer_id' => 'P7UJII3OVC')
     # https://ethor-test.apigee.net:443/v1/customers/update/P7UJII3OVC?restaurant_id=HI6PIDO5JS&apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def update(body,parameters={})
        extra_params parameters
        return {"error" => "Restaurant Id is required"} if @restaurant.blank?
        return {"error" => "Customer Id is required"} if @restaurant.blank?
        begin
          response = RestClient.put "#{ETHOR_URL}/customers/update/#{@customer}?restaurant_id=#{@restaurant}&apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve a list of order history from a customer by its unique ID
     # API: Ethor::Customer.customer_history('customer_id' => 'P2AMIDOZA8', 'fields' => ["order_id"])
     # https://ethor-test.apigee.net:443/v1/customers/P2AMIDOZA8/history?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&offset=0&limit=10
      def customer_history(parameters={})
        extra_params parameters
        return {"error" => "Restaurant Id is required"} if @customer.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/customers/#{@customer}/history?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

    end # Class Ends Here

  end

  module Store

    class << self.extend(Ethor)

     # Retrieve a list of stores
     # API: Ethor::Store.get_list('lat'=>'51.045219','lng'=>'-114.062981', 'dist' => 5)
     # https://ethor-test.apigee.net:443/v1/stores?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&lat=51.045219&lng=-114.062981&dist=5&offset=0&limit=10
      def get_list(parameters={})
        extra_params parameters
        begin
          response = RestClient.get "#{ETHOR_URL}/stores?apikey=#{API_KEY}#{@lat}#{@lng}#{@dist}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve a store by its unique ID
     # API: Ethor::Store.get_store('store' => 'HI6PIDO5JS', 'fields'=>["id","chain_id","operating_hours"])
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=id%2Cchain_id%2Coperating_hours
      def get_store(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve a list of tables by store ID
     # API: Ethor::Store.get_restaurant_tables('store' => 'RURZ978VFI', 'fields' => ["id", "name","store_id"])
     # https://ethor-test.apigee.net:443/v1/stores/RURZ978VFI/tables?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&offset=0&limit=10
      def get_restaurant_tables(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/tables?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve a table by its unique ID
     # API: Ethor::Store.get_table('store' => 'RURZ978VFI','table_id' => 'Y6ZF0U742C', 'fields' => ["id", "name","store_id"])
     # https://ethor-test.apigee.net:443/v1/stores/RURZ978VFI/tables/Y6ZF0U742C?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def get_table(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Table Id is required"} if @table.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/tables/#{@table}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

    end # Class Ends Here

  end

  module Order

    class << self.extend(Ethor)

     # Retrieve a list of orders for the requested store ID
     # API: Ethor::Order.get_store_orders('store' => 'RURZ978VFI', 'fields' => ["order_id", "order_date"], 'start_date' => '11/13/2014', 'end_date' => '11/26/2014')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/orders?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&start_date=09%2F10%2F2014&end_date=11%2F26%2F2014&offset=0&limit=10
      def get_store_orders(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/orders?apikey=#{API_KEY}#{@start_date}#{@end_date}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve an order identified by its unique ID
     # Ethor::Order.get_order('store' => 'RURZ978VFI','order_id'=>'LLQMISO1SW', 'fields' => ["order_id", "order_date"])
     # https://ethor-test.apigee.net:443/v1/stores/RURZ978VFI/orders/LLQMISO1SW?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=order_id
      def get_order(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Order Id is required"} if @order.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/orders/#{@order}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", {:accept => :json}
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Validate and calculate an order for the store identified by ID
     # body = {"order_type" => "pickup","order_status" => "STAGED","customer" => {"customer_id" => "P2AMIDOZA8","first_name" => "Braham","last_name" => "Shakti","gender" => "M", "email_address" => "brahamshakti@gmail.com","phone_number" => {"type" => "home","area_code" => "0171","number" => "2520002"}},"order_items" => [{"description" => "Order","menu_item_id" => "VD1PIEBIIG","menu_item_name" => "Create Your Own","quantity" => "2","variant_id" => "TXDOR7S83E","variant_name" => "X-Lg 18\"",   "modifiers" => [{"modifier_id" => "6HEK9TXSBQ","modifier_name" => "Shrimp"}]}]}
     # API: Ethor::Order.calculate_order(body, 'store' => 'HI6PIDO5JS','check_location'=> true)
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/orders/calculate?check_location=true&apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def calculate_order(body, parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        begin
          response = RestClient.post "#{ETHOR_URL}/stores/#{@store}/orders/calculate?apikey=#{API_KEY}#{@check_location}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve an order identified by its unique POS Ticket ID
     # API: Ethor::Order.retrieve_order_by_pos_ticket_id('pos' => '1636', 'store' => 'HI6PIDO5JS')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/orders/pos/1636?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=order_id%2Ccustomer
      def retrieve_order_by_pos_ticket_id(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "POS Ticket Id is required"} if @pos.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/orders/pos/#{@pos}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Submit an order down the point of sale system for the store identified by ID
     # body = {"order_type" => "pickup","customer" => {"customer_id" => "P2AMIDOZA8","first_name" => "Braham","last_name" => "Shakti","gender" => "M","email_address" => "brahamshakti@gmail.com","phone_number" => {"type" => "home","area_code" => "0171","number" => "2520002"}},"order_items" => [{"description" => "Order","menu_item_id" => "VD1PIEBIIG", "menu_item_name" => "Create Your Own","quantity" => "1","variant_id" => "TXDOR7S83E","variant_name" => "X-Lg 18\""}],"order_payments" => [{"payment_type" => "C","payment_tendered" => 20}]}
     # API: Ethor::Order.submit_to_pos(body, 'store' => 'HI6PIDO5JS')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/orders/create?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def submit_to_pos(body, parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        begin
          response = RestClient.post "#{ETHOR_URL}/stores/#{@store}/orders/create?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

    end # Class Ends Here

  end

  module Ticket

    class << self.extend(Ethor)

     # Create a new ticket
     # body = {'order_type' => 'pickup'}
     # API: Ethor::Ticket.create(body,'store' => 'HI6PIDO5JS')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def create(body, parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        begin
          response = RestClient.post "#{ETHOR_URL}/stores/#{@store}/tickets?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Add items to a specific ticket identified by ID
     # body = {"order_items" => [{"menu_item_id" => "VD1PIEBIIG","menu_item_name" => "Create Your Own","modifiers" => [{"modifier_id" => "6HEK9TXSBQ","modifier_name" => "Shrimp"}], "quantity" => "1","total" => 31.98,"variant_id" => "TXDOR7S83E"}]}
     # API: Ethor::Ticket.add_items(body,'store' => 'HI6PIDO5JS', 'ticket_id' => 'NLXJIDZAFC')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/NGDMI9KKU8/items?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def add_items(body,parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Ticket Id is required"} if @ticket.blank?
        begin
          response = RestClient.post "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}/items?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          puts e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Add items to a specific ticket identified by ID
     # body = {"order_items" => [{"menu_item_id" => "W9UCRIKOFU","menu_item_name" => "Garlic Bread","quantity" => "1"}]}
     # API: Ethor::Ticket.update_items(body,'store' => 'HI6PIDO5JS', 'ticket_id' => 'NGDMI9KKU8')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/NGDMI9KKU8/items?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def update_items(body,parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Ticket Id is required"} if @ticket.blank?
        begin
          response = RestClient.put "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}/items?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Add discounts to a specific ticket identified by ID
     # body = {"order_discounts" => [{"discount_amount" => 2,"discount_name" => "Happy Hour","discount_code" => "HH01"},{"discount_amount" => 4,"discount_name" => "Employee Discount",  "discount_code" => "ED01"}]}
     # API: Ethor::Ticket.add_discount(body,'store' => 'HI6PIDO5JS', 'ticket_id' => 'NGDMI9KKU8')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/NGDMI9KKU8/discounts?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def add_discount(body,parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Ticket Id is required"} if @ticket.blank?
        begin
          response = RestClient.post "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}/discounts?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Add payment details to a specific ticket identified by ID
     # body = {"order_payments" => [{"payment_type" => "C","payment_tendered" => 20}]}
     # API: Ethor::Ticket.add_payments(body,'store' => 'HI6PIDO5JS', 'ticket_id' => 'NGDMI9KKU8')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/NGDMI9KKU8/payments?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def add_payments(body,parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Ticket Id is required"} if @ticket.blank?
        begin
          response = RestClient.post "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}/payments?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Calculate order down the point of sale system for the store identified by ID
     # API: Ethor::Ticket.calculate_order('store' => 'HI6PIDO5JS', 'ticket_id' => 'NGDMI9KKU8')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/NGDMI9KKU8/calculate?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def calculate_order(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Ticket Id is required"} if @ticket.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}/calculate?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Void a order ticket point of sale system for the store identified by ID
     # API: Ethor::Ticket.delete('store' => 'HI6PIDO5JS', 'ticket_id' => '5J5L0TRXMS')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/EHRLRJO98I?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def delete(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Ticket Id is required"} if @ticket.blank?
        begin
          response = RestClient.delete "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Update ticket down the point of sale system for the store identified by ID. Only Order_type and Order_date can be updated
     # body = {"order_type" =>"pickup","order_date" =>"2014-11-30T04:49:09.133+0000"}
     # API: Ethor::Ticket.update_ticket(body, 'store' => 'HI6PIDO5JS', 'ticket_id' => 'WKJK93VM12')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/WKJK93VM12?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def  update_ticket(body,parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Ticket Id is required"} if @ticket.blank?
        begin
          response = RestClient.post "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve a ticket identified by its unique ID
     # API: Ethor::Ticket.get_ticket('store' => 'HI6PIDO5JS', 'ticket_id' => 'WKJK93VM12', 'fields' => ['order_id', 'order_date', 'order_type'])
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/WKJK93VM12?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=order_id%2Corder_type
      def get_ticket(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Ticket Id is required"} if @ticket.blank?
        puts "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}"
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Submit order down the point of sale system for the store identified by ID
     # body = {"customer" => {"customer_id" => "P2AMIDOZA8","first_name" => "Braham","last_name" => "Shakti","gender" => "M","email_address" => "brahamshakti@gmail.com","phone_number" => {"type" => "home","area_code" => "0171","number" => "2520002"}}}
     # API: Ethor::Ticket.submit_order(body, 'store' => 'HI6PIDO5JS', 'ticket_id' => 'WKJK93VM12')
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/WKJK93VM12/submit?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n
      def submit_order(body,parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "Ticket Id is required"} if @ticket.blank?
        begin
          response = RestClient.post "#{ETHOR_URL}/stores/#{@store}/tickets/#{@ticket}/submit?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", body.to_json, :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

     # Retrieve a ticket identified by its unique POS Ticket ID
     # API: Ethor::Ticket.retrieve_ticket_by_pos_ticket_id('store' => 'HI6PIDO5JS', 'pos' => '1640', 'fields' => ["order_id","order_date","order_type"])
     # https://ethor-test.apigee.net:443/v1/stores/HI6PIDO5JS/tickets/pos/1640?apikey=XyRZmbcyVoBHhlA9we3cH8VMiyXNF96n&fields=order_type%2Corder_status%2Corder_id
      def retrieve_ticket_by_pos_ticket_id(parameters={})
        extra_params parameters
        return {"error" => "Store Id is required"} if @store.blank?
        return {"error" => "POS Ticket Id is required"} if @pos.blank?
        begin
          response = RestClient.get "#{ETHOR_URL}/stores/#{@store}/tickets/pos/#{@pos}?apikey=#{API_KEY}#{@fields}#{@offset}#{@limit}", :content_type => :json, :accept => :json
          data = JSON.parse(response)
        rescue Exception => e
          data = JSON.parse(e.response)
        end
        return data
      end

    end # Class Ends Here

  end


  def extra_params(parameters)
    @fields = parameters.has_key?('fields') ? "&fields=#{parameters['fields'].join(',')}" : ""
    @offset = parameters.has_key?('offset') ? "&offset=#{parameters['offset']}" : ""
    @limit = parameters.has_key?('limit') ? "&limit=#{parameters['limit']}" : ""
    @chain = parameters['chain_id'] ||= ""
    @store = parameters['store'] ||= ""
    @customer = parameters['customer_id'] ||= ""
    @categories = parameters.has_key?('categories') ? "&categories=#{parameters['categories'].join(',')}" : ""
    @lat = parameters.has_key?('lat') ? "&lat=#{parameters['lat']}" : ""
    @lng = parameters.has_key?('lng') ? "&lng=#{parameters['lng']}" : ""
    @dist = parameters.has_key?('dist') ? "&dist=#{parameters['dist']}" : ""
    @start_date = parameters.has_key?('start_date') ? "&start_date=#{parameters['start_date']}" : ""
    @end_date = parameters.has_key?('end_date') ? "&end_date=#{parameters['end_date']}" : ""
    @check_location = parameters.has_key?('check_location') ? "&check_location=#{parameters['check_location']}" : ""
    @item = parameters.has_key?('menu_item_id') ? parameters['menu_item_id'] : ""
    @restaurant = parameters['restaurant_id'] ||= ""
    @table = parameters['table_id'] ||= ""
    @order = parameters['order_id'] ||= ""
    @ticket = parameters['ticket_id'] ||= ""
    @pos = parameters['pos'] ||= ""
  end


end