require "ethor"
module EthorData
  class Ticket
    def self.info(restaurant)
      ticket = Ethor::Ticket.create({'order_type' => 'dine_in'},'store' => restaurant.rid)
      ticket["ticket"]["order_id"] rescue nil
    end
  end
  class Customer
    def self.info
      Ethor::Chain.get_customer('chain_id' => 'YCJC0YLTNG', 'customer_id' => '7AMI02B1NW')
    end
  end
  class Order
    def self.info(order)
      order_details = order.orderdetails.where(:submitted => false)
      food_items = order.fooditems.select{|f| f if order_details.map{|o| o.food_id }.include? f.id}
      order_with_extras = order.orderdetailswithextras.where(:submitted => false)
      order_with_drinks = order.orderdetailswithdrinkses.where(:submitted => false)
      drink_items = order.drinks.select{|d| d if order_with_drinks.map{|o| o.drink_id }.include? d.id}
      menu_items = food_items + drink_items
      overall_details = order_details + order_with_drinks
      {"table_id" => (order.table.table_id.to_s rescue ""), "order_items" => menu_items.map{|item| {"menu_item_id" => item.menu_item_id, "menu_item_name"=>(item.class.to_s == "Fooditem" ? item.food_name : item.drink_name), "modifiers" => order_with_extras.select{|extra|extra.food_id == item.id}.map{|e| {"modifier_id" => e.modifier.modifier_id, "modifier_name" => (e.modifier.class.to_s == "Extra" ? e.modifier.extra_name : e.modifier.drinkcategoryname)}}, "quantity" => overall_details.find{|d| (d.class.to_s == "Orderdetail" ? d.food_id == item.id : d.drink_id == item.id)}.quantity.to_s, "variant_id" => (overall_details.find{|d| (d.class.to_s == "Orderdetail" ? d.food_id == item.id : d.drink_id == item.id)}.variant.variant_id rescue "")}}}
    end
  end
  class Payment
    def self.info(order)
      {"order_payments" => [{"payment_type" => "C","payment_tendered" => order.total_bill.to_f}]}
    end
  end
end