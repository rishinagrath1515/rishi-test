
namespace :rws do
  desc "Cleaning database test records" 
    task :clean_test_restaurants,[:restaurant_id]=> :environment do|t, args|
      if rest_detail = args[:restaurant_id] && Restaurantdetail.where(rid: args[:restaurant_id]).first 
        ActiveRecord::Base.transaction do
          begin
            Gimbaldetail.where(restaurant_id: rest_detail.id).delete_all
            Admin.where(restaurant_id: rest_detail.id).delete_all
            Restauranttable.where(restaurant_id: rest_detail.id).delete_all
            Order.where(restaurant_id: rest_detail.id).delete_all
            Restauranttiming.where(restaurant_id: rest_detail.id).delete_all
            Category.where(restaurant_id: rest_detail.id).delete_all
            Restauranthappyhour.where(restaurant_id: rest_detail.id).delete_all
            Extra.where(restaurant_id: rest_detail.id).delete_all
            Fooditem.where(restaurant_id: rest_detail.id).delete_all
            Drinkcategory.where(restaurant_id: rest_detail.id).delete_all
            Alldrinks.where(restaurant_id: rest_detail.id).delete_all
            Foodandextra.where(restaurant_id: rest_detail.id).delete_all
            Fooditemandcatgory.where(restaurant_id: rest_detail.id).delete_all
            Advertisement.where(restaurant_id: rest_detail.id).delete_all
            if rest_detail.destroy
              Rails.logger.info "Restaurent with id = #{args[:restaurant_id]} removed"
            end
            rescue=>e
            Rails.logger.error "Error occured in cleaning Restaurent data"
            Rails.logger.error e.message
            Rails.logger.error e.backtrace
            p e.message
            p e.backtrace
            raise ActiveRecord::Rollback
          end
        end
      end
  end
end  