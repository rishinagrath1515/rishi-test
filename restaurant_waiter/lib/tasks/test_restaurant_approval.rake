namespace :restaurant_waiter do
  
  desc "Approval dummy restaurant as paid"
  task :make_restaurant_live,[:email] => :environment do|t, args|
    user = Admin.where(email: args[:email]).first
    user.amount_paid = 100
    user.amount_paid_date = Time.now
    user.confirm_status = true
    user.is_paid = true
    user.save
    
    
  end 
  
  
  
end