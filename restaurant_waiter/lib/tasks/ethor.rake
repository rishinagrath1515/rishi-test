require 'ethor'
namespace :ethor do
  desc "Update Menus Items id"
  task :sync_data => :environment do
    restaurants = Restaurantdetail.all
    sizes = Size.all
    food_items = Fooditem.all
    extras = Extra.all
    drinks = Alldrinks.all
    drink_categories = Drinkcategory.all
    restaurants.each do |restaurant|
      menus = Ethor::Menu.get_list('store' => restaurant.rid) rescue {}
      unless menus.has_key? 'error'
        categories = menus["menu"]["categories"]
        categories.each do |category_hash|
          menus_items = category_hash["menu_items"]
          menus_items.each do |item_hash|
            food_items.each{|item| item.update_attributes(:menu_item_id => item_hash['menu_item_id']) if item.food_name.downcase == item_hash['menu_item_name'].downcase}
            drinks.each{|item| item.update_attributes(:menu_item_id => item_hash['menu_item_id']) if item.drink_name.downcase == item_hash['menu_item_name'].downcase}
            item_detail = Ethor::Menu.get_menu_item('store' => restaurant.rid, 'menu_item_id' => item_hash['menu_item_id'])
            variants_array = item_detail["menu_item"]["variants"]
            variants_array.each do |variant_hash|
              sizes.each{|s| s.update_attributes(:variant_id => variant_hash['variant_id']) if s.size_name.downcase == variant_hash['variant_name'].downcase}
            end
            modifiers_array = item_detail["menu_item"]["modifier_groups"]
            modifiers_array.each do |modifier_hash|
              modifiers = modifier_hash['modifiers']
              modifiers.each do |modifier|
               extras.each{|fx| fx.update_attributes(:modifier_id => modifier['modifier_id']) if fx.extra_name.downcase == modifier['modifier_name'].downcase} rescue ""
              end
            end
          end
        end
      end
    end
  end
end